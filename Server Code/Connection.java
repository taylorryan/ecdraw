import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Random;

import javax.imageio.ImageIO;

public class Connection extends Thread{

	//Packet Info
	private static final int PACKET_TYPE_HDR = 0;
	private static final int SIZE_HDR = 1;
	private static final int CLIENT_ID_HDR = 2;
	private static final int PAGE_ID_HDR = 3;
	private static final int LINE_ID_HDR = 4;
	//Message Types
	private static final int LINE_MSG = 0;
	private static final int IMAGE_MSG = 1;
	private static final int TEXT_MSG = 2;
	private static final int SHAPE_MSG = 3;
	private static final int UNDO_MSG = 4;
	private static final int CLEAR_MSG = 5;
	private static final int CLEAR_ALL_MSG = 6;
	private static final int ADD_PAGE_MSG = 7;
	private static final int REQUEST_PAGE_MSG = 8;
	private static final int DELETE_PAGE_MSG = 9;
	private static final int REFRESH_PAGE_REQUEST_MSG = 10;
	private static final int REFRESH_PAGE_MSG = 11;
	private static final int CLIENT_ID_MSG = 12;
	private static final int CLIENT_ID_REQUEST_MSG = 13;
	private static final int REDO_MSG = 14;
	private static final int SESSION_NEW_MSG = 15;
	private static final int SESSION_LOAD_MSG = 16;
	private static final int SESSION_SAVE_MSG = 17;
	private static final int NEW_HOST_MSG = 18;
	private static final int CLOSE_CONNECTION_MSG = 19;
	private static final int CHECK_PASSWORD_MSG = 20;
	private static final int DELETE_MSG = 21;
	private static final int HEARTBEAT_MSG = 22;

	// Connection info: host address and port
	//	private int port;
	public String hostName;
	public Room room;
	public boolean isHost;
	public int id = -1;
	private long lastReadTime;

	// Connection info: native socket handle
	private Socket socket;
	private Server server;

	// Read stream
	private BufferedInputStream buffIn;
	private int packetInfo[]= new int[5];
	private byte[] packet;

	// Write stream
	private BufferedOutputStream buffOut;

	public Connection(int _port, Socket _socket, Server _server) throws IOException{
		//		port = _port;
		socket = _socket;
		server = _server;
		hostName = socket.getInetAddress().getHostName();

		buffIn = new BufferedInputStream(socket.getInputStream());
		buffOut = new BufferedOutputStream(socket.getOutputStream());
		buffOut.flush();

		for(int i = 0; i < packetInfo.length; i++)
			packetInfo[i] = -2;

	}
	public void run(){

		while(true){//!socket.isClosed()			

			try{

				//Check if entire packet header has been read in
				if(packetInfo[LINE_ID_HDR] == -2){
					//Still more to read.

					byte[] b = new byte[4];
					int result = buffIn.read(b);

					if(result == -1){//Client Closed connection without notification, probably a crash.
						if(room == null){
							//This comes from the checker program, just close connection.
							close();
							break;
						}

						// remove connection from room
						room.removeFromRoom(this);

						if(room.count() < 1){
							server.removeRoom(room);
						}
						else{
							//fix packet and info
							packet = intToByteArray(1);
							packetInfo[PACKET_TYPE_HDR] = NEW_HOST_MSG;
							packetInfo[SIZE_HDR] = 4;
							packetInfo[CLIENT_ID_HDR] = id;
							packetInfo[LINE_ID_HDR] = 0;
							if(isHost)
								packetInfo[LINE_ID_HDR] = -1;

							//send packet to new host
							room.sendToHost(packet, packetInfo);
						}
						//close connection
						close();
						break;
					}

					int value = byteArrayToInt(b);

					for(int i = 0; i < packetInfo.length; i++)
					{
						if(packetInfo[i] == -2){
							packetInfo[i] = value;

							break;
						}
					}

				}
				else{					
					packet = new byte[packetInfo[SIZE_HDR]];

					int result = buffIn.read(packet);
					while(result < packetInfo[SIZE_HDR]){//reads in entire packet
						result += buffIn.read(packet,result,packetInfo[SIZE_HDR]-result);
						//System.out.print(result + ".");
					}

					//process packet to correct client
					processPacket(packet,packetInfo);

					if (packetInfo[PACKET_TYPE_HDR] == NEW_HOST_MSG){
						//If packet type is this, it means the new host was told they are the new host and this connection can be closed.
						close();
						break;
					}

					//reset packet info.
					for(int i = 0; i < 5; i++)
						packetInfo[i] = -2;


				}

				lastReadTime = System.currentTimeMillis();

			}
			catch (SocketTimeoutException t){
				//No contact within 10 seconds.
				int maxTimeout = 30000; // 1 minute
				long currentTimeout = System.currentTimeMillis() - lastReadTime;

				if(currentTimeout > maxTimeout){
					//To long since last contact

					// remove from room
					if(room!=null)
						room.removeFromRoom(this);
					
					//If was host, send message to new host.
					if(isHost){						
						int header[]= new int[5];
						byte[] data = intToByteArray(1);
						room.setNewHost();
						header[PACKET_TYPE_HDR] = NEW_HOST_MSG;
						header[CLIENT_ID_HDR] = id;
						header[LINE_ID_HDR] = -1;
						header[SIZE_HDR] = 4;
												
						try {
							room.sendToHost(data, header);
						} catch (IOException e) {}
					}						

					//room now empty? remove room from server.
					if(room.count() < 1){
						server.removeRoom(room);
					}
					
					//close connection
					close();
				}
				else{
					//try to send refresh
					try {
						sendHeartbeat();
					} catch (IOException e) {}
					
//					System.out.println("sent heartbeat");
				}
			}
			catch (IOException e) {
				if(!socket.isClosed()){
					// remove from room
					room.removeFromRoom(this);
					
					//If was host, send message to new host.
					if(isHost){						
						int header[]= new int[5];
						byte[] data = intToByteArray(1);
						room.setNewHost();
						header[PACKET_TYPE_HDR] = NEW_HOST_MSG;
						header[CLIENT_ID_HDR] = id;
						header[LINE_ID_HDR] = -1;
						header[SIZE_HDR] = 4;
												
						try {
							room.sendToHost(data, header);
						} catch (IOException ee) {}
					}						

					//room now empty? remove room from server.
					if(room.count() < 1){
						server.removeRoom(room);
					}
					// close connection.
					close();
				}
				break;
			}

		}


	}

	private void processPacket(byte[] _packet, int[] _packetInfo) throws IOException{
		byte[] rawPacket = _packet;
		int[] info = _packetInfo;

		boolean forward = false;
		boolean sendToHost = false;

		String temp;
		String roomName;

		switch (info[PACKET_TYPE_HDR])
		{
		case LINE_MSG:
		case IMAGE_MSG:
		case TEXT_MSG:
		case SHAPE_MSG:
		case UNDO_MSG:
			forward = true;
			break;
		case CLEAR_MSG:
		case CLEAR_ALL_MSG:
			if(isHost)
				forward = true;
			else
				sendToHost = true;
			break;
		case ADD_PAGE_MSG:
			forward = true;
			break;
		case REQUEST_PAGE_MSG:
			sendToHost = true;
			break;
		case DELETE_PAGE_MSG:
			if(isHost)
				forward = true;
			else
				sendToHost = true;
			break;
		case REFRESH_PAGE_REQUEST_MSG:
			sendToHost = true;
			break;
		case REFRESH_PAGE_MSG:
			forward = true;
			break;
		case CLIENT_ID_MSG:
			////NOT USED
			break;
		case CLIENT_ID_REQUEST_MSG:
			roomName = new String(rawPacket);

			//check if room needs a password.
			if(server.roomNeedPassword(roomName)){
				//room needs a password. Send packet to iPad to ask for password.
				info[PACKET_TYPE_HDR] = CHECK_PASSWORD_MSG;

				sendPacket(rawPacket,info);
				break;
			}

			room = server.addToRoom(this, roomName);

			if(room == null){
				close();
				break;
			}
			System.out.println(new Timestamp(System.currentTimeMillis()) +": Joining Room: '" +roomName+"'");

			info[PACKET_TYPE_HDR] = CLIENT_ID_MSG;
			info[CLIENT_ID_HDR] = genID();
			id = info[CLIENT_ID_HDR];
			info[LINE_ID_HDR] = 0;
			if(isHost)
				info[LINE_ID_HDR] = -1;
			sendPacket(rawPacket,info);
			break;
		case REDO_MSG:
			forward = true;
			break;
		case SESSION_NEW_MSG:
			sendToHost = true;
			break;
		case SESSION_LOAD_MSG:
			if(isHost)
				forward = true;
			break;
		case SESSION_SAVE_MSG:
			////NOT USED
			break;
		case NEW_HOST_MSG:
			sendToHost = true;
			isHost = true;
			temp = new String(rawPacket);
			boolean isPassword = temp.contains("$");

			if(isPassword){
				roomName = temp.substring(0, temp.indexOf('$'));
				temp = temp.substring(temp.indexOf('$')+1);
			}
			else{
				roomName = temp;
			}

			room = server.createRoom(roomName);
			if(room == null){
				isHost = false; //room already exists and there is a host
				sendToHost = false;
				System.out.println(new Timestamp(System.currentTimeMillis()) +": Joining Room: '" +roomName+"'");
				info[LINE_ID_HDR] = 0;
			}
			else{
				info[LINE_ID_HDR] = -1;
				if(isPassword){
					//set password
					room.setPassword(temp);
				}
				System.out.println(new Timestamp(System.currentTimeMillis()) +": Creating Room: '"+roomName+"' Password: " + isPassword);
			}
			room = server.addToRoom(this, roomName);
			info[PACKET_TYPE_HDR] = CLIENT_ID_MSG;
			info[CLIENT_ID_HDR] = genID();
			id = info[CLIENT_ID_HDR];

			if(!isHost)
				sendPacket(rawPacket,info);

			break;
		case CLOSE_CONNECTION_MSG:
			//closes connection

			// remove from room
			room.removeFromRoom(this);

			//transfer host to next client
			if(isHost){
				//select sendtohost
				sendToHost = true;
				room.setNewHost();
				//change packet type
				info[PACKET_TYPE_HDR] = NEW_HOST_MSG;
				info[CLIENT_ID_HDR] = id;
				info[LINE_ID_HDR] = -1;
			}

			//room now empty? remove room from server.
			if(room.count() < 1){
				server.removeRoom(room);
				sendToHost = false;
			}

			break;
		case CHECK_PASSWORD_MSG:

			temp = new String(rawPacket);
			roomName = temp.substring(0, temp.indexOf('$'));
			temp = temp.substring(temp.indexOf('$')+1);

			if(server.checkRoomPassword(roomName, temp)){
				// password is correct, join room.
				room = server.addToRoom(this, roomName);

				if(room == null){
					System.out.println("Room is null");
					close();
					break;
				}

				System.out.println(new Timestamp(System.currentTimeMillis()) +": Joining Room: '" +roomName+"'");

				info[PACKET_TYPE_HDR] = CLIENT_ID_MSG;
				info[CLIENT_ID_HDR] = genID();
				id = info[CLIENT_ID_HDR];
				info[LINE_ID_HDR] = 0;
				if(isHost)
					info[LINE_ID_HDR] = -1;
				sendPacket(rawPacket,info);

			}
			else{
				//Password is incorrect, or room doesnt exist.
				close();
			}

			break;
		case DELETE_MSG:
			forward = true;
			break;

		}

		if(forward)
			room.forwardToClient(rawPacket, info, this);
		else if(sendToHost)
			room.sendToHost(rawPacket, info);

	}

	public void sendPacket(byte[] _packet, int[] _packetInfo) throws IOException{
		if(socket.isClosed()){
			room.removeFromRoom(this);
			close();
		}

		//write header to buffOut
		for(int i = 0; i < 5; i++){
			buffOut.write(intToByteArray(_packetInfo[i]));
		}

		//write data to buffOut		
		buffOut.write(_packet);

		//flush
		buffOut.flush();
	}

	public void close(){
		if(!hostName.equals("localhost"))
			System.out.println(new Timestamp(System.currentTimeMillis()) +": Connection closed with " + hostName);

		try {
			//close everything up
			buffIn.close();
			buffOut.close();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void sendHeartbeat() throws IOException{
		int header[]= new int[5];
		byte[] data;

		header[PACKET_TYPE_HDR] = HEARTBEAT_MSG;
		header[SIZE_HDR] = 4;
		header[CLIENT_ID_HDR] = 0;
		header[PAGE_ID_HDR] = 0;
		header[LINE_ID_HDR] = 0;

		data = intToByteArray(4);

		sendPacket(data,header);
	}

	private final int byteArrayToInt(byte[] b){
		int value = 0;
		for (int i = 0; i < b.length; i++)
		{
			value += ((int) b[i] & 0xffL) << (8 * i);
		}
		return value;
	}

	private final byte[] intToByteArray(int i){
		return new byte[] {
				(byte)(i),
				(byte)(i >>> 8),
				(byte)(i >>> 16),
				(byte)(i >>> 24)
		};
	}

	private int genID(){
		Random random = new Random();
		int newID = random.nextInt(10000);
		while(room.isIDused(newID))
			newID = random.nextInt(10000);
		return newID;
	}

}
