#!/bin/sh
#
# init script for a Java application
#

toEmail='email to send to'
fromEmail='email sent from this account'
smtpUser='fromEmail smtp username'
smtpPassword='fromEmail smtp password'

cd /etc/LightLink

java -cp /etc/LightLink Check > check

value=`cat check`

if [ $value = "OK" ]
then
	#Everything is up and running.
	echo "Server is up and running"
else
	cp /etc/LightLink/error.log /etc/LightLink/mailError.log
	java -Xmx2000m -server -classpath /etc/LightLink/server.jar Server noMenu > /etc/LightLink/output.log 2> /etc/LightLink/error.log &
	mailsend -f $fromEmail  -smtp mail.etown.edu -sub "LightLink Crontab Error" -to $toEmail -starttls -autho-login -user $smtpUser -pass $smtpPassword -M "The LightLink server was down when Crontab tried to connect. The server has been restarted." -attach "/etc/LightLink/mailError.log,text/plain"
	rm /etc/LightLink/mailError.log
fi

rm check

exit 0
