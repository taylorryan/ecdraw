import java.io.IOException;
import java.util.ArrayList;


public class Room {

	private ArrayList<Connection> connections = new ArrayList<Connection>();

	public String name;
	private String password;
	public boolean passwordSet = false;
	private String creator;
	private Connection host;
	
	public Room(String name) {
		this.name = name;
	}

	public void closeAllConnections(){
		for(Connection c : connections)
			c.close();

		connections.clear();
	}

	public int addConnection(Connection c) {

		if(connections.size() == 0){
			// Connection being added is the creator of the Room
			this.creator = c.hostName;
			this.host = c;

			connections.add(c);
		}
		else if(this.creator.equals(c.hostName)){
			// Connection being added created the room originally.
			c.isHost = true;

			//set old host isHost to false;
			host.isHost = false;
			
			//Add connection to front of arrayList
			connections.add(0,c);
			
			try {
				//Set up packets
				byte[] bytes = intToByteArray(1);
				int packetInfo[]= new int[5];
				packetInfo[0] = 12;
				packetInfo[1] = 4;
				packetInfo[2] = host.id;
				packetInfo[3] = 0;
				packetInfo[4] = 0;
				host.sendPacket(bytes, packetInfo);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			//Set host of room to creator connection.
			host = c;
		}
		else{
			//Just add connection to arrayList.
			connections.add(c);
		}

		return connections.indexOf(c);
	}

	public void sendToHost(byte[] _packet, int[] _packetInfo) throws IOException{
		if(connections.size() < 1)
			return;

		Connection c = connections.get(0);
		c.sendPacket(_packet, _packetInfo);
	}

	public void forwardToClient(byte[] _packet, int[] _packetInfo, Connection connect) throws IOException{		
		for(Connection c: connections){
			if(!c.hostName.equals(connect.hostName) ){
				c.sendPacket(_packet, _packetInfo);
			}
		}
	}

	public void setNewHost()
	{
		if(connections.size() > 0)
		{
			Connection c = connections.get(0);
			c.isHost = true;
			host = c;
		}
	}

	public boolean removeFromRoom(Connection c){
		return connections.remove(c);
	}

	public int count(){
		return connections.size();
	}

	public boolean isIDused(int id){
		for(Connection c : connections){
			if(c.id == id)
				return true;
		}
		return false;
	}

	// Returns true if pass matches room password. False otherwise.
	public boolean correctPassword(String pass)
	{
		return (password.equals(pass));
	}

	//Setter method for password
	public void setPassword(String pass)
	{
		this.password = pass;
		this.passwordSet = true;
	}

	//List all clients in the Room.
	public void listClients(){
		for(Connection c : connections){
			System.out.print(c.hostName);
			if(c.isHost)
				System.out.print(" (HOST)");
			System.out.println();
		}
	}

	private final byte[] intToByteArray(int i){
		return new byte[] {
				(byte)(i),
				(byte)(i >>> 8),
				(byte)(i >>> 16),
				(byte)(i >>> 24)
		};
	}

	public String toString(){
		return "Room: " + name + " User(s): " + connections.size();
	}
}
