import java.io.*;
import java.net.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Scanner;

public class Server extends Thread{

	private static ServerSocket serverSocket;
	private static int port;
	private static ArrayList<Room> rooms = new ArrayList<Room>();
	private boolean running;

	public static void main(String[] args) {

		System.out.println();
		System.out.println("==========================================================");
		System.out.println("Starting Server on port 9200...");
		System.out.println("==========================================================");
		System.out.println();

		Server server = new Server(9200);
		System.out.println();

		server.start();// Start Server Thread

		if(args.length < 1){
			//Only Show menu option if  no arguments is passed at startup.
			server.serverMessage("Type 'm' for the menu.");

			Scanner in = new Scanner(System.in);
			String temp = in.nextLine();

			//prevent empty string...
			while(temp.length() <1){
				temp = in.nextLine();
			}

			char choice = temp.charAt(0);

			while(choice != 'q'){
				// "Menu" Loop

				switch (choice)
				{
				case 'm':
					System.out.println("==========================================================");
					System.out.println("LightLink Server Menu:");
					System.out.println("l: Shows list of active rooms.");
					System.out.println("m: Shows this menu.");
					System.out.println("q: Shutdowns the server.");
					System.out.println("r ?: Shows the clients connected to Room ?  (i.e. r 2)");
					System.out.println("==========================================================");
					break;

				case 'l':
					System.out.println("==========================================================");
					System.out.println("Active Rooms:");
					if(rooms.size() == 0){
						System.out.println("No Active Rooms");
					}
					else{
						int c = 1;
						for(Room r : rooms){
							System.out.println(c + ". " + r);
							c++;
						}
					}
					System.out.println("==========================================================");
					break;
				case 'r':
					System.out.println("==========================================================");
					int roomNumber;
					try{
						roomNumber = Integer.parseInt(temp.substring(1).trim());
					}
					catch(NumberFormatException e){
						System.out.println("Error: Room Number entered is not a valid number.");
						System.out.println("==========================================================");
						break;
					}

					// check to make sure the room actually exists
					if( roomNumber > rooms.size() || roomNumber <= 0){
						//roomNumber out of bounds.
						System.out.println("Room Number " + roomNumber + " does not exist.");
					}
					else{
						//roomNumber in bounds
						Room room = rooms.get(roomNumber-1);
						room.listClients();
					}
					System.out.println("==========================================================");
					break;
				default:
					server.serverMessage("Error: Incorrect Menu Choice. Type 'm' for the menu.");
					break;
				}


				//Prompt Next choice
				temp = in.nextLine();
				//prevent empty string...
				while(temp.length() <1)
					temp = in.nextLine();
				choice = temp.charAt(0);
			}

			// 'q' was chosen. Shutdown server.
			server.serverMessage("Shutting down server...");

			server.shutdownServer();

			server.serverMessage("Server has been stopped.");
		}
	}

	private void serverMessage(String msg){
		System.out.println("==========================================================");
		System.out.println(msg);
		System.out.println("==========================================================");
	}

	private void shutdownServer(){
		for(Room r : rooms){
			r.closeAllConnections();
		}

		rooms.clear();

		try {
			serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		running = false;
	}

	public Server(int _port){
		port = _port;
		running = true;
		try {
			serverSocket = new ServerSocket(port);
			this.serverMessage("Waiting for connections...");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void run()
	{
		while(running){
			try {

				Socket socket = serverSocket.accept();
				socket.setSoTimeout(10000);// 10 seconds
				if(!socket.getInetAddress().getHostName().equals("localhost"))
					System.out.println(new Timestamp(System.currentTimeMillis()) +": Connection received from " + socket.getInetAddress().getHostName());
				Connection connection = new Connection(port,socket,this);
				connection.start();

			} 
			catch (SocketTimeoutException t){
				System.out.println("TIMEOUT SERVER");
			}
			catch (SocketException s){}
			catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	public Room createRoom(String name){

		for(Room r : rooms){
			if(r.name.equals(name)){
				//room already exists, add to room.
				return null;
			}
		}

		Room newRoom = new Room(name);
		rooms.add(newRoom);

		return newRoom;
	}

	public Room addToRoom(Connection c, String room){
		for(Room r : rooms){
			if(r.name.equals(room)){
				r.addConnection(c);
				return r;
				//return r.roomID;
			}
		}

		//no room found by that name.
		return null;
	}

	public boolean removeRoom(Room r){
		boolean result = rooms.remove(r);
		if(result)
			System.out.println(new Timestamp(System.currentTimeMillis()) +": Removed Room: '" + r.name+ "'");

		return result;
	}

	public boolean checkRoomPassword(String room, String password){
		for(Room r : rooms){
			if(r.name.equals(room))
				return r.correctPassword(password);
		}

		return false;
	}

	public boolean roomNeedPassword(String name){

		for(Room r : rooms){
			if(r.name.equals(name))
				return r.passwordSet;
		}

		return false;
	}

	public String roomsList(){
		String result = "";

		for(Room r : rooms){
			result += r.name+ "$";
			if(r.passwordSet)
				result += "1$";
			else
				result += "0$";
		}

		return result;
	}

}
