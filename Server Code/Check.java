import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;


public class Check {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Boolean error = false;
		
		try {
			new Socket("127.0.0.1",9200);
		} catch (UnknownHostException e) {
			System.out.print("ERROR");
			error = true;
		} catch (IOException e) {
			System.out.print("ERROR");
			error = true;
		}
		
		if(!error)
			System.out.print("OK");
		
	}

}
