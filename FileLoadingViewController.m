//
//  FileLoadingViewController.m
//  ECDraw
//
//  Created by Ryan, Taylor on 5/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FileLoadingViewController.h"
#import "DrawAreaViewController.h"

@interface FileLoadingViewController ()

@end

@implementation FileLoadingViewController
@synthesize tableView;
@synthesize controller;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //first use the local documents folder
    NSString *docsPath = [NSString stringWithFormat:@"%@/Documents", NSHomeDirectory()];
    //then use its bundle, indicating its path
    NSString *bundleRoot = [[NSBundle bundleWithPath:docsPath] bundlePath];
    //then get its content
    NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:bundleRoot error:nil];
    // this counts the total of jpg images contained in the local document folder of the app
    files = [[NSMutableArray arrayWithArray:[dirContents filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self ENDSWITH '.dat'"]]] retain];       
    
    [tableView reloadData];
}

- (void)viewDidUnload
{
    [self setTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft)||(interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

-(NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section{
    return MAX([files count],1);
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"Saved Sessions";
}

-(UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    if ([files count] < 1) {
        [cell.textLabel setText:@"No sessions saved"];
    }
    else {
        NSString *file=[files objectAtIndex:indexPath.row];
        [cell.textLabel setText:[file substringToIndex:[file length]-4]];
    }
    
    return cell;
    
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([files count] < 1) {
        return NO;
    }
    return YES;
}
-(void)tableView:(UITableView *)aTableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete && [files count] > 0) {
        
        NSString *file = [[files objectAtIndex:indexPath.row] retain];
        
        [files removeObjectAtIndex:indexPath.row];
        
        //first use the local documents folder
        NSString *docsPath = [NSString stringWithFormat:@"%@/Documents", NSHomeDirectory()];
        //then use its bundle, indicating its path
        NSString *bundleRoot = [[NSBundle bundleWithPath:docsPath] bundlePath];

        bundleRoot = [NSString stringWithFormat:@"%@/%@",bundleRoot,file];
        //then delete the file
        [[NSFileManager defaultManager] removeItemAtPath:bundleRoot error:nil];
  
        [file release];
        
        if ([files count] > 0) {
            [aTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
        else {
            [aTableView reloadData];
        }
        
    }
}

-(void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([files count] > 0) {
        [controller loadSession:[files objectAtIndex:indexPath.row]];
        [self dismissModalViewControllerAnimated:YES];
    }
    [aTableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (void)dealloc {
    [tableView release];
    [files release];
    [super dealloc];
}
- (IBAction)cancelPressed:(UIButton *)sender {
    [self dismissModalViewControllerAnimated:YES];
}
@end
