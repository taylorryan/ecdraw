//
//  DrawArea.h
//  QuartzDraw
//
//  Created by Bianco, Anthony M on 6/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

typedef enum{
	DRAW_MODE,
	TEXT_MODE,
	SHAPE_MODE,
	ERASE_MODE,
	SCROLL_MODE,
    MOVE_MODE
} ViewModes;

#import <UIKit/UIKit.h>
#import "ECLine.h"
#import "ECText.h"
#import "ECPage.h"
#import "ECShape.h"
#import "ECImage.h"
#import "ShapePickerControllerDelegate.h"

@class DrawAreaViewController;


@interface DrawArea : UIView <ShapePickerControllerDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate>{
	@private
	NSMutableArray *pages;
	NSInteger currentPageIndex, pageCount;
	UIColor *currentColor;
	CGFloat currentSize;
	CGPoint	location;
	int mode;
	UITextField *userText;
	ECLine *currentLine;
	ECShape *currentActiveShape;
	ShapeType currentShapeType;
	
    DrawingObject *movingObject;
    
	DrawAreaViewController *controller;
    
    BOOL isMainScreen;
    
    UIImage *pickerImage;
    
}

@property (nonatomic, retain) IBOutlet UITextField *userText;
@property (readonly) NSInteger pageCount;
@property (readwrite) NSInteger currentPageIndex;
@property (assign) DrawAreaViewController *controller;
@property int mode;
@property BOOL isMainScreen;
@property (nonatomic) BOOL externalScreenAttached;

+ (DrawArea *)getInstance;
- (void)drawRect:(CGRect)rect;

- (int)getMode;
- (void)setColorToClear;
- (void)setColorToUIColor:(UIColor *)color;
- (void)setSize:(CGFloat)stroke;

- (void)erasePageDrawing;
- (void)erasePageDrawingForPageID:(NSInteger)pid;
- (void)erasePageImage;
- (void)erasePageImageForPageID:(NSInteger)pid;

- (void)addImage:(UIImage *)img;
- (void)addImage:(UIImage *)img forPage:(NSInteger)pid;
- (void)refreshImage;

- (void)addPage;
- (void)removePageForPageID:(NSInteger)pid;
- (void)removeAllPages;
- (void)nextPage;
- (void)previousPage;
- (void)sendPages;
- (void)replacePageWith:(NSMutableArray *)replacementPage;

- (void)setTranslationToZero;

- (void)savePages:(NSString *) filename;
- (void)loadPages:(NSString *)filename;
- (void)updateClientID:(NSInteger)newID from:(NSInteger)oldID;

- (void)redo;
- (void)redoForClient:(NSInteger)cid onPage:(NSInteger)pid;
- (void)undo;
- (void)undoForClient:(NSInteger)cid onPage:(NSInteger)pid;
- (ECPage *)getCurrentPage;
- (void)addDrawingObjectToPage:(DrawingObject *)item;
- (ECText *)addText:(NSString *) text;
- (void)deleteObject:(DrawingObject *)object;

- (NSInteger)currentPageID;
- (void)handlePinchGesture:(UIPinchGestureRecognizer *)sender;
- (void)handleLongPressGesture:(UILongPressGestureRecognizer *) gesture;

- (void) showGrid;
- (int) undoCount;
@end
