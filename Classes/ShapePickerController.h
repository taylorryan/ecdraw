//
//  ShapePickerController.h
//  ECDraw
//
//  Created by Bianco, Anthony M on 8/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShapePickerControllerDelegate.h"


@interface ShapePickerController : UIViewController {
	id<ShapePickerControllerDelegate> delegate;
	
    IBOutlet UIView *shapes2D;
    IBOutlet UIView *shapes3D;
	//BOOL show3DShapes;
}

//2D Shapes
- (IBAction)squarePressed:(UIButton *) sender;
- (IBAction)rectanglePressed:(UIButton *) sender;
- (IBAction)circlePressed:(UIButton *) sender;
- (IBAction)eTrianglePressed:(UIButton *) sender;
- (IBAction)rTrianglePressed:(UIButton *) sender;
- (IBAction)ovalPressed:(UIButton *) sender;
- (IBAction)hLinePressed:(UIButton *) sender;
- (IBAction)vLinePressed:(UIButton *) sender;
- (IBAction)dLinePressed:(UIButton *) sender;
//3D Shapes
- (IBAction)cubePressed:(UIButton *)sender;         //(0,0)
- (IBAction)cuboidPressed:(UIButton *)sender;       //(0,1)
- (IBAction)pyramidPressed:(UIButton *)sender;      //(1,0)
- (IBAction)prismPressed:(UIButton *)sender;        //(1,1)
- (IBAction)conePressed:(UIButton *)sender;         //(1,2)
- (IBAction)cylinderPressed:(UIButton *)sender;     //(2,0)
- (IBAction)spherePressed:(UIButton *)sender;       //(2,1)

- (IBAction)flipView:(UIButton *) sender;

@property (nonatomic,retain) id<ShapePickerControllerDelegate> delegate;

@end
