//
//  ColorPickerView.m
//  ColorPicker
//
//  Created by Gilly Dekel on 23/3/09.
//  Extended by Fabián Cañas August 2010.
//  Copyright 2010. All rights reserved.
//

#import "ColorPickerView.h"
#import "GradientView.h"
#import "Constants.h"
#import "UIColor-HSVAdditions.h"

@implementation ColorPickerView

@synthesize currentHue;
@synthesize currentSaturation;
@synthesize currentBrightness;

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        // Initialization code
		
    }
    return self;
}

- (id)initWithCoder:(NSCoder*)coder  {
	if (self = [super initWithCoder:coder]) {
		
		gradientView = [[GradientView alloc] initWithFrame:kBrightnessGradientPlacent];
		//[gradientView setTheColor:[UIColor yellowColor]];
		[self addSubview:gradientView];
		[self sendSubviewToBack:gradientView];
		[self setMultipleTouchEnabled:YES];
		colorMatrixFrame = kHueSatFrame;
		UIImageView *hueSatImage = [[UIImageView alloc] initWithFrame:colorMatrixFrame];
		[hueSatImage setImage:[UIImage imageNamed:kHueSatImage]];
		[self addSubview:hueSatImage];
		[self sendSubviewToBack:hueSatImage];
		[hueSatImage release];
		currentBrightness = kInitialBrightness;
		
		currentColor = [[UIColor alloc]init];
        
        //update recent colors
        [self updateSwatches];
	}
	return self;
}

- (void) setColor:(UIColor *)color {
    currentColor = color;
    currentHue = color.hue;
    currentSaturation = color.saturation;
    currentBrightness = color.brightness;
    CGPoint hueSatPosition;
    CGPoint brightnessPosition;
    hueSatPosition.x = (currentHue*kMatrixWidth)+kXAxisOffset;
    hueSatPosition.y = (1.0-currentSaturation)*kMatrixHeight+kYAxisOffset;
    brightnessPosition.x = (1.0+kBrightnessEpsilon-currentBrightness)*gradientView.frame.size.width;
    
    // Original input brightness code (from down below)
    // currentBrightness = 1.0-(position.x/gradientView.frame.size.width) + kBrightnessEpsilon;
    
    brightnessPosition.y = kBrightBarYCenter;
    [gradientView setTheColor:color];
    //[showColor setBackgroundColor:currentColor];
    showColor.swatchColor = currentColor;
    
    crossHairs.center = hueSatPosition;
    brightnessBar.center = brightnessPosition;
    
    //update recent colors
    [self updateSwatches];
} 


- (void) updateHueSatWithMovement : (CGPoint) position {

	currentHue = (position.x-kXAxisOffset)/kMatrixWidth;
	currentSaturation = 1.0 -  (position.y-kYAxisOffset)/kMatrixHeight;
	
	UIColor *forGradient = [UIColor colorWithHue:currentHue 
									saturation:currentSaturation 
									brightness: kInitialBrightness 
									alpha:1.0];
	
	[gradientView setTheColor:forGradient];
	[gradientView setupGradient];
	[gradientView setNeedsDisplay];

	currentColor  = [UIColor colorWithHue:currentHue 
									   saturation:currentSaturation 
									   brightness:currentBrightness
									   alpha:1.0];
	
	//[showColor setBackgroundColor:currentColor];
    showColor.swatchColor = currentColor;
    [showColor setNeedsDisplay];
}


- (void) updateBrightnessWithMovement : (CGPoint) position {
	
	currentBrightness = 1.0-(position.x/gradientView.frame.size.width) + kBrightnessEpsilon;
	
	UIColor *forColorView  = [UIColor colorWithHue:currentHue 
										saturation:currentSaturation 
										brightness:currentBrightness
											 alpha:1.0];
	
	//[showColor setBackgroundColor:forColorView];
    showColor.swatchColor = forColorView;
    [showColor setNeedsDisplay];
}

//Touch parts : 

// Scales down the view and moves it to the new position. 
- (void)animateView:(UIImageView *)theView toPosition:(CGPoint) thePosition
{
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:kAnimationDuration];
	// Set the center to the final postion
	theView.center = thePosition;
	// Set the transform back to the identity, thus undoing the previous scaling effect.
	theView.transform = CGAffineTransformIdentity;
	[UIView commitAnimations];	
}

- (UIColor *) swatchColor:(int) from{
    NSUserDefaults *saveColors = [NSUserDefaults standardUserDefaults];
    NSData *colorData= [saveColors objectForKey:[NSString stringWithFormat:@"lastColor%d",from]];
    if (colorData == nil) {
        //no color saved there, this means black is shown. so we will return black.
        return [UIColor blackColor];
    }
    UIColor *choosenColor = (UIColor*)[NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    
    //return color 
    return choosenColor;
}

-(void) dispatchTouchEvent:(CGPoint)position
{
	if (CGRectContainsPoint(colorMatrixFrame,position))
	{
		[self animateView:crossHairs toPosition: position];
		[self updateHueSatWithMovement:position];
		
	}
	else if (CGRectContainsPoint(gradientView.frame, position))
	{
		CGPoint newPos = CGPointMake(position.x,kBrightBarYCenter);
		[self animateView:brightnessBar toPosition: newPos];
		[self updateBrightnessWithMovement:position];
	}
    else if (CGRectContainsPoint(lastColor0.frame, position)) {
        //change to lastcolor0
        showColor.swatchColor = [self swatchColor:0];
        
    }
    else if (CGRectContainsPoint(lastColor1.frame, position)) {
        //change to lastcolor1
        showColor.swatchColor = [self swatchColor:1];
    }
    else if (CGRectContainsPoint(lastColor2.frame, position)) {
        //change to lastcolor2
        showColor.swatchColor = [self swatchColor:2];
    }
    else if (CGRectContainsPoint(lastColor3.frame, position)) {
        //change to lastcolor3
        showColor.swatchColor = [self swatchColor:3];
    }
    else if (CGRectContainsPoint(lastColor4.frame, position)) {
        //change to lastcolor4
        showColor.swatchColor = [self swatchColor:4];
    }
    
    [showColor setNeedsDisplay];
	
}


// Handles the start of a touch
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	
	for (UITouch *touch in touches) {
		[self dispatchTouchEvent:[touch locationInView:self]];
		}	
}

// Handles the continuation of a touch.
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{  

	for (UITouch *touch in touches){
		[self dispatchTouchEvent:[touch locationInView:self]];
	}
	
}

- (void)drawRect:(CGRect)rect {
    
	//CGFloat x = currentHue * kMatrixWidth;
	//CGFloat y = currentSaturation * kMatrixHeight;
	
	//crossHairs.center = CGPointMake(x,y);
	
	//x = currentBrightness * gradientView.frame.size.width;
	
	//brightnessBar.center = CGPointMake(x,kBrightBarYCenter);
	
	[gradientView setupGradient];
	[gradientView setNeedsDisplay];
	[self sendSubviewToBack:showColor];

}

//updates color swatches
-(void) updateSwatches{
    NSUserDefaults *saveColors = [NSUserDefaults standardUserDefaults];
    
    //lastcolor0
    NSData *colorData= [saveColors objectForKey:@"lastColor0"];
    UIColor *color;
    if (colorData!=nil) {
        //grabs color
        color = (UIColor*)[NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    }
    else
        color = [UIColor blackColor]; //If there is no color, black is shown.
    lastColor0.swatchColor = color;
    
    //lastcolor1
    colorData= [saveColors objectForKey:@"lastColor1"];
    if (colorData!=nil) {
        //grabs color
        color = (UIColor*)[NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    }
    else
        color = [UIColor blackColor]; //If there is no color, black is shown.
    lastColor1.swatchColor = color;
    
    //lastcolor2
    colorData= [saveColors objectForKey:@"lastColor2"];
    if (colorData!=nil) {
        //grabs color
        color = (UIColor*)[NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    }
    else
        color = [UIColor blackColor]; //If there is no color, black is shown.
    lastColor2.swatchColor = color;
    
    //lastcolor3
    colorData= [saveColors objectForKey:@"lastColor3"];
    if (colorData!=nil) {
        //grabs color
        color = (UIColor*)[NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    }
    else
        color = [UIColor blackColor]; //If there is no color, black is shown.
    lastColor3.swatchColor = color;
    
    //lastcolor4
    colorData= [saveColors objectForKey:@"lastColor4"];
    if (colorData!=nil) {
        //grabs color
        color = (UIColor*)[NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    }
    else
        color = [UIColor blackColor]; //If there is no color, black is shown.
    lastColor4.swatchColor = color;
    
    [lastColor0 setNeedsDisplay];
    [lastColor1 setNeedsDisplay];
    [lastColor2 setNeedsDisplay];
    [lastColor3 setNeedsDisplay];
    [lastColor4 setNeedsDisplay];
    
}
//get color shown
- (UIColor *) getColorShown {
    [self updateSwatches];
    return showColor.swatchColor;
	//return [UIColor colorWithHue:currentHue saturation:currentSaturation brightness:currentBrightness alpha:1.0];
}

- (void)dealloc {
    [lastColor0 release];
    [super dealloc];
	
}

@end
