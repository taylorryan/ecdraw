//
//  ExternalScreenController.h
//  ECDraw
//
//  Created by Ryan, Taylor on 6/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DrawArea;

@interface ExternalScreenController : UIViewController{
    DrawArea *drawingView;
}
@property (retain, nonatomic) IBOutlet DrawArea *drawingView;

@end
