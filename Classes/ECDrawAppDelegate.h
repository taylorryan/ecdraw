//
//  ECDrawAppDelegate.h
//  ECDraw
//
//  Created by Bianco, Anthony M on 6/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DrawAreaViewController.h"
#import "ECLine.h"
#import "ExternalScreenController.h"



@interface ECDrawAppDelegate : NSObject <UIApplicationDelegate > {
    UIWindow *window;
	NSInteger clientID;
    
    UIWindow *externalWindow;
    DrawArea *externalDrawArea;
	
}

@property (nonatomic, retain) IBOutlet UIWindow *window;

+ (ECDrawAppDelegate *)getInstance;
- (NSInteger)getClientID;
- (void)setClientID:(NSInteger)cid;

- (DrawArea *)getExternalDrawingArea;
@end

