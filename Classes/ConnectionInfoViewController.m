//
//  ConnectionInfoViewController.m
//  ECDraw
//
//  Created by Bianco, Anthony M on 6/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ConnectionInfoViewController.h"
#import <ifaddrs.h>
#import <arpa/inet.h>

static BOOL ipIsValid(NSString *ipAddressStr)
{
	int ipQuads[4];
	const char *ipAddress = [ipAddressStr cStringUsingEncoding:NSUTF8StringEncoding];
	
	sscanf(ipAddress, "%d.%d.%d.%d", &ipQuads[0], &ipQuads[1], &ipQuads[2], &ipQuads[3]);
	
	for (int quad = 0; quad < 4; quad++) {
		if ((ipQuads[quad] < 0) || (ipQuads[quad] > 255))
		{
			return NO;
		}
	}
	return YES;
}

static NSString* getIPAddress()
{
	NSString *address = @"No Internet!";
	struct ifaddrs *interfaces = NULL;
	struct ifaddrs *temp_addr = NULL;
	int success = 0;
	
	// retrieve the current interfaces - returns 0 on success
	success = getifaddrs(&interfaces);
	if (success == 0)
	{
		// Loop through linked list of interfaces
		temp_addr = interfaces;
		while(temp_addr != NULL)
		{
			if(temp_addr->ifa_addr->sa_family == AF_INET)
			{
				// Check if interface is en0 which is the wifi connection on the iPhone
				if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"])
				{
					// Get NSString from C String
//					address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    address = @"Not Connected";
				}
			}
			
			temp_addr = temp_addr->ifa_next;
		}
	}
	
	// Free memory
	freeifaddrs(interfaces);
	
	return address;
}

@implementation ConnectionInfoViewController

@synthesize activityIndicator;
@synthesize joinRoomButton;
@synthesize status;
@synthesize delegate;
@synthesize createRoomButton;
@synthesize ipAddress;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	
	ipAddress = getIPAddress();
	[deviceAddress setText:ipAddress];
	
	activityIndicator.hidesWhenStopped = YES;
	
	connectionAddressField.clearsOnBeginEditing = YES;
	connectionAddressField.delegate = self;
	connectionAddressField.placeholder = @"Enter address";
	
	
	//[connectionAddressField sendAction:@selector(connect:) to:self forEvent:nil];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        return;
    }
    
    NSString *temp = @"";
    
    if (alertView.tag == 1 || alertView.tag == 2) {
        
        
        temp = [[alertText.text stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"$" withString:@""];//remove spaces and '$'
        
        
        if ([temp isEqualToString:@""])
            return;
    }
    
    if (alertView.tag == 1) {
        //Create Room
        ipAddress = [[temp uppercaseString] retain];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Room's Password" message:@"Enter the room's password. Leave blank for no password.\n\n\n" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Submit",nil];
        //alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        passwordText = [[UITextField alloc] initWithFrame:CGRectMake(12,100, 260, 25)];
        [passwordText setBackgroundColor:[UIColor whiteColor]];
        passwordText.autocorrectionType = UITextAutocorrectionTypeNo;
        [alert addSubview:passwordText];
        alert.tag = 3;
        [alert show];
         [passwordText becomeFirstResponder];
        [alert release];
        //[self connect:kServerAddress newRoom:YES name:[temp uppercaseString]];
    }
    else if (alertView.tag == 2) {
        //Join Room
        [self connect:kServerAddress newRoom:NO name:[temp uppercaseString]];
    }
    else if (alertView.tag == 3) {
        //Password for room.
        
        temp = [[passwordText.text stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"$" withString:@""];
        
        if (temp != nil && ![temp isEqualToString:@""]) {
            //Encrypt password and create string to send to server
            temp = [NSString stringWithFormat:@"%@$%@",ipAddress,[passwordText.text sha1]];
        }
        else {
            temp = ipAddress;
        }
        //send string to server
        [self connect:kServerAddress newRoom:YES name:[temp uppercaseString]];
    }
    else if (alertView.tag == 4) {
        //just displayed warning about joining a room overwrites the drawings currently on the screen.
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Join Room" message:@"What room would you like to join?\n\n\n" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Submit",nil];
        //alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        alertText = [[UITextField alloc] initWithFrame:CGRectMake(12,85, 260, 25)];
        [alertText setBackgroundColor:[UIColor whiteColor]];
        alertText.autocorrectionType = UITextAutocorrectionTypeNo;
        [alert addSubview:alertText];
        alert.tag = 2;
        [alert show];
         [alertText becomeFirstResponder];
        [alert release];
    }
}
- (BOOL)connect:(NSString *)text newRoom:(BOOL)create name:(NSString *)name
{
	[activityIndicator startAnimating];
	self.view.userInteractionEnabled = NO;
	
	connectionAddress = text;
    NSRange range = [name rangeOfString:@"$"];
    if (range.length > 0) {
        ipAddress = [[name substringToIndex:range.location] retain];
	}
    else {
        ipAddress = [name retain];
    }
	[connectionAddressField resignFirstResponder];
	
    [status setText:@"Connecting..."];
    
    [delegate connectToHost:connectionAddress room:create name:name];

	
	return YES;
}

- (IBAction)createRoomPressed:(UIButton *)sender {
    if ([createRoomButton.titleLabel.text isEqualToString:@"Create Room"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Create Room" message:@"What should the room be called?\n\n\n" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Submit",nil];
        //alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        alertText = [[UITextField alloc] initWithFrame:CGRectMake(12,85, 260, 25)];
        [alertText setBackgroundColor:[UIColor whiteColor]];
        alertText.autocorrectionType = UITextAutocorrectionTypeNo;
        [alert addSubview:alertText];
        alert.tag = 1;
        [alert show];
        [alertText becomeFirstResponder];
        [alert release];
        
    }
    else if ([createRoomButton.titleLabel.text isEqualToString:@"Refresh"]){
        //[delegate closeConnection];
        [delegate refresh];
    }
}

- (IBAction)joinRoomPressed:(UIButton *)sender {
    if ([joinRoomButton.titleLabel.text isEqualToString:@"Join Room"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notice" message:@"When you join a room, all unsaved data will be lost. The room's session will overwrite your current session." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK",nil];
        alert.tag = 4;
        [alert show];
        [alert release];
    }
    else if ([joinRoomButton.titleLabel.text isEqualToString:@"Disconnect"]){
        [delegate closeConnection];
    }
}

- (void)connectionComplete
{
	[activityIndicator stopAnimating];
	self.view.userInteractionEnabled = YES;
	[status setText:ipAddress];
	[createRoomButton setTitle:@"Refresh" forState:UIControlStateNormal];
	[createRoomButton setTitle:@"Refresh" forState:UIControlStateHighlighted];
	[createRoomButton setTitle:@"Refresh" forState:UIControlStateDisabled];
	[createRoomButton setTitle:@"Refresh" forState:UIControlStateSelected];
    [joinRoomButton setTitle:@"Disconnect" forState:UIControlStateNormal];
	[joinRoomButton setTitle:@"Disconnect" forState:UIControlStateHighlighted];
	[joinRoomButton setTitle:@"Disconnect" forState:UIControlStateDisabled];
	[joinRoomButton setTitle:@"Disconnect" forState:UIControlStateSelected];
}

- (void)connectionFailed
{
	[activityIndicator stopAnimating];
	self.view.userInteractionEnabled = YES;
	[status setText:@"Connection failed"];
}

- (void)connectionEnded
{
	[status setText:@"Not Connected"];
	[createRoomButton setTitle:@"Create Room" forState:UIControlStateNormal];
	[createRoomButton setTitle:@"Create Room" forState:UIControlStateHighlighted];
	[createRoomButton setTitle:@"Create Room" forState:UIControlStateDisabled];
	[createRoomButton setTitle:@"Create Room" forState:UIControlStateSelected];
	[joinRoomButton setTitle:@"Join Room" forState:UIControlStateNormal];
	[joinRoomButton setTitle:@"Join Room" forState:UIControlStateHighlighted];
	[joinRoomButton setTitle:@"Join Room" forState:UIControlStateDisabled];
	[joinRoomButton setTitle:@"Join Room" forState:UIControlStateSelected];
    
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [self setJoinRoomButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

//UITextFieldDelegate implementations

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	//[self connect:textField.text];
	
	return YES;
}

- (void)dealloc {
    [joinRoomButton release];
    [super dealloc];
}


@end
