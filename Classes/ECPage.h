//
//  ECPage.h
//  ECDraw
//
//  Created by Bianco, Anthony M on 6/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ECLine.h"
#import "ECText.h"
#import "ECShape.h"
#import "ECImage.h"

@interface ECPage : NSObject <NSCoding> {
	NSMutableArray *lines;// Array of lines contained in the page
    NSMutableArray *undoActions;// Array of lines removed from DrawArea via Undo. Used for Redo.
	CGPoint	translate;// Keeps track of current translation received from pan gesture
    float pageScale;// Keeps track of the current scale factor
	NSInteger pageID, userElementID;
	UIImage* image;//reference to a CG Image
	bool hasImage;//YES if the page contains an image already, otherrwise NO
	NSInteger pageCount;
}

@property (nonatomic , readwrite) CGPoint translate;
@property (nonatomic, readwrite) float pageScale;
@property (nonatomic , readwrite) bool hasImage;
//@property (nonatomic , readwrite) UIImage *image;
@property (assign) NSInteger pageID;
@property (assign, readwrite) NSInteger userElementID;
@property BOOL gridIsActive;

- (ECPage *)init:(int)count;

- (void)drawPageWithContext:(CGContextRef) context;
- (void)encodeWithCoder:(NSCoder *)coder;
- (ECPage *)initWithCoder:(NSCoder *)coder;
- (bool)hasAnImage;
- (void)setImage:(UIImage *)img;
- (void)addToTranslate:(CGPoint)point;
- (CGPoint)getTranslate;
- (void)eraseDrawing;
- (void)eraseImage;
- (void)removeObjectFromPage:(id)object;
- (NSInteger)getPageID;

- (void)redo;
- (void)redoForClient:(NSInteger)cid;
- (void)undo;
- (void)undoForClient:(NSInteger)cid;
- (UIImage *)getImage;
- (void) updateClientID:(NSInteger)newID from:(NSInteger)oldID;

- (ECLine *)newLineWithColor:(UIColor *)setColor andSize:(CGFloat)setSize atPoint:(CGPoint)setStarte;
- (ECText *)newText:(NSString *)text withPoint:(CGPoint)point andSize:(CGFloat)setSize andColor:(UIColor *)setColor;
- (ECShape *)newShapeWithColor:(UIColor *)setColor andSize:(CGFloat)setSize andType:(ShapeType)setType atPoint:(CGPoint)setStart;
- (ECImage *) newImageWithImage:(UIImage *)source atPoint:(CGPoint)setStart;
- (void)addPoint:(CGPoint)point;
- (void)addDrawingObject:(DrawingObject *)item;
-(float)getPageScale;

- (DrawingObject *) shapeAtPoint:(CGPoint)point;

- (void) reducePageCount;
- (int) undoCount;
- (BOOL)deleteDrawingObject:(DrawingObject *)drawingObject;

@end
