//
//  DrawAreaViewController.h
//  ECDraw
//
//  Created by Bianco, Anthony M on 6/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h> 
#import "DrawArea.h"
#import "ConnectionInfoViewController.h"
#import "ColorPickerViewController.h"
#import "TextViewController.h"
#import "ECLine.h"
#import "Server.h"
#import "ECText.h"
#import "Connection.h"
#import "NSString+SHA1.h"
#import "HelpView.h"
#import "ConnectionInfoViewControllerDelegate.h"
#import "ShapePickerController.h"
#import "ShapeTableController.h"
#import "FileLoadingViewController.h"


@interface DrawAreaViewController : UIViewController <UIAlertViewDelegate, TextViewControllerDelegate, ConnectionInfoViewControllerDelegate, UIPopoverControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, ColorPickerViewControllerDelegate,  ServerDelegate, ConnectionDelegate,NSNetServiceBrowserDelegate> {
	
	UIImageView *imageView;
	DrawArea *drawingView;
    HelpView *helpOverlay;
    BOOL helpIsActive;
	UIToolbar *toolBar;
    BOOL gridIsActive;
	BOOL popoverIsActive;
	IBOutlet UISegmentedControl* sizeSegmentedControl;
	IBOutlet UIBarButtonItem* colorPickerLauncher;
	IBOutlet UIBarButtonItem* shapeLauncher;
	IBOutlet UIButton* colorPickerButton;
    IBOutlet UIBarButtonItem *redoButton;
    IBOutlet UIBarButtonItem *undoButton;
    IBOutlet UIBarButtonItem *gridButton;
    IBOutlet UIBarButtonItem *saveButton;
    IBOutlet UIBarButtonItem *homeButton;
    IBOutlet UIBarButtonItem *connectButton;
	TextViewController* textController;
	ConnectionInfoViewController* connectionController;
	
	UIPopoverController *popOver;
	UILabel *totalPages, *currentPage;
    
    UITextField *alertText;
	
	NSInteger clientID;
	Server *localServer;
	Connection *localConnection;
	NSMutableSet *clientSet;
	BOOL host;
    
    BOOL isLocalClient;
    NSString *roomName;
    NSMutableArray *servers;
    NSTimer *searchTimer;
    NSTimer *localTimer;
    NSNetServiceBrowser *serviceBrowser;
    BOOL tryToJoin;
	
	int size[4];
	UIColor *color[3];
}

@property BOOL popoverIsActive;
@property (nonatomic, retain) IBOutlet UILabel *totalPages;
@property (nonatomic, retain) IBOutlet UILabel *currentPage;
@property (nonatomic, retain) IBOutlet DrawArea *drawingView;
@property (nonatomic, retain) IBOutlet UIImageView *imageView;
@property (nonatomic, retain) IBOutlet UIToolbar *toolBar;
@property (nonatomic, retain) IBOutlet UIPopoverController *popOver;


@property (assign) NSInteger clientID;

- (IBAction)savePressed:(UIBarButtonItem *)sender;
- (IBAction)erasePressed:(UIBarButtonItem *)sender;
- (IBAction)undoPressed:(UIBarButtonItem *)sender;
- (IBAction)redoPressed:(UIBarButtonItem *)sender;
- (IBAction)colorPickerPressed:(id)sender;
- (IBAction)imagePressed:(UIBarButtonItem *)sender;
- (IBAction)connectionPressed:(UIBarButtonItem *)sender;
- (IBAction)modeSwitch:(id)sender;
- (IBAction)sizePressed:(id)sender;
- (IBAction)switchPagePressed:(id)sender;
- (IBAction)homePressed:(UIBarButtonItem *)sender;
- (IBAction)gridPressed:(UIBarButtonItem *)sender;
- (IBAction)helpPressed:(UIBarButtonItem *)sender;
- (void)textFieldPressed:(CGPoint) location withTranslation:(CGPoint)translation;
- (void)launchShapePickerController:(UIBarButtonItem *)sender;
- (void)segmentedControlTextImages;
- (void)segmentedControlDrawingImages;
- (void)segmentedControlErasingImages;

- (void)sendDrawingObject:(DrawingObject *)sendItem;
- (void)sendRedoForPage:(NSInteger)pid;
- (void)sendUndoForPage:(NSInteger)pid;
- (void)sendClearForPage:(NSInteger)pid;
- (void)sendClearAllForPage:(NSInteger)pid;
- (void)sendDeleteForPage:(NSInteger)pid;
- (void)sendRequestPage;
- (void)sendAddPage;
- (void)sendImage:(UIImage *)img forPage:(NSInteger)page;
- (void)sendRefreshPageRequestForPage:(NSInteger)pid;
- (void)sendRefreshPage:(NSMutableArray *)pageToSend;
- (void)sendDeleteForObject:(DrawingObject *)deleteItem;

- (void)setImage:(UIImage *)img;

- (void)textViewControllerDidCancel:(TextViewController *) viewController;
- (void)textViewController:(TextViewController *)viewController didFinishWithText:(NSString *)text;

- (void)update;
- (void)loadSession:(NSString *)file;

- (void)closeConnection;

////    server delegate methods    ////
// Server has been terminated because of an error
- (void) serverFailed:(Server*)server reason:(NSString*)reason;

// Server has accepted a new connection and it needs to be processed
- (void) handleNewConnection:(Connection*)connection;

////    connection delegate methods    ////
- (void) connectionAttemptFailed:(Connection*)connection;
- (void) connectionTerminated:(Connection*)connection;
- (void) receivedNetworkPacket:(NSData *)packet viaConnection:(Connection*)connection withPacketInfo:(int *)packetInfo;
- (void) connectionStatusChange:(NSString *) message;

////    connectionInfoViewController delegate methods    ////
- (BOOL)connectToHost:(NSString *)hostAddr room:(BOOL)create name:(NSString*)name;

@end
