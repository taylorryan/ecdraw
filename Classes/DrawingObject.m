//
//  DrawingObject.m
//  ECDraw
//
//  Created by Bianco, Anthony M on 7/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DrawingObject.h"
#import "ECDrawAppDelegate.h"

@implementation DrawingObject

@synthesize type;
@synthesize size;
@synthesize color;
@synthesize pageID;
@synthesize elementID;
@synthesize clientID;

- (DrawingObject *)initWithPage:(NSInteger)pid
{
	self = [super init];
	
	pageID = [[DrawArea getInstance] currentPageID];
	
	clientID = [[ECDrawAppDelegate getInstance] getClientID];
	
	return self;
}

- (void)drawInContext:(CGContextRef)context
{
	//do nothing should be overwritten by subclasses
}

- (DrawingObject *)initWithCoder:(NSCoder *)coder
{
	self = [super init];
	elementID = [coder decodeIntegerForKey:@"elementID"];
	pageID = [coder decodeIntegerForKey:@"pageID"];
	clientID = [coder decodeIntegerForKey:@"clientID"];
	color = [[coder decodeObjectForKey:@"color"] retain];
	size = [coder decodeFloatForKey:@"size"];
	
	return self;
}

//Encodes objects into the provided NSCoder
- (void)encodeWithCoder:(NSCoder *)coder
{		
	[coder encodeInteger:elementID forKey:@"elementID"];
	[coder encodeInteger:pageID forKey:@"pageID"];
	[coder encodeInteger:clientID forKey:@"clientID"];
	[coder encodeObject:color forKey:@"color"];
	[coder encodeFloat:size forKey:@"size"];

}

-(BOOL)containsPoint:(CGPoint)point{
    // Should be implemented by child objects
    return NO;
}
-(BOOL)moveToPoint:(CGPoint)point{
    // Should be implemented by child objects
    return NO;
}
@end
