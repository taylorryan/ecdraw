//
//  DrawingObject.h
//  ECDraw
//
//  Created by Bianco, Anthony M on 7/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
	LINE_TYPE,
	TEXT_TYPE,
	SHAPE_TYPE,
	OVAL_TYPE,
    IMAGE_TYPE
} DrawingObjectTypes;



@interface DrawingObject : NSObject <NSCoding> {

	int type;
	NSInteger pageID;
	NSInteger elementID;
	NSInteger clientID;
	
	CGFloat size;
	UIColor* color;
}

@property (readonly) int type;
@property (assign) CGFloat size;
@property (assign) UIColor *color;
@property (assign) NSInteger pageID;
@property (assign) NSInteger elementID;
@property (assign) NSInteger clientID;

- (DrawingObject *)initWithPage:(NSInteger)pid;
- (void)drawInContext:(CGContextRef)context;
- (DrawingObject *)initWithCoder:(NSCoder *)coder;
- (void)encodeWithCoder:(NSCoder *)coder;

- (BOOL)containsPoint:(CGPoint)point;
- (BOOL)moveToPoint:(CGPoint)point;

@end
