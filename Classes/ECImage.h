//
//  ECImage.h
//  ECDraw
//
//  Created by Ryan, Taylor on 5/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DrawingObject.h"
#import "ECPoint.h"

@interface ECImage : DrawingObject <NSCoding>{
    UIImage *image;
    CGPoint location;
    ECPoint *lastOffset;
}

- (ECImage *)initWithImage:(UIImage *)source atPoint:(CGPoint)point andPageNumber:(NSInteger)pid andElementID:(NSInteger)eid;

- (ECImage *)initWithCoder:(NSCoder *)coder;
- (void)encodeWithCoder:(NSCoder *)coder;

- (void)drawInContext:(CGContextRef)context;

@end
