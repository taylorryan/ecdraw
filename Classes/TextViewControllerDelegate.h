//
//  TextViewControllerDelegate.h
//  ECDraw
//
//  Created by Bianco, Anthony M on 7/14/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TextViewController;

@protocol TextViewControllerDelegate

- (void)textViewControllerDidCancel:(TextViewController *) viewController;
- (void)textViewController:(TextViewController *)viewController didFinishWithText:(NSString *)text;

@end
