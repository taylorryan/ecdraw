//
//  Line.h
//  QuartzDraw
//
//  Created by Bianco, Anthony M on 6/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DrawingObject.h"


@interface ECLine : DrawingObject <NSCoding> {
	NSMutableArray *points;//points that make up a line
}

- (void)encodeWithCoder:(NSCoder *)coder;
- (ECLine *)initWithCoder:(NSCoder *)coder;
- (ECLine *)initWithColor:(UIColor *)setColor andSize:(CGFloat)setSize atPoint:(CGPoint)setStart andPageNumber:(NSInteger)pid andElementID:(NSInteger)eid;
- (void)addPoint:(CGPoint) point;
//- (void)drawInContext:(CGContextRef)context withTranslation:(CGPoint)translate;
- (NSInteger)pointCount;

@end
