//
//  DrawAreaViewController.h
//  ECDraw
//
//  Created by Bianco, Anthony M on 6/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DrawArea.h"


@interface DrawAreaViewController : UIViewController {
	
	DrawArea *drawingView;
	UIToolbar *toolbar;
}

@property (retain) DrawArea *drawingArea;
@property (retain) UIToolbar *toolbar;

@end
