//
//  ECSegmentedControl.m
//  ECDraw
//
//  Created by Bianco, Anthony M on 8/4/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ECSegmentedControl.h"


@implementation ECSegmentedControl


// iOS 4 and under support

- (void)setSelectedSegmentIndex:(NSInteger)toValue {
	// Trigger UIControlEventValueChanged even when re-tapping the selected segment.
    
    //Grab system version
    int version = [[[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."] objectAtIndex:0] intValue];
    
	if (version < 5 && toValue==self.selectedSegmentIndex) {
		[self sendActionsForControlEvents:UIControlEventValueChanged];
	}
	[super setSelectedSegmentIndex:toValue];        
}

// iOS 5 and up support

// Saves old selected index, then allow super class to grab new select index.
// If they are the same, it sends valueChanged to allow the segment to act like
// it was just tapped for the first time.
-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    //Grab system version
    int version = [[[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."] objectAtIndex:0] intValue];
    
    if (version >= 5) {
        
        int oldValue = self.selectedSegmentIndex;
        [super touchesBegan:touches withEvent:event];
        if ( oldValue == self.selectedSegmentIndex )
            [self sendActionsForControlEvents:UIControlEventValueChanged];
    }
    else
        [super touchesBegan:touches withEvent:event];
}

@end
