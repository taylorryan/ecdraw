//
//  Socket.h
//  ECDraw
//
//  Created by Ryan, Taylor on 5/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SocketDelegate<NSObject>

@optional
-(void)messageReceived:(NSString*)message;

@end

@interface Socket : NSObject <NSStreamDelegate> {
	NSInputStream	*inputStream;
	NSOutputStream	*outputStream;
    id <SocketDelegate> delegate;
}

@property (nonatomic, retain) NSInputStream *inputStream;
@property (nonatomic, retain) NSOutputStream *outputStream;
@property (nonatomic, assign) id delegate;
- (id) initWithAddress:(NSString*)addr port:(int)p;
- (void) sendMessage:(NSString*)s;
- (void) messageReceived:(NSString *)message;

@end

