//
//  ShapeTableController.m
//  ECDraw
//
//  Created by Ryan, Taylor on 6/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ShapeTableController.h"

@interface ShapeTableController (){
@private
    NSArray *shapes;
}

@end

@implementation ShapeTableController

@synthesize controller;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    shapes = [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Shapes" ofType:@"plist"]];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return @"2D Shapes";
    }
    else {
        return @"3D Shapes";
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    if (section == 0) {
        return 9;
    }
    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
    
    if ([indexPath section] == 0) {
        //2D Shapes
        NSDictionary *shape = [shapes objectAtIndex:[indexPath row]];
        [cell.textLabel setText: [shape objectForKey:@"Name"] ];
        [cell.imageView setImage:[UIImage imageNamed:[shape objectForKey:@"Image"]]];
    }
    else {
        //3D Shapes
        int row = [indexPath row] + 9;
        NSDictionary *shape = [shapes objectAtIndex:row];
        [cell.textLabel setText: [shape objectForKey:@"Name"] ];
        [cell.imageView setImage:[UIImage imageNamed:[shape objectForKey:@"Image"]]];
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int row = [indexPath row];
    if ([indexPath section] == 1) {
        row +=9;
    }
    switch (row) {
        case 0:
            [controller shapePickerDidFinishWithShape: SQUARE_SHAPE];
            break;
        case 1:
            [controller shapePickerDidFinishWithShape: RECTANGLE_SHAPE];
            break;
        case 2:
            [controller shapePickerDidFinishWithShape: CIRCLE_SHAPE];
            break;
        case 3:
            [controller shapePickerDidFinishWithShape: OVAL_SHAPE];
            break;
        case 4:
            [controller shapePickerDidFinishWithShape: RTRIANGLE_SHAPE];
            break;
        case 5:
            [controller shapePickerDidFinishWithShape: ETRIANGLE_SHAPE];
            break;
        case 6:
            [controller shapePickerDidFinishWithShape: HLINE_SHAPE];
            break;
        case 7:
            [controller shapePickerDidFinishWithShape: VLINE_SHAPE];
            break;
        case 8:
            [controller shapePickerDidFinishWithShape: DLINE_SHAPE];
            break;
        case 9:
            [controller shapePickerDidFinishWithShape: CUBE_3D_SHAPE];
            break;
        case 10:
            [controller shapePickerDidFinishWithShape: CUBOID_3D_SHAPE];
            break;
        case 11:
            [controller shapePickerDidFinishWithShape: PYRAMID_3D_SHAPE];
            break;
        case 12:
            [controller shapePickerDidFinishWithShape: PRISM_3D_SHAPE];
            break;
        case 13:
            [controller shapePickerDidFinishWithShape: CONE_3D_SHAPE];
            break;
        case 14:
            [controller shapePickerDidFinishWithShape: CYLINDER_3D_SHAPE];
            break;
        case 15:
            [controller shapePickerDidFinishWithShape: SPHERE_3D_SHAPE];
            break;
        default:
            break;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
