//
//  HelpView.h
//  ECDraw
//
//  Created by Ryan, Taylor on 6/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DrawAreaViewController;

@interface HelpView : UIView{
    int helpSlide;
    UILabel *description;
    CGPoint labelPosition,arrowPosition;
    UIImageView *arrowView;
}
@property (assign) DrawAreaViewController *controller;

-(void) updateHelpSlide;
-(void) resetSlides;

@end
