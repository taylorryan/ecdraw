//
//  ConnectionInfoViewController.h
//  ECDraw
//
//  Created by Bianco, Anthony M on 6/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "ECDrawAppDelegate.h"
#import "ConnectionInfoViewControllerDelegate.h"
#import "NSString+SHA1.h"

@interface ConnectionInfoViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate> {
	id<ConnectionInfoViewControllerDelegate> delegate;
	
	UILabel *deviceAddress,*status;
	UITextField *connectionAddressField;
	UIActivityIndicatorView *activityIndicator;
	UIButton *createRoomButton;
	NSString *ipAddress;
	NSString *connectionAddress;
    
    UITextField *alertText;
    UITextField *passwordText;
}

@property (retain, nonatomic) IBOutlet UIButton *joinRoomButton;
@property (nonatomic, retain) IBOutlet UILabel *status;
@property (nonatomic, retain) IBOutlet UIButton *createRoomButton;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (retain, nonatomic) NSString *ipAddress;

@property (nonatomic,retain) id<ConnectionInfoViewControllerDelegate> delegate;

- (BOOL)connect:(NSString *)text newRoom:(BOOL)create name:(NSString *)name;
- (IBAction)createRoomPressed:(UIButton *)sender;
- (IBAction)joinRoomPressed:(UIButton *)sender;

- (void)connectionFailed;
- (void)connectionComplete;
- (void)connectionEnded;

@end
