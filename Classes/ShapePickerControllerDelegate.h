//
//  ConnectionViewControllerDelegate.h
//  ECDraw
//
//  Created by Bianco, Anthony M on 8/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol ShapePickerControllerDelegate

- (void)shapePickerDidFinishWithShape:(int)type;

@end
