//
//  DrawArea.m
//  QuartzDraw
//
//  Created by Bianco, Anthony M on 6/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DrawArea.h"
#import "ECPage.h"
#import "ECDrawAppDelegate.h"

static DrawArea* instance;


@implementation DrawArea

@synthesize pageCount;
@synthesize currentPageIndex;
@synthesize userText;
@synthesize controller;
@synthesize mode;
@synthesize isMainScreen;
@synthesize externalScreenAttached;

-(void)setNeedsDisplay{
    
    
    if (isMainScreen) {
        NSMutableArray *newPages = [pages retain];
        [[[ECDrawAppDelegate getInstance] getExternalDrawingArea] replacePageWith:newPages];
        [newPages release];
        
        [[ECDrawAppDelegate getInstance] getExternalDrawingArea].currentPageIndex = currentPageIndex;
    }
    
    [super setNeedsDisplay];
}

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
		//set all instance variables to initial values
        pages = [[NSMutableArray alloc] init];        
        [pages addObject:[[ECPage alloc] init:1]];
        
        instance = self;
        
        pageCount = 1;
        currentPageIndex = 0;
        
        currentLine = [ECLine alloc];

        
    }
    
    return self;
}

+ (DrawArea *)getInstance
{
	return instance;
}

- (id)initWithCoder:(NSCoder *)aDecoder//called when view is initialized
{
	[super initWithCoder:aDecoder];
	
	//set all instance variables to initial values
	pages = [[NSMutableArray alloc] init];
	currentSize = 2.0;
	currentColor = [UIColor blackColor];
	
	[pages addObject:[[ECPage alloc] init:1]];
	
	instance = self;
	
	pageCount = 1;
	currentPageIndex = 0;
	
	currentLine = [ECLine alloc];
	
	//add pan gesture recognizer to view
	UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc]
										  initWithTarget:self action:@selector(pan:)];
	
	panGesture.minimumNumberOfTouches = 2;
	
	[self addGestureRecognizer:panGesture];
	[panGesture release];
	
	mode = DRAW_MODE;
	
	
	//pinch gesture recognizer
	UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(handlePinchGesture:)];
    [self addGestureRecognizer:pinchGesture];
    [pinchGesture release];
	
    //long press gesture recognizer
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture:)];
    longPressGesture.minimumPressDuration = 0.75;
    longPressGesture.cancelsTouchesInView = YES;
    [self addGestureRecognizer:longPressGesture];
    [longPressGesture release];
    
	return self;
    
}

//returns the current mode
- (int)getMode
{
	return mode;
}

- (void)setModeTo:(int)set
{
	mode=set;
}

- (void)drawRect:(CGRect)rect {
    // Drawing code.
	
	CGContextRef context = UIGraphicsGetCurrentContext();//get contextref for drawing
	
	CGContextBeginTransparencyLayer(context, NULL);
    
	[[pages objectAtIndex:currentPageIndex] drawPageWithContext:context];
    
	CGContextEndTransparencyLayer(context);
    
}

- (void)refreshImage
{
	if([[pages objectAtIndex:currentPageIndex] hasImage])
	{
		[controller setImage:[[pages objectAtIndex:currentPageIndex] getImage]];
	}
	else
	{
		[controller setImage:nil];
	}
}
#pragma mark -
#pragma mark Erase

- (void)erasePageImage
{
	[[pages objectAtIndex:currentPageIndex] eraseImage];
	
	[self setTranslationToZero];
	
	[self setNeedsDisplay];
}

- (void)erasePageImageForPageID:(NSInteger)pid
{
	for(int i = [pages count] -1; i >= 0; i--)
	{
		if([[pages objectAtIndex:i] getPageID] == pid)
		{
			[[pages objectAtIndex:i] eraseImage];
			[self setNeedsDisplay];
			[controller update];
			return;
		}
	}
    
}

- (void)erasePageDrawing
{
	[[pages objectAtIndex:currentPageIndex] eraseDrawing];
	
	[self setTranslationToZero];
	
	[self setNeedsDisplay];
}

- (void)erasePageDrawingForPageID:(NSInteger)pid
{
	for(int i = [pages count] -1; i >= 0; i--)
	{
		if([[pages objectAtIndex:i] getPageID] == pid)
		{
			[[pages objectAtIndex:i] eraseDrawing];
			[self setNeedsDisplay];
			[controller update];
			return;
		}
	}
}

- (void)removePageForPageID:(NSInteger)pid
{
	if( pageCount > 1)
	{
		for(int i = [pages count] - 1; i >= 0; i--)
		{
			if([[pages objectAtIndex:i] getPageID] == pid)
			{
				[pages removeObjectAtIndex:i];
				pageCount--;
			}
		}
	}
	else {
		if([[pages objectAtIndex:0] getPageID] == pid)
		{
			[self erasePageDrawing];
			[self erasePageImage];
		}
	}
	
	if(currentPageIndex > pageCount - 1)
	{
		currentPageIndex--;
	}
	
	[self refreshImage];
	
	[self setNeedsDisplay];
    
}

-(void)removeAllPages{
//    [pages removeAllObjects];
//    [pages addObject:[[ECPage alloc] init]];
    [pages release];
    pages = [[NSMutableArray alloc] init];    
    pageCount = 1;
    [pages addObject:[[ECPage alloc] init:pageCount]];
    
    currentPageIndex = 0;
    
    [self setNeedsDisplay];
    [controller update];
}

-(void)deleteObject:(DrawingObject *)object
{
    [[pages objectAtIndex:currentPageIndex] deleteDrawingObject:object];
    [self setNeedsDisplay];
}

#pragma mark -
#pragma mark Switch Pages
- (void)nextPage
{
	if(currentPageIndex < (pageCount - 1))
	{
		currentPageIndex++;
		
		[self refreshImage];
		
		[self setNeedsDisplay];
	}
}

- (void)previousPage
{
	if(currentPageIndex > 0)
	{
		currentPageIndex--;
		
		[self refreshImage];
		
		[self setNeedsDisplay];
	}
}
#pragma mark Set Size, Color, Translation

- (void)setSize:(CGFloat)size
{
	currentSize = size;
}

- (void)setColorToUIColor:(UIColor *)color
{
	currentColor = color;
}

- (void)setColorToClear
{
	currentColor = [UIColor clearColor];
}

- (void)setTranslationToZero
{
	[[pages objectAtIndex:currentPageIndex] setTranslate:CGPointZero];
	
	[self setNeedsDisplay];
    
}
#pragma mark -
#pragma mark Add

- (void)addPage
{
    
    pageCount++;
	[pages addObject:[[ECPage alloc] init:pageCount]];
	
	currentPageIndex = pageCount - 1;
    
	[self refreshImage];
	
	[self setNeedsDisplay];
	[controller update];
}

//given a UIImage save it so it can be drawn
- (void)addImage:(UIImage *)img
{
	//self.backgroundColor = [[UIColor alloc] initWithPatternImage:img];
	
    pickerImage = [img retain];
    
    float scale = [[pages objectAtIndex:currentPageIndex] getPageScale];
    
    location = CGPointMake(512/scale, 384/scale);
    CGPoint translation = [[pages objectAtIndex:currentPageIndex] getTranslate];
    location.x -= translation.x*scale;
    location.y -= translation.y*scale;
    
    //	[[pages objectAtIndex:currentPageIndex] setImage:img];
    
    ECImage *newImage = [[pages objectAtIndex:currentPageIndex] newImageWithImage:img atPoint:location];
    
    //	[controller sendImage:img forPage:[[pages objectAtIndex:currentPageIndex] getPageID]];
    [controller sendDrawingObject:newImage];
    
    [newImage release];
    
    [self setNeedsDisplay];
    
}

- (void)addImage:(UIImage *)img forPage:(NSInteger)pid
{
	for(int i = [pages count] -1; i >= 0; i--)
	{
		if([[pages objectAtIndex:i] getPageID] == pid)
		{
			[[pages objectAtIndex:i] setImage:img];
			
			[self refreshImage];//update imageview to have correct image
			return;
		}
	}
    
}
- (void)addDrawingObjectToPage:(DrawingObject *)item
{
    
    for(int i = [pages count] -1; i >= 0; i--)
	{
		if([[pages objectAtIndex:i] getPageID] == item.pageID)
		{
			[[pages objectAtIndex:i] addDrawingObject:item];
            
            [self setNeedsDisplay];
			return;
		}
	}
    
	
}
#pragma mark -
#pragma mark Undo/Redo
- (void)redo{
    [controller sendRedoForPage:[[pages objectAtIndex:currentPageIndex] getPageID]];
    
    [[pages objectAtIndex:currentPageIndex] redo];
        
    [self setNeedsDisplay];    
}

- (void)redoForClient:(NSInteger)cid onPage:(NSInteger)pid{
    for(int i = [pages count]-1; i>=0; i --)
	{
		if([[pages objectAtIndex:i] pageID] == pid)
		{
			[[pages objectAtIndex:i] redoForClient:cid];
			
			[self setNeedsDisplay];
            
			return;
		}
        
	}
}

- (void)undo
{	
	[controller sendUndoForPage:[[pages objectAtIndex:currentPageIndex] getPageID]];
	
    [[pages objectAtIndex:currentPageIndex] undo];
    
	[self setNeedsDisplay];
}

- (void) undoForClient:(NSInteger)cid onPage:(NSInteger)pid
{
	for(int i = [pages count]-1; i>=0; i --)
	{
		if([[pages objectAtIndex:i] pageID] == pid)
		{
			[[pages objectAtIndex:i] undoForClient:cid];
			
			[self setNeedsDisplay];
            
			return;
		}
        
	}
	
}

-(int)undoCount{
    return [[pages objectAtIndex:currentPageIndex] undoCount];
}

#pragma mark -

-(void)showGrid
{
    ECPage *page = [pages objectAtIndex:currentPageIndex];
    page.gridIsActive = !page.gridIsActive;    
    
    [self setNeedsDisplay];
}

- (void)replacePageWith:(NSMutableArray *)replacementPages
{
    for (ECPage *page in pages) {
        for (ECPage *replacement in replacementPages) {
            if ([page getPageID] == [replacement getPageID]) {
                //'page' is going to be replaced by 'replacement'
                
                //transfer userelementid to replacement page
                replacement.userElementID = page.userElementID;
                
                break;
            }
        }
    }
	
    if (replacementPages != pages){
        [pages removeAllObjects];
        pages = [replacementPages retain];
    }
    
    pageCount = [pages count];
    if (currentPageIndex >= pageCount) {
        currentPageIndex--;
    }
	
	[self setNeedsDisplay];
    
    
    
}

- (void)sendPages
{
	[controller sendRefreshPage:pages];
    
}

- (void)savePages:(NSString *) filename
{
    NSString *path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:filename];
    BOOL result = [NSKeyedArchiver archiveRootObject:pages toFile:path];
    
    if (!result) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"There was a problem saving the session." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
}
- (void)loadPages:(NSString *)filename
{
    NSString *path = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:filename];
    NSMutableArray *newPages = [[NSKeyedUnarchiver unarchiveObjectWithFile:path] retain];
    pageCount = [newPages count];
    [self replacePageWith:newPages];
    [newPages release];
}

- (void)updateClientID:(NSInteger)newID from:(NSInteger)oldID{
    for (ECPage *page in pages) {
        [page updateClientID:(NSInteger)newID from:(NSInteger)oldID];
    }
}

- (NSInteger)currentPageID
{
	return [[pages objectAtIndex:currentPageIndex] getPageID];
}

//handles a two (or more) finger panning gesture
- (void)pan:(UIPanGestureRecognizer *)sender
{
	if((sender.state == UIGestureRecognizerStateChanged) || 
	   (sender.state == UIGestureRecognizerStateEnded)){
        
		CGPoint temp;
		temp = [sender translationInView:self];
        
		[[pages objectAtIndex:currentPageIndex] addToTranslate:temp];
		
		[sender setTranslation:CGPointZero inView:self];
		
		[self setNeedsDisplay];
		
	}
    
}

- (ECText *)addText:(NSString *)text
{
	
	ECText *addedText = [[pages objectAtIndex:currentPageIndex] newText:text withPoint:location andSize:currentSize andColor:currentColor];
    
	[self setNeedsDisplay];
	
	
	return addedText;
}

- (ECPage *)getCurrentPage
{
	return [pages objectAtIndex:currentPageIndex];
}
#pragma mark -
#pragma mark Touch Handling
// Handles the start of a touch
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    
    UITouch* touch = [[event touchesForView:self] anyObject];
    
    float scale = [[pages objectAtIndex:currentPageIndex] getPageScale];
    
    location = [touch locationInView:self];
    location.x /=scale;
    location.y /=scale;
    
    CGPoint translation = [[pages objectAtIndex:currentPageIndex] getTranslate];
    
    location.x -= translation.x*scale;
    location.y -= translation.y*scale;
    
    if(mode == TEXT_MODE)
    {
        //CGRect textFrame = CGRectMake(location.x, location.y, 80, 20);
        //[[userText alloc] initWithFrame:textFrame];
        
        CGPoint locationWithScale;
        locationWithScale.x = (location.x + (translation.x*scale))*scale  - 0.5;
        locationWithScale.y = (location.y + (translation.y*scale))*scale  - 0.5;
        
        [controller textFieldPressed:locationWithScale withTranslation:translation];
        
    }
    else if(mode == SHAPE_MODE)
    {
        currentActiveShape = [[pages objectAtIndex:currentPageIndex] newShapeWithColor:currentColor 
                                                                               andSize:currentSize 
                                                                               andType:currentShapeType 
                                                                               atPoint:location];
        currentActiveShape.endPoint = location;
    }
    else if (mode == SCROLL_MODE) {
        
    }
    else if (mode == MOVE_MODE) {
        movingObject = [[pages objectAtIndex:currentPageIndex] shapeAtPoint:location];
        
        if (movingObject.color == [UIColor clearColor]) {
            movingObject = nil;
        }
    }
    else //LINE_MODE or ERASE_MODE
    {
        currentLine = [[pages objectAtIndex:currentPageIndex] newLineWithColor:currentColor 
                                                                       andSize:currentSize 
                                                                       atPoint:location];//add a line with starting point at touch
        
//        [controller sendDrawingObject:currentLine];
    }
	
	
	[self setNeedsDisplay];
}

// Handles the continuation of a touch.
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{  
	if(mode == TEXT_MODE || mode == SCROLL_MODE)
	{
        //nothing to do if in text mode
		return;
    }
    
    UITouch* touch = [[event touchesForView:self] anyObject];
    
    location = [touch locationInView:self];
    location.x /=[[pages objectAtIndex:currentPageIndex] getPageScale];
    location.y /=[[pages objectAtIndex:currentPageIndex] getPageScale];
    
    CGPoint translation = [[pages objectAtIndex:currentPageIndex] getTranslate];
    
    location.x -= translation.x*[[pages objectAtIndex:currentPageIndex] getPageScale];
    location.y -= translation.y*[[pages objectAtIndex:currentPageIndex] getPageScale];
    
    if(mode == DRAW_MODE || mode == ERASE_MODE)
    {
        [currentLine addPoint:location];
        if([currentLine pointCount] % 4 == 0)
        {
//            [controller sendDrawingObject:currentLine];
        }
    }
    else if(mode == SHAPE_MODE)
    {
        currentActiveShape.endPoint = location;
    }
    else if (mode == MOVE_MODE && movingObject) {
        [movingObject moveToPoint:location];
    }
	
	
	
	[self setNeedsDisplay];
}

// Handles the end of a touch event or when the touch is a tap.
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
	if(mode == TEXT_MODE || mode == SCROLL_MODE)
	{
        //nothing to do if in text mode
		return;
    }
    
    UITouch*	touch = [[event touchesForView:self] anyObject];
    
    location = [touch previousLocationInView:self];
    location.x /=[[pages objectAtIndex:currentPageIndex] getPageScale];
    location.y /=[[pages objectAtIndex:currentPageIndex] getPageScale];
    
    CGPoint translation = [[pages objectAtIndex:currentPageIndex] getTranslate];
    
    location.x -= translation.x*[[pages objectAtIndex:currentPageIndex] getPageScale];
    location.y -= translation.y*[[pages objectAtIndex:currentPageIndex] getPageScale];
    
    if(mode == DRAW_MODE || mode == ERASE_MODE)
    {
        [currentLine addPoint:location];
        
        
        [controller sendDrawingObject:currentLine];
        
    }
    else if(mode == SHAPE_MODE)
    {
        currentActiveShape.endPoint = location;
        [controller sendDrawingObject:currentActiveShape];
    }
    else if (mode == MOVE_MODE && movingObject) {
        [movingObject moveToPoint:location];
        [controller sendDrawingObject:movingObject];
    }
	
	[self setNeedsDisplay];
	currentLine = nil;
	currentActiveShape = nil;
    movingObject = nil;
}


- (void)dealloc {
	[pages release];
    pages = nil;
    
    [super dealloc];
}

//handle pinch gesture
- (void)handlePinchGesture:(UIPinchGestureRecognizer *)sender{
    
    if (sender.state == UIGestureRecognizerStateChanged || 
        (sender.state == UIGestureRecognizerStateEnded)){
        
        ECPage *page = [pages objectAtIndex:currentPageIndex];
        page.pageScale = page.pageScale*sender.scale;
        sender.scale = 1.0;
        
        [self setNeedsDisplay];
    }
    
    
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

- (void)handleLongPressGesture:(UILongPressGestureRecognizer *) gesture
{
    //grab longPress point
    CGPoint point = [gesture locationInView:self];
    
    //grab top most object at point.
    movingObject = [[pages objectAtIndex:currentPageIndex] shapeAtPoint:location];
    
    //If object is not nil, show alert to delete shape.
    if (movingObject && !controller.popoverIsActive) {        
        controller.popoverIsActive = YES;
        
        UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"" destructiveButtonTitle:@"Delete" otherButtonTitles: nil];
        [action showFromRect:CGRectMake(point.x-.5, point.y-.5, 1, 1) inView:self animated:YES];
        [action release];
    }
    
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    controller.popoverIsActive = NO;
    
    if (movingObject) {
        
//        DrawingObject *object = [movingObject retain];
        
        [controller sendDeleteForObject:movingObject];
        
        [self deleteObject:movingObject];
    
        movingObject = nil;
    }
    
    [self setNeedsDisplay];
}

//////// delegate implementation ////////

- (void)shapePickerDidFinishWithShape:(int)type
{
	[controller.popOver dismissPopoverAnimated:YES];
	controller.popoverIsActive = NO;
	currentShapeType = type;
}

// External Screen Info
-(void)setExternalScreenAttached:(BOOL)attached{
    
    if(attached)
        [[pages objectAtIndex:0] reducePageCount];
    
    externalScreenAttached = attached;
    
}

@end
