//
//  ECLine.m
//  QuartzDraw
//
//  Created by Bianco, Anthony M on 6/20/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ECLine.h"
#import "ECPoint.h"
#import "ECDrawAppDelegate.h"

@implementation ECLine




//inits a line with the provided color size and start point
- (ECLine *)initWithColor:(UIColor *)setColor andSize:(CGFloat)setSize atPoint:(CGPoint)setStart andPageNumber:(NSInteger)pid andElementID:(NSInteger)eid
{
	self = [super initWithPage:pid];
	ECPoint *setter = [[[ECPoint alloc] initWithPoint:setStart] autorelease];
	
	elementID=eid;
	
	self.size = setSize;
	self.color = setColor;
	
	points = [[NSMutableArray alloc] init];
	[points addObject:setter];

	type = LINE_TYPE;	
	
	return self;
}

//NSCoding compliance
//init using the info decoded from the NSCoder
- (ECLine *)initWithCoder:(NSCoder *)coder
{
	self = [super initWithCoder:coder];
	
	points = [[coder decodeObjectForKey:@"points"] retain];
	type = LINE_TYPE;
	
	return self;
}

//Encodes objects into the provided NSCoder
- (void)encodeWithCoder:(NSCoder *)coder
{	
	[super encodeWithCoder:coder];
	[coder encodeObject:points forKey:@"points"];
}

- (NSInteger)pointCount
{
	return [points count];
}

//adds the provided point to the line object
- (void)addPoint:(CGPoint) point
{
	ECPoint *setter = [[[ECPoint alloc] initWithPoint:point] autorelease];
	[points addObject:setter];
}

void SmoothDrawing(CGContextRef context, ECPoint *currentPoint, ECPoint *endPoint, ECPoint *nextPoint, ECPoint *previousPoint)
{
	
	float smoothingFactor = .75;
	CGPoint CP1,CP2;
	
	CP1.x = ((smoothingFactor + 3.0) * currentPoint.x + endPoint.x - smoothingFactor * previousPoint.x) / 4.0;
	CP1.y = ((smoothingFactor + 3.0) * currentPoint.y + endPoint.y - smoothingFactor * previousPoint.y) / 4.0;
	
	CP2.x = ((smoothingFactor + 3.0) * endPoint.x + currentPoint.x - smoothingFactor * nextPoint.x) / 4.0;
	CP2.y = ((smoothingFactor + 3.0) * endPoint.y + currentPoint.y - smoothingFactor * nextPoint.y) / 4.0;
	
	CGContextAddCurveToPoint(context, CP1.x, CP1.y, CP2.x, CP2.y, endPoint.x, endPoint.y);

}

//the line draws itself with the provided graphics context
- (void)drawInContext:(CGContextRef)context
{
	if([points count] >1)
	{
		UIGraphicsPushContext(context);
		
		ECPoint *currentPoint = [points objectAtIndex:0];
		
		if([points count] == 2)//handles single dots
		{
			CGRect rect;
			rect.origin.x = currentPoint.x - (size / 2);
			rect.origin.y = currentPoint.y - (size / 2);
			rect.size.height = size;
			rect.size.width = size;
			
			[color setFill];
			
			CGContextFillRect(context,rect);
		}
		else
		{		
			
			ECPoint *endPoint = [points objectAtIndex:1];
			ECPoint *nextPoint = [points objectAtIndex:2];
			ECPoint *previousPoint;
			
			//sets the color and size
			CGContextSetLineWidth(context, size);
			[color setStroke];
			
			//if the color is clear it is an eraser line and the blending mode needs changed
			if(color == [UIColor clearColor])
			{
				CGContextSetBlendMode(context, kCGBlendModeClear);
				CGContextSetLineWidth(context, size);
			}
			
			CGContextMoveToPoint(context, currentPoint.x, currentPoint.y);
			
			SmoothDrawing(context, currentPoint, endPoint, nextPoint, currentPoint);//first case
			
			//trace lines from point to point to make a path then stroke it
			for(int i=3;i< [points count];i++)
			{
				previousPoint = currentPoint;
				currentPoint = endPoint;
				endPoint = nextPoint;
				nextPoint = [points objectAtIndex:i];
				
				SmoothDrawing(context, currentPoint, endPoint, nextPoint, previousPoint);			
			}
			
			previousPoint = currentPoint;
			currentPoint = endPoint;
			endPoint = nextPoint;
			
			SmoothDrawing(context, currentPoint, endPoint, endPoint, previousPoint);//end case
			
			CGContextStrokePath(context);
			
			//set the blending mode back to normal after drawing the eraser line
			if(color == [UIColor clearColor])
			{
				CGContextSetBlendMode(context, kCGBlendModeNormal);
			}
		}
		UIGraphicsPopContext();
	}
}

-(BOOL)containsPoint:(CGPoint)point
{
    
//    for (ECPoint *ecpoint in points) {
//        if (abs(point.x - [ecpoint x]) <= 10 && abs(point.y - [ecpoint y]) <= 10) {
//            return YES;
//        }
//    }
    
    return NO;
}
-(BOOL)moveToPoint:(CGPoint)point
{
    
//    location.x += (point.x - location.x);
//    location.y += (point.y - location.y);
    
    return NO;
}

- (void)dealloc
{
	[points release];
	
	[super dealloc];
}

@end
