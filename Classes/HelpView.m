//
//  HelpView.m
//  ECDraw
//
//  Created by Ryan, Taylor on 6/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HelpView.h"
#import "DrawAreaViewController.h"

#define kArrowWidth 64

@implementation HelpView

@synthesize controller;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.opaque = NO;
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
        
//        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(450, 20, 200, 30)];
//        title.textColor = [UIColor whiteColor];
//        title.backgroundColor = [UIColor clearColor];
//        title.opaque = NO;
//        title.text = @"Help Screen";
//        [self addSubview:title];
                
        labelPosition = CGPointMake(5,300);
        arrowPosition = CGPointMake(-5, 704-kArrowWidth);

        arrowView = [[UIImageView alloc] initWithFrame:CGRectMake(arrowPosition.x, arrowPosition.y, kArrowWidth, kArrowWidth)];
        arrowView.image = [UIImage imageNamed:@"down_arrow.png"];
        [self addSubview:arrowView];
        
        description = [[UILabel alloc] initWithFrame:CGRectMake(5, 300, 350, 500)];
        description.textColor = [UIColor whiteColor];
        description.backgroundColor = [UIColor clearColor];
        description.opaque = NO;
        description.numberOfLines = UILineBreakModeWordWrap;
        description.font = [UIFont fontWithName:@"Chalkduster" size:20];
        
        [self addSubview:description];      
        
        
        helpSlide = 0;
    }
    return self;
}

#pragma mark -
#pragma mark Help Slide
-(void) updateHelpSlide{
    
    NSString *string = @"";
        
    switch (helpSlide) {
        case 0:
        {
            //Save
            string = @"Brings up a menu to Start a new session, Save sessions, Load sessions, and Save screenshots.";
            
            labelPosition.x = 5;
            arrowPosition = CGPointMake(-5, 704-kArrowWidth);
            break;
        }
        case 1:
        {
            //Trash
            string = @"Brings up a menu to delete the drawings on a page, or the entire page.";
            labelPosition.x = 5;
            arrowPosition = CGPointMake(45, 704-kArrowWidth);
            break;
        }
        case 2:
        {
            //Photo Library
            string = @"Add an image saved to your camera roll.";
            labelPosition.x = 5;
            arrowPosition = CGPointMake(90, 704-kArrowWidth);
            break;
        }
        case 3:
        {
            //Mode switcher
            string = @"Switch between drawing lines, drawing shapes, typing text, erasing, scrolling and moving objects.";
            labelPosition.x = 226-120;
            arrowPosition = CGPointMake(labelPosition.x + 120, 704-kArrowWidth);
            break;
        }
        case 4:
        {
            //Color Picker
            string = @"Select the color of the drawing you are currently doing. Make sure to tap choose to finalize the selection.";
            labelPosition.x = 362-120;
            arrowPosition = CGPointMake(labelPosition.x + 120, 704-kArrowWidth);
            break;
        }
        case 5:
        {
            //Line Width
            string = @"Select the width of the lines drawn or text typed.";
            labelPosition.x = 447-120;
            arrowPosition = CGPointMake(labelPosition.x + 120, 704-kArrowWidth);
            break;
        }
        case 6:
        {
            //Grid
            string = @"Toggle on and off the grid.";
            labelPosition.x = 530-120;
            arrowPosition = CGPointMake(labelPosition.x + 120, 704-kArrowWidth);
            break;
        }
        case 7:
        {
            //Page switcher
            string = @"Switch back and forth between pages and add new pages with the '+'.";
            labelPosition.x = 602-120;
            arrowPosition = CGPointMake(labelPosition.x + 120, 704-kArrowWidth);
            break;
        }
        case 8:
        {
            //Undo
            string = @"Undo the last drawing you did.";
            labelPosition.x = 677-120;
            arrowPosition = CGPointMake(labelPosition.x + 120, 704-kArrowWidth);
            break;
        }
        case 9:
        {
            //Redo
            string = @"Redo the last drawing removed with undo.";
            labelPosition.x = 725-120;
            arrowPosition = CGPointMake(labelPosition.x + 120, 704-kArrowWidth);
            break;
        }
        case 10:
        {
            //Home
            string = @"Returns the drawing canvas to the starting point.";
            labelPosition.x = 777-120;
            arrowPosition = CGPointMake(labelPosition.x + 120, 704-kArrowWidth);
            break;
        }
        case 11:
        {
            //Connect
            string = @"Connect to the server and create a room to collaborate with others, or join a room already in use.";
            labelPosition.x = 826-145;
            arrowPosition = CGPointMake(826, 704-kArrowWidth);
            break;
        }
            
        default:
        {
            //Close help screen
            helpSlide = 0;
            labelPosition.x = -50;
            [controller helpPressed:nil];
            
            break;
        }
    }
        
    [UIView beginAnimations:@"moveLabel" context:NULL];
    
    CGRect frame = description.frame;
    frame.origin.x = labelPosition.x;
    frame.origin.y = labelPosition.y;
    description.frame = frame;
    
    frame = arrowView.frame;
    frame.origin.x = arrowPosition.x;
    frame.origin.y = arrowPosition.y;
    arrowView.frame = frame;
    
    [UIView commitAnimations];
    description.text = string;
//    [description sizeToFit];
    
//    [UIView commitAnimations];
}

-(void)resetSlides{
    helpSlide = 0;
}

#pragma mark -
#pragma mark Touch Handling

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    
    helpSlide++;
    
    [self updateHelpSlide];    

}

-(void)dealloc{
    [super dealloc];
    [description release];
}

@end
