//
//  ECShape.h
//  ECDraw
//
//  Created by Bianco, Anthony M on 8/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DrawingObject.h"
#import "ECPoint.h"

typedef enum{
	SQUARE_SHAPE,
	RECTANGLE_SHAPE,
	OVAL_SHAPE,
	CIRCLE_SHAPE,
	RTRIANGLE_SHAPE,
	ETRIANGLE_SHAPE,
	VLINE_SHAPE,
	HLINE_SHAPE,
	DLINE_SHAPE,
    CUBE_3D_SHAPE,
    CUBOID_3D_SHAPE,
    CONE_3D_SHAPE,
    PYRAMID_3D_SHAPE,
    PRISM_3D_SHAPE,
    CYLINDER_3D_SHAPE,
    SPHERE_3D_SHAPE
}ShapeType;

@interface ECShape : DrawingObject {

	CGPoint startPoint, endPoint;
	ShapeType shapeType;
	ECPoint *lastOffset;
}

@property CGPoint endPoint;
@property BOOL snapToGrid;

- (ECShape *)initWithColor:(UIColor *)setColor andSize:(CGFloat)setSize andType:(ShapeType)setType atPoint:(CGPoint)setStart andPageNumber:(NSInteger)pid andElementID:(NSInteger)eid;
- (void)drawInContext:(CGContextRef)context;
- (ECShape *)initWithCoder:(NSCoder *)coder;
- (void)encodeWithCoder:(NSCoder *)coder;

@end
