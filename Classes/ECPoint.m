//
//  Point.m
//  QuartzDraw
//
//  Created by Bianco, Anthony M on 6/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ECPoint.h"


@implementation ECPoint

//set the points x
- (void)setX:(CGFloat)aFloat
{
	x=aFloat;
}

//gets the points x
- (CGFloat)x
{
	return x;
}

//sets the points y
- (void)setY:(CGFloat)aFloat
{
	y=aFloat;
}

//gets the points y
- (CGFloat)y
{
	return y;
}

//NSCoding compliance
//encodes the point into the provided NSCoder
- (void)encodeWithCoder:(NSCoder *)coder
{
	[coder encodeFloat:x forKey:@"x"];
	[coder encodeFloat:y forKey:@"y"];
}

//init the point with data provided from the NSCoder
- (ECPoint *)initWithCoder:(NSCoder *)coder
{
	self = [super init];
	
	x = [coder decodeFloatForKey:@"x"];
	y = [coder decodeFloatForKey:@"y"];
	
	return self;
}

//init with the given CGPoint
- (ECPoint *)initWithPoint:(CGPoint)setPoint
{
	self = [super init];
	
	x = setPoint.x;
	y = setPoint.y;
	
	return self;
}

//init with the given x and y values
-(ECPoint *)initWithX:(CGFloat)setX andY:(CGFloat)setY
{
	self = [super init];
	
	x = setX;
	y = setY;
	
	return self;

}

@end
