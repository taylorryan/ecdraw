//
//  ECPage.m
//  ECDraw
//
//  Created by Bianco, Anthony M on 6/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ECPage.h"
#import "ECDrawAppDelegate.h"

//static NSInteger pageCount=0;//keeps track of number of pages, used to get page ID;


@implementation ECPage

@synthesize hasImage;
@synthesize translate;
@synthesize pageScale;
//@synthesize image;
@synthesize pageID;
@synthesize userElementID;
@synthesize gridIsActive;

// init the page
- (ECPage *)init:(int)count
{
	self = [super init];

	pageCount = count;
	pageID=count - 1;
	//set all standard initial values
	lines = [[NSMutableArray alloc]init];
    undoActions = [[NSMutableArray alloc] init];
	translate = CGPointZero;
    pageScale = 1;
	image = nil;
	hasImage = NO;
	userElementID = 0;
    gridIsActive = NO;
	
	return self;
}

//init the page with settings retrieved from the NSCoder
- (ECPage *)initWithCoder:(NSCoder *)coder
{
	self = [super init];

	//Decode and store all the items from the Coder
	lines = [[coder decodeObjectForKey:@"lines"] retain];
    undoActions = [[coder decodeObjectForKey:@"undos"] retain]; //[[NSMutableArray alloc] init];
	translate.x = 0;
	translate.y = 0;
    pageScale = 1;
	hasImage = [coder decodeBoolForKey:@"hasImage"];
	userElementID = [coder decodeIntegerForKey:@"elementID"];
	
	pageID=[coder decodeIntegerForKey:@"PageID"];
	
	//UIImage *img = [[coder decodeObjectForKey:@"image"] retain];
	//image = img;
	//[img autorelease];
	
    gridIsActive = NO;
    
	return self;
}

//encode the current page into an NSCoder
- (void)encodeWithCoder:(NSCoder *)coder
{	
	//encode each object for its type
	[coder encodeObject:lines forKey:@"lines"];
    [coder encodeObject:undoActions forKey:@"undos"];
	[coder encodeBool:hasImage forKey:@"hasImage"];
	[coder encodeInteger:pageID forKey:@"PageID"];//encodeObject:pageID forKey:@"PageID"];
	[coder encodeInteger:userElementID forKey:@"elementID"];//encodeObject:userElementID forKey:@"elementID"];

	//UIImage *img = image;
	//
	//[coder encodeObject:img forKey:@"image"];
}

//update client id received from host
- (void) updateClientID:(NSInteger)newID from:(NSInteger)oldID{
    
    for (DrawingObject *object in lines) {
        if (object.clientID == oldID) {
            object.clientID = newID;
        }
    }
    for (DrawingObject *object in undoActions) {
        if (object.clientID == oldID) {
            object.clientID = newID;
        }
    }
     
}

- (NSInteger)getPageID
{
	return pageID;
}

- (void) clearUndoActions:(NSInteger)cid{
    //called when a new line is added to lines array.
    //clears the undoActions array, 
    NSMutableArray *deleting = [[NSMutableArray alloc] init];
    for (DrawingObject *object in undoActions) {
        if (object.clientID == cid) {
            [deleting addObject:object];
        }
    }
    
    [undoActions removeObjectsInArray:deleting];
    if (deleting) [deleting release];
}

- (int)undoCount
{
    int count = 0;
    for (int i = [undoActions count] -1; i >=0 ; i--) {
        DrawingObject *object = [undoActions objectAtIndex:i];
        if (object.clientID == [[ECDrawAppDelegate getInstance] getClientID]) {
            count++;
        }
    }
    return count;
}

- (void)redo{
    if ([undoActions count] < 1) {
        //nothing to redo
        return;
    }
    
    for (int i = [undoActions count] -1; i >=0 ; i--) {
         DrawingObject *object = [undoActions objectAtIndex:i];
        if (object.clientID == [[ECDrawAppDelegate getInstance] getClientID]) {
            [lines addObject:object];
            userElementID++;
            [undoActions removeObject:object];
            return;
        }
    }

}

- (void)redoForClient:(NSInteger)cid{
    if ([undoActions count] < 1) {
        //nothing to redo
        return;
    }
    
    for (int i = [undoActions count] -1; i >=0 ; i--) {
        DrawingObject *object = [undoActions objectAtIndex:i];
        if (object.clientID == cid) {
            [lines addObject:object];
            [undoActions removeObject:object];
            return;
        }
    }
}

- (void)undo
{
	for(int i = [lines count]-1;i>=0;i--)
	{
		DrawingObject *object = [lines objectAtIndex:i];
		if(object.clientID == [[ECDrawAppDelegate getInstance] getClientID] && object.elementID == userElementID-1)
		{
            [undoActions addObject:object];//add object to undo array
			[lines removeObject:object];//remove from lines array
			userElementID--;
			return;
		}
	
	}
}

- (void)undoForClient:(NSInteger)cid
{
	for(int i = [lines count]-1;i>=0;i--)
	{
		DrawingObject *object = [lines objectAtIndex:i];
		if(object.clientID == cid)
		{
            [undoActions addObject:object];
			[lines removeObject:object];
			return;
		}
		
	}

}

- (void)removeObjectFromPage:(id)object
{
	[lines removeObject:object];
}

//sets an incoming image ref to the current image and
//sets the state of hasImage to YES
- (void)setImage:(UIImage *)img
{
	image = img;
	hasImage = YES;
}

//Allows other objects to see if the current page has an image
- (bool)hasAnImage
{
	return hasImage;
}

- (UIImage *)getImage
{
	return image;
}

//Allows the Pan gesture transform to be added to the current transform
- (void)addToTranslate:(CGPoint)point
{
	translate.x += point.x/pageScale;
	translate.y += point.y/pageScale;
}
//returns the current translation to the caller
- (CGPoint)getTranslate
{
    CGPoint returning;
    returning.x = translate.x/pageScale;
    returning.y = translate.y/pageScale;
	return returning;//translate;
}
-(void)setPageScale:(float)_scale{    
    //restrict scale
    if (_scale < .1 || _scale > 8.0) {
        return;
    }
    pageScale = _scale;
    
    //float factor = .5-.5*pageScale;
    //translate.x = factor*(1024)/pageScale;
    //translate.y = factor*(768)/pageScale;
    

}
-(float)getPageScale{
    return pageScale;
}
//draws the current page with the provided graphics context
- (void)drawPageWithContext:(CGContextRef) context
{
	CGContextScaleCTM(context, pageScale, pageScale);
	CGContextTranslateCTM(context, translate.x, translate.y);
	
    if (gridIsActive) {
        //Show Grid
        UIImage *backgroundPattern = [UIImage imageNamed:@"grid32.png"];
        CGRect tiledRect;
        tiledRect.origin = CGPointZero;
        tiledRect.size = backgroundPattern.size;
        CGContextDrawTiledImage(context, tiledRect, backgroundPattern.CGImage);
    }
    
    
    CGContextBeginTransparencyLayer(context, NULL);//Transparency Layer prevents eraser from erasing Grid.
    
    //Draw the shapes and images on their own layer.
    for (id object in lines) {
        if ([object class] == [ECShape class] || [object class] == [ECImage class]) {
            [object drawInContext:context];
        }
    }
    
    CGContextBeginTransparencyLayer(context, NULL);//Transparency Layer prevents eraser from erasing the shapes.
    
	for (id object in lines)
	{
		//Lines draw themselves so each line in the array is told to do so
        if ([object class] != [ECShape class] && [object class] != [ECImage class])
            [object drawInContext:context];
	}
    
	CGContextEndTransparencyLayer(context);
    CGContextEndTransparencyLayer(context);
    
	//[[lines lastObject] drawLineInContext:context withTranslation:translate];
}

//erases all lines and images on the current page
- (void)eraseImage
{
	[self eraseDrawing];
	[image release];
	image = nil;
	hasImage = NO;
    pageScale = 1;
}

//erases all the lines on the screen
- (void)eraseDrawing
{
	[lines removeAllObjects];
    pageScale = 1;
}

- (BOOL)deleteDrawingObject:(DrawingObject *)drawingObject
{
    for(int i = [lines count]-1;i>=0;i--)
	{
		DrawingObject *object = [lines objectAtIndex:i];
		if(drawingObject.elementID == object.elementID && 
           drawingObject.pageID == object.pageID && 
           drawingObject.clientID == object.clientID){
			[lines removeObject:object];//remove from lines array
            return YES;
        }
	}
    
    return NO;
}

//adds a new line withe the provided color, size and start point
- (ECLine *)newLineWithColor:(UIColor *)setColor andSize:(CGFloat)setSize atPoint:(CGPoint)setStart
{
	ECLine *line = [[ECLine alloc] initWithColor:setColor andSize:setSize atPoint:setStart andPageNumber:pageID andElementID:userElementID++];
    line.clientID = [[ECDrawAppDelegate getInstance] getClientID];
	[lines addObject:line];
    [self clearUndoActions:[[ECDrawAppDelegate getInstance] getClientID]];
	return line;
}

//adds a new shape with the provided color, size, type and start point.
- (ECShape *)newShapeWithColor:(UIColor *)setColor andSize:(CGFloat)setSize andType:(ShapeType)setType atPoint:(CGPoint)setStart
{
	ECShape *shape = [[ECShape alloc] initWithColor:setColor andSize:setSize andType:setType atPoint:setStart andPageNumber:pageID andElementID:userElementID++];
    shape.clientID = [[ECDrawAppDelegate getInstance] getClientID];
    shape.snapToGrid = gridIsActive;//Only snap shape to grid if grid is showing
	[lines addObject:shape];
    [self clearUndoActions:[[ECDrawAppDelegate getInstance] getClientID]];
	return shape;
}

//adds a image with the provide uiimage
- (ECImage *) newImageWithImage:(UIImage *)source atPoint:(CGPoint)setStart{
    ECImage *img = [[ECImage alloc] initWithImage:source atPoint:setStart andPageNumber:pageID andElementID:userElementID++];
    img.clientID = [[ECDrawAppDelegate getInstance] getClientID];
	[lines addObject:img];
    [self clearUndoActions:[[ECDrawAppDelegate getInstance] getClientID]];
	return img;
}

- (void)addDrawingObject:(DrawingObject *)item
{
	BOOL found = NO;
	
    if (item == nil) {
        return;
    }
	
	for(int i = [lines count] - 1; i >= 0 && !found; i--)
	{
		DrawingObject *ptr = [lines objectAtIndex:i];
		if(ptr.clientID == item.clientID)
		{
			if(ptr.elementID == item.elementID && item != nil)
			{
                [lines replaceObjectAtIndex:i withObject:item];
				found = YES;
			}
			else if(ptr.elementID < item.elementID )
			{
				[lines addObject:item];
				found = YES;
			}
		}
	}
	
	if(!found)
	{
		[lines addObject:item];
	}
	[self clearUndoActions:item.clientID];
}

//add given text at given point
- (ECText *)newText:(NSString *)text withPoint:(CGPoint)point andSize:(CGFloat)setSize andColor:(UIColor *)setColor
{
	ECText *returnText = [[ECText alloc] initWithText:text andPoint:point andSize:setSize andColor:setColor andPage:pageID andElementID:userElementID++];
    returnText.clientID = [[ECDrawAppDelegate getInstance] getClientID];
    returnText.snapToGrid = gridIsActive;
	[lines addObject:returnText];
	[self clearUndoActions:[[ECDrawAppDelegate getInstance] getClientID]];
	return returnText;
}

//adds a point to the current line
- (void)addPoint:(CGPoint)point
{
	[[lines lastObject]addPoint:point];
}

//Returns the top most object at the given point
- (DrawingObject *)shapeAtPoint:(CGPoint)point{
    DrawingObject *temp;
    for (int i = [lines count] - 1; i >=0; i--) {
        temp = [lines objectAtIndex:i];
       
        if ([temp containsPoint:point]) {
            return temp;
        }
        
    }
    
    return nil;
}

-(void)reducePageCount{
    pageCount--;
}

- (void)dealloc
{
	[image release];
	[super dealloc];
	
}
@end
