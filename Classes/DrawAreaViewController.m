//
//  DrawAreaViewController.m
//  ECDraw
//
//  Created by Bianco, Anthony M on 6/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

//#import <QuartzCore/QuartzCore.h>
#import "DrawAreaViewController.h"
#import "ConnectionInfoViewController.h"
#import "ColorPickerViewController.h"
#import "ECLine.h"
#import "ECDrawAppDelegate.h"

#import <SystemConfiguration/SystemConfiguration.h>
#import <netinet/in.h>

@implementation DrawAreaViewController

@synthesize drawingView;
@synthesize imageView;
@synthesize currentPage;
@synthesize totalPages;
@synthesize toolBar;
@synthesize popOver;
@synthesize popoverIsActive;
@synthesize clientID;

- (void) changeButtonColorTo:(UIColor *)setColor
{	
	[[colorPickerButton layer] setCornerRadius:6.0f];
	[[colorPickerButton layer] setMasksToBounds:YES];
	[[colorPickerButton layer] setBorderWidth:1.0f];
	[[colorPickerButton layer] setBorderColor: [[UIColor grayColor] CGColor]];
	[[colorPickerButton layer] setBackgroundColor:[setColor CGColor]];
	colorPickerButton.frame=CGRectMake(0.0, 100.0, 60.0, 30.0);
    
}

- (void)segmentedControlTextImages
{	
	[sizeSegmentedControl setImage:[UIImage imageNamed:@"small.png"] forSegmentAtIndex:0];
	[sizeSegmentedControl setImage:[UIImage imageNamed:@"normal.png"] forSegmentAtIndex:1];
	[sizeSegmentedControl setImage:[UIImage imageNamed:@"large.png"] forSegmentAtIndex:2];
}

- (void)segmentedControlDrawingImages
{
	[sizeSegmentedControl setImage:[UIImage imageNamed:@"fine.png"] forSegmentAtIndex:0];
	[sizeSegmentedControl setImage:[UIImage imageNamed:@"medium.png"] forSegmentAtIndex:1];
	[sizeSegmentedControl setImage:[UIImage imageNamed:@"thick.png"] forSegmentAtIndex:2];
}

- (void)segmentedControlErasingImages
{	
	
	[sizeSegmentedControl setImage:[UIImage imageNamed:@"little_eraser.png"] forSegmentAtIndex:0];
	[sizeSegmentedControl setImage:[UIImage imageNamed:@"erase.png"] forSegmentAtIndex:1];
	[sizeSegmentedControl setImage:[UIImage imageNamed:@"big_eraser.png"] forSegmentAtIndex:2];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
        
		[self connectionStart];
        
		[[ECDrawAppDelegate getInstance] setClientID:arc4random()%10000];
		
        drawingView.isMainScreen = YES;
        
    }
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if(interfaceOrientation == UIDeviceOrientationLandscapeLeft ||
	   interfaceOrientation == UIDeviceOrientationLandscapeRight)
	{
		return YES;
	}
	else {
		return NO;
	}
    
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)update
{
	[currentPage setText:[NSString stringWithFormat:@"%ld",(drawingView.currentPageIndex + 1)]];
	[totalPages setText:[NSString stringWithFormat:@"%ld",drawingView.pageCount]];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	for(int i = DRAW_MODE; i <= SHAPE_MODE;i ++)
		color[i]=[[UIColor colorWithRed:0 green:0 blue:0 alpha:1] retain];
	
	[self changeButtonColorTo:[UIColor blackColor]];
	drawingView.controller = self;
	drawingView.mode = DRAW_MODE;
    drawingView.isMainScreen = YES;
	imageView.contentMode = UIViewContentModeScaleAspectFit;
	for(int i = DRAW_MODE; i <= ERASE_MODE;i ++)
		size[i]=0;
	
	[self update];//update pages
	
	connectionController = [[ConnectionInfoViewController alloc] init];
	connectionController.delegate = self;
    
    //start Local Server
    localServer = [[Server alloc] init];
    localServer.delegate = self;
    
    //Disable redo button since at the start, there is nothing to redo.
    redoButton.enabled = NO;
    
    UIEdgeInsets insets = UIEdgeInsetsMake(-2, 0, 0, 0);
    gridButton.imageInsets = insets;
    saveButton.imageInsets = insets;
    homeButton.imageInsets = insets;
    connectButton.imageInsets = insets;
	undoButton.imageInsets = insets;
    redoButton.imageInsets = insets;
}

- (void)viewDidUnload {
    [redoButton release];
    redoButton = nil;
    [gridButton release];
    gridButton = nil;
    [saveButton release];
    saveButton = nil;
    [connectButton release];
    connectButton = nil;
    [homeButton release];
    homeButton = nil;
    [undoButton release];
    undoButton = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)alertView:(UIAlertView *) alertView clickedButtonAtIndex:(NSInteger)index
{
    if (index == 0) {
        //cancel button was hit, do nothing
        return;
    }
    
    if (alertView.tag == 1) {
        //Erase Alertview
        
        if(index == 1)//Drawing only
        {		
            if(host)
                [drawingView erasePageDrawing];
            
            [self sendClearForPage:[drawingView currentPageID]];
        }
        else if(index == 2)//remove whole page
        {
            
            [self sendDeleteForPage:[drawingView currentPageID]];
            
            if(host){
                [drawingView removePageForPageID:[drawingView currentPageID]];
            }
            
        }
    }
    else if (alertView.tag == 2) {
        //Save & Load alert
        if (index == 1) {
            //New Session
            
            //Check if not host
            if (!host) {
                [localConnection sendNetworkPacket:0 withType:SESSION_NEW_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:-1 andID:-1];
                
                return;
            }
            else {
                //Ask host if they want to start a new session.
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"New Session" message:@"Are you sure you want to start a new session. All unsaved data will be lost."  delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok",nil];
                alert.tag = 3;
                [alert show];
                [alert release];
            }
        }
        else if (index == 2 && host) {
            //Load Session
            
            FileLoadingViewController *fileView = [[[FileLoadingViewController alloc] initWithNibName:@"FileLoadingViewController" bundle:[NSBundle mainBundle]] autorelease];
            fileView.controller = self;
            fileView.modalPresentationStyle = UIModalPresentationFormSheet;
            [self presentModalViewController:fileView animated:YES];
            
        }
        else if (index == 3) {
            //Save Session
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            [drawingView savePages:[NSString stringWithFormat:@"%@.dat",[formatter stringFromDate:[NSDate date]]]];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Saved" message:@"The session has been saved to your device." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [alert release];
            [formatter release];
            formatter = nil;
        }
        else if (index == 4){
            //Save Screenshot
            UIGraphicsBeginImageContextWithOptions(drawingView.bounds.size, NO, 0);
            [drawingView.layer renderInContext:UIGraphicsGetCurrentContext()];
            UIImage *imageToSave = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            UIImageWriteToSavedPhotosAlbum(imageToSave, self, nil, nil);
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Saved" message:@"A screenshot has been saved to your Camera Roll." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
    }
    else if (alertView.tag == 3 && host) {
        // Host Alert: Start a new session?
        if (index == 1) {
            //delete old pages
            [drawingView removeAllPages];
            
            //send refresh.
            [self refresh];
            
        }
    }
    else if (alertView.tag == 4){
        NSString *password = alertText.text;
        
        //encrypt password
        password = [password sha1];
        
        //String to send...
        password = [NSString stringWithFormat:@"%@$%@",connectionController.ipAddress,password];
        
        [localConnection sendNetworkPacket:[password uppercaseString] withType:CHECK_PASSWORD_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:-1 andID:-1];
    }
    
	[self update];//update pages
	
	
}

- (void) loadSession:(NSString *)file{
    [drawingView loadPages:file];
    [self refresh];
    [self update];
}

- (void)connectionInfo:(NSString *)connectionMessage
{
	if([connectionMessage isEqualToString:@"COMPLETE"])
	{
		[connectionController connectionComplete];
	}
	else if([connectionMessage isEqualToString:@"ERROR"])
	{
        [connectionController connectionFailed];
        host = YES;
	}
	else if([connectionMessage isEqualToString:@"Connection Terminated"])
	{
		[connectionController connectionEnded];
	}
}

#pragma mark -
#pragma mark UI Button Methods
- (IBAction)savePressed:(UIBarButtonItem *)sender {
    if (helpIsActive) return;
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Save / Load Session" 
													message:@"What would you like to do?" 
												   delegate:self 
										  cancelButtonTitle:@"Cancel"
										  otherButtonTitles:@"New Session",@"Load Session",@"Save Session",@"Save Screenshot",nil];
	alert.tag = 2;
    [alert show];
	[alert release];
    
    
}

- (IBAction)erasePressed:(UIBarButtonItem *)sender
{
    if (helpIsActive) return;
    
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erase" 
													message:@"Select what to erase." 
												   delegate:self 
										  cancelButtonTitle:@"Cancel"
										  otherButtonTitles:@"Drawings",@"Whole Page",nil];//,@"Drawings and Images"
    alert.tag= 1;
	[alert show];
	[alert release];
}

- (IBAction)undoPressed:(UIBarButtonItem *)sender
{
    if (helpIsActive) return;
    
	[drawingView undo];
    
    //Check if the redo button is disabled and there is something that could be redone
    if (!redoButton.enabled && [drawingView undoCount] > 0) {
        //Disable redo button.
        redoButton.enabled = YES;
    }
}

- (IBAction)redoPressed:(UIBarButtonItem *)sender 
{
    if (helpIsActive) return;
    
    [drawingView redo];
    
    //Check if there is anything else to redo, and if there isnt, disable redo button.
    if ([drawingView undoCount] < 1) {
        //Disable redo button.
        redoButton.enabled = NO;
    }
}

- (IBAction)modeSwitch:(id)sender
{
	NSInteger index = [sender selectedSegmentIndex];
	
	drawingView.mode = index;//sets mode of DrawArea
	sizeSegmentedControl.userInteractionEnabled = YES;
	colorPickerButton.userInteractionEnabled=YES;
    [[drawingView.gestureRecognizers objectAtIndex:0] setMinimumNumberOfTouches:2];
	
	if(index <= ERASE_MODE)
	{
		sizeSegmentedControl.selectedSegmentIndex = size[index];
		[self sizePressed:sizeSegmentedControl];
	}
	
	if(index <= SHAPE_MODE)
	{
		[drawingView setColorToUIColor:color[drawingView.mode]];
		[self changeButtonColorTo:color[drawingView.mode]];
	}
	
	if(index == DRAW_MODE)
	{
		[self segmentedControlDrawingImages];
        
	}
	else if(index == SHAPE_MODE)
	{
		[self segmentedControlDrawingImages];
		
		[self launchShapePickerController:sender];
        
	}
	else if(index == TEXT_MODE)
	{
		[self segmentedControlTextImages];
		
	}
	else if(index == ERASE_MODE)
	{
		[self segmentedControlErasingImages];
		[drawingView setColorToClear];
		colorPickerButton.userInteractionEnabled=NO;
	}
	else if(index == SCROLL_MODE)
	{
        [[drawingView.gestureRecognizers objectAtIndex:0] setMinimumNumberOfTouches:1];
		sizeSegmentedControl.userInteractionEnabled = NO;
		colorPickerButton.userInteractionEnabled=NO;
	}
    else if (index == MOVE_MODE) {
        sizeSegmentedControl.userInteractionEnabled = NO;
		colorPickerButton.userInteractionEnabled=NO;
    }
    
}

- (IBAction)sizePressed:(id)sender
{
	NSInteger index = [sender selectedSegmentIndex];
	if(drawingView.mode <= ERASE_MODE)
		size[drawingView.mode] = index;
    
	if(drawingView.mode == DRAW_MODE || drawingView.mode == SHAPE_MODE)
	{
		
		if(index == 0)
		{
			[drawingView setSize:2.0];
			
		}
		else if(index ==1)
		{
			[drawingView setSize:6.0];
		}
		else if(index == 2)
		{
			[drawingView setSize:10.0];
		}
	}
	else if(drawingView.mode == ERASE_MODE)
	{
		
		if(index == 0)
		{
			[drawingView setSize:10.0];
			
		}
		else if(index ==1)
		{
			[drawingView setSize:20.0];
		}
		else if(index == 2)
		{
			[drawingView setSize:30.0];
		}
	}
	else if(drawingView.mode == TEXT_MODE)
	{
		
		if(index == 0)
		{
			[drawingView setSize:48.0];
			
		}
		else if(index ==1)
		{
			[drawingView setSize:72.0];
		}
		else if(index == 2)
		{
			[drawingView setSize:96.0];
		}
	}
	
	
    
}

- (IBAction)switchPagePressed:(id)sender
{
    if (helpIsActive) return;
    
	NSInteger index = [sender selectedSegmentIndex];
	
	if(index == 0)
	{
		[drawingView previousPage];
	}
	else if(index == 1)
	{
		if(host)
		{
			[self sendAddPage];
			
		}
		else
		{
			[self sendRequestPage];
		}
	}
	else if(index == 2)
	{
		[drawingView nextPage];
	}
	
	[self update];//update pages
}

- (IBAction)homePressed:(UIBarButtonItem *)sender
{    
    if (helpIsActive) return;
    
	[drawingView setTranslationToZero];
	
	CGRect temp = {0,0,imageView.bounds.size.height, imageView.bounds.size.width};
	imageView.bounds = temp;
	
	imageView.transform = CGAffineTransformRotate(imageView.transform, M_PI/2);
	
	[imageView setNeedsDisplay];
}

- (IBAction)gridPressed:(UIBarButtonItem *)sender {
    if (helpIsActive) return;
    
    //toggle grid on and off
    if (!gridIsActive) {
        gridIsActive = YES;
        [drawingView showGrid];
    }
    else {
        gridIsActive = NO;
        [drawingView showGrid];
    }
}

- (IBAction)helpPressed:(UIBarButtonItem *)sender {
    //Display help screens
    if (!helpIsActive) {
        helpIsActive = YES;
        
        if (popoverIsActive) {
            [popOver dismissPopoverAnimated:YES];
            popoverIsActive = NO;
        }
        
        if (!helpOverlay){
            helpOverlay = [[HelpView alloc] initWithFrame:CGRectMake(0, 0, 1024, 704)];
            helpOverlay.controller = self;
        }
        
        helpOverlay.hidden = NO;
        
        [helpOverlay updateHelpSlide];
        
        [self.view addSubview:helpOverlay];
    }
    else {
        helpIsActive = NO;
        helpOverlay.hidden = YES;
        [helpOverlay resetSlides];
    }
}

- (IBAction)imagePressed:(UIBarButtonItem *)sender
{
    if (helpIsActive) return;
    
	if(!popoverIsActive)
	{
		popoverIsActive = YES;
		//[drawingView addImage];
		UIImagePickerController *picker = [[[UIImagePickerController alloc] init] autorelease];
		
		if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
		{
			picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
		}
        
		
		picker.delegate = self;
		picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:
							 UIImagePickerControllerSourceTypeSavedPhotosAlbum];
		
		popOver = [[UIPopoverController alloc] initWithContentViewController:picker];
		
		popOver.delegate = self;
		[popOver presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
		
	}
	else
	{
		[popOver dismissPopoverAnimated:YES];
		popoverIsActive = NO;
	}
    
	
}

- (IBAction)colorPickerPressed:(id)sender
{
    if (helpIsActive) return;
    
	if(!popoverIsActive)
	{
		popoverIsActive = YES;
		
		ColorPickerViewController *colorPicker = [[[ColorPickerViewController alloc] init] autorelease];
		colorPicker.delegate = self;
		
		if(drawingView.mode <= SHAPE_MODE)
			colorPicker.defaultsColor = color[drawingView.mode];
		
		else
			colorPicker.defaultsColor = [UIColor blackColor];
		
		popOver = [[UIPopoverController alloc] initWithContentViewController:colorPicker];
		
		CGSize viewSize;
		
		viewSize.width = 320;
		viewSize.height = 550;
		
		popOver.delegate = self;
		popOver.popoverContentSize = viewSize;
		[popOver presentPopoverFromBarButtonItem:colorPickerLauncher permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
		
	}
	else
	{
		[popOver dismissPopoverAnimated:YES];
		popoverIsActive = NO;
	}
}

- (void)launchShapePickerController:(UIBarButtonItem *)sender
{
    if (helpIsActive) return;
    
	if(!popoverIsActive)
	{
		popoverIsActive = YES;
		
		ShapeTableController *shapePicker = [[ShapeTableController alloc] init];
        
		popOver = [[UIPopoverController alloc] initWithContentViewController:shapePicker];
        
		shapePicker.controller = drawingView;
		
		CGSize viewSize;
		
		viewSize.width = 250;
		viewSize.height = 300;
		
        
		popOver.delegate = self;
		popOver.popoverContentSize = viewSize;
        
        CGRect arrowLocation = CGRectMake(240,710,1.0,1.0);//Location of arrow point.
        [popOver presentPopoverFromRect:arrowLocation inView:drawingView permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
		
		[shapePicker release];
    }
	else
	{
		[popOver dismissPopoverAnimated:YES];
		popoverIsActive = NO;
	}
}

- (void)textFieldPressed:(CGPoint) location withTranslation:(CGPoint)translation
{
    
	if(!popoverIsActive)
	{
		popoverIsActive = YES;
		
		textController = [[[TextViewController alloc] init] autorelease];
		
		popOver = [[UIPopoverController alloc] initWithContentViewController:textController];
		
		textController.delegate = self;
		
        //		textController.userText.delegate= [drawingView addText:@""];
        textController.textView.delegate=[drawingView addText:@""];
		
		CGSize viewSize;
		
		viewSize.width = 320;
		viewSize.height = 160;//100
        
        CGRect touchLocation = CGRectMake(location.x,location.y,1.0,1.0);//location.x + translation.x - 0.5, location.y + translation.y - 0.5, 1.0, 1.0);// rect centered at the touch location
		
		popOver.delegate = self;
		popOver.popoverContentSize = viewSize;
		[popOver presentPopoverFromRect:touchLocation inView:drawingView permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
		
	}
	else
	{
		[popOver dismissPopoverAnimated:YES];
		popoverIsActive = NO;
	}
	
    
}

- (IBAction)connectionPressed:(UIBarButtonItem *)sender
{
    if (helpIsActive) return;
    
	if(!popoverIsActive)
	{
		popoverIsActive = YES;
		
		if(connectionController == nil)
		{
			connectionController = [[ConnectionInfoViewController alloc] init];
			connectionController.delegate = self;
		}
		
		popOver = [[UIPopoverController alloc] initWithContentViewController:connectionController];
		CGSize viewSize;
		
		viewSize.width = 320;
		viewSize.height = 170;
		
		popOver.delegate = self;
		popOver.popoverContentSize = viewSize;
		[popOver presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
		
		
	}
	else
	{
		[popOver dismissPopoverAnimated:YES];
		popoverIsActive = NO;
	}
	
	
}

#pragma mark -
#pragma mark Network management

- (void)sendAddPage
{
    [drawingView addPage];
    
    for(id object in clientSet)
    {
        [object sendNetworkPacket:0 withType:ADD_PAGE_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:drawingView.currentPageIndex andID:-1];
    }
    
    if(localConnection != nil)
    [localConnection sendNetworkPacket:0 withType:ADD_PAGE_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:drawingView.currentPageIndex andID:-1];
	
    
	
	[self update];
}

- (void)sendRequestPage
{
    if(localConnection != nil)
	[localConnection sendNetworkPacket:0 withType:REQUEST_PAGE_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:-1 andID:-1];
}

- (void)sendRefreshPageRequestForPage:(NSInteger)pid
{
    if(localConnection != nil)
	[localConnection sendNetworkPacket:0 withType:REFRESH_PAGE_REQUEST_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:pid andID:-1];
}

- (void)sendRefreshPage:(NSMutableArray *)pageToSend
{
    if (host) {
        
        for(id object in clientSet)
        {
            [object sendNetworkPacket:pageToSend withType:REFRESH_PAGE_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:-1 andID:-1];
        }
        
        if(localConnection != nil)
        [localConnection sendNetworkPacket:pageToSend withType:REFRESH_PAGE_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:-1 andID:-1];
    }
}

- (void)sendClearForPage:(NSInteger)pid
{
    for(id object in clientSet)
        [object sendNetworkPacket:0 withType:CLEAR_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:pid andID:-1];
    
    if(localConnection != nil)
	{
		[localConnection sendNetworkPacket:0 withType:CLEAR_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:pid andID:-1];
		
	}
}

- (void)sendClearAllForPage:(NSInteger)pid
{
    for(id object in clientSet)
        [object sendNetworkPacket:0 withType:CLEAR_ALL_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:pid andID:-1];
    
    if(localConnection != nil)
    {
        [localConnection sendNetworkPacket:0 withType:CLEAR_ALL_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:pid andID:-1];
        
    }
}

- (void)sendDeleteForPage:(NSInteger)pid
{
    for(id object in clientSet)
        [object sendNetworkPacket:0 withType:DELETE_PAGE_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:pid andID:-1];
    
    if(localConnection != nil)
    {
        [localConnection sendNetworkPacket:0 withType:DELETE_PAGE_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:pid andID:-1];
        
    }
}

- (void)sendRedoForPage:(NSInteger)pid
{
    for(id object in clientSet)
        [object sendNetworkPacket:0 withType:REDO_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:pid andID:-1];        
    
    if(localConnection != nil)
    {
        [localConnection sendNetworkPacket:0 withType:REDO_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:pid andID:-1];
        
    }
}

- (void)sendUndoForPage:(NSInteger)pid
{
    for(id object in clientSet)
        [object sendNetworkPacket:0 withType:UNDO_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:pid andID:-1];
    
    if(localConnection != nil)
    {
        [localConnection sendNetworkPacket:0 withType:UNDO_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:pid andID:-1];
        
    }
    
}

- (void)sendImage:(UIImage *)img forPage:(NSInteger)pid
{
    if(localConnection != nil)
    {
        [localConnection sendNetworkPacket:img withType:IMAGE_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:pid andID:-1];
        
    }
}

- (void)sendDrawingObject:(DrawingObject *)sendItem
{
	MessageType packetType; 
	switch (sendItem.type) {
		case LINE_TYPE:
			packetType = LINE_MSG;
			break;
		case TEXT_TYPE:
			packetType = TEXT_MSG;
			break;
		case SHAPE_TYPE:
			packetType = SHAPE_MSG;
			break;
        case IMAGE_TYPE:
            packetType = IMAGE_MSG;
            break;
		default:
			return;
			break;
	}
    
    for(id object in clientSet)
        [object sendNetworkPacket:sendItem withType:packetType andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:sendItem.pageID andID:sendItem.elementID];
    
    if(localConnection != nil)
    {
        
        [localConnection sendNetworkPacket:sendItem withType:packetType andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:sendItem.pageID andID:sendItem.elementID];
        
    }
    
    [[[ECDrawAppDelegate getInstance] getExternalDrawingArea] addDrawingObjectToPage:sendItem];
}

-(void)sendDeleteForObject:(DrawingObject *)deleteItem
{
    for(id object in clientSet)
        [object sendNetworkPacket:deleteItem withType:DELETE_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:deleteItem.pageID andID:deleteItem.elementID];
    
    if (localConnection != nil) {
        [localConnection sendNetworkPacket:deleteItem withType:DELETE_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:deleteItem.pageID andID:deleteItem.elementID];
    }
}

- (void)connectionStart
{
    
	host = YES;// keep track of whether or not you are running a server.
    
	clientSet = [[NSMutableSet alloc] init];
}

- (void)closeConnection
{
    
	[self connectionInfo:@"Connection Terminated"];
    
    for (id object in clientSet) {
        [object sendNetworkPacket:0 withType:CLOSE_CONNECTION_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:0 andID:0];
        [object close];
        object = nil;
    }
    
    [localConnection sendNetworkPacket:0 withType:CLOSE_CONNECTION_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:0 andID:0];
    
    
    [localConnection close];
    [localConnection release];
    localConnection = nil;
    [localServer stop];
    host = YES;
    [clientSet removeAllObjects];
}

#pragma mark -
#pragma mark Delegate Implementations

- (void)textViewControllerDidCancel:(TextViewController *) viewController
{
	[[drawingView getCurrentPage] removeObjectFromPage:viewController.textView.delegate];
	
	[drawingView setNeedsDisplay];
	
	[popOver dismissPopoverAnimated:YES];
	popoverIsActive = NO;
	textController = nil;
}
- (void)textViewController:(TextViewController *)viewController didFinishWithText:(NSString *)text
{
	//[drawingView addText:text];
    
	[self sendDrawingObject:viewController.textView.delegate];
	[popOver dismissPopoverAnimated:YES];
	popoverIsActive = NO;
	textController = nil;
    
    [drawingView setNeedsDisplay];
}



- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
	if(textController != nil)
	{
		[self textViewControllerDidCancel:textController];
	}
	popoverIsActive = NO;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
	[popOver dismissPopoverAnimated:YES];
	popoverIsActive = NO;
    
	UIImage *image = [[info objectForKey:@"UIImagePickerControllerOriginalImage"]retain];

    // Compress image by 50% before adding.
    [drawingView addImage:[UIImage imageWithData:UIImageJPEGRepresentation(image, 0.5)]];
    [image release];
}

- (void)colorPickerViewController:(ColorPickerViewController *)colorPicker didSelectColor:(UIColor *)setColor
{	
	[setColor retain];
	
	if( drawingView.mode <= SHAPE_MODE)
	{
		//[color[drawingView.mode] release];
		color[drawingView.mode]=setColor;
		[drawingView setColorToUIColor:setColor];
		[self changeButtonColorTo:setColor];
	}
	
	[popOver dismissPopoverAnimated:YES];
	popoverIsActive = NO;
    
}

- (void)colorPickerViewControllerDidCancel:(ColorPickerViewController *)colorPicker
{
	[popOver dismissPopoverAnimated:YES];
	popoverIsActive = NO;
}


#pragma mark Server Delegate Methods
//////// ServerDelegate Method Implementations

// Server has failed. Stop the world.
- (void) serverFailed:(Server*)server reason:(NSString*)reason {
	
	if(host)
	{
		// Stop everything and let our delegate know
        [localServer stop];
	}
}

- (void)setImage:(UIImage *)img
{
	imageView.image = img;
	[imageView setNeedsDisplay];
}

#pragma mark NSNetServiceBrowser Delegate Methods

// New service was found
- (void)netServiceBrowser:(NSNetServiceBrowser *)netServiceBrowser didFindService:(NSNetService *)netService moreComing:(BOOL)moreServicesComing {
    
    if (servers == nil) {
        servers = [[NSMutableArray alloc] init];
    }
    
    // Make sure that we don't have such service already (why would this happen? not sure)
    if ( ! [servers containsObject:netService] ) {
        // Add it to our list
        [servers addObject:netService];
    }
    
    // If more entries are coming, no need to update UI just yet
    if ( moreServicesComing ) {
        NSDate *fire = [searchTimer fireDate];
        [searchTimer setFireDate:[fire dateByAddingTimeInterval:10.0]];
        return;
    }
    
    for (NSNetService *service in servers) 
    {
        NSString *serviceName;
        
        //Bypass Password locally
        NSRange range = [service.name rangeOfString:@"$"];
        if (range.length > 0) {
            serviceName = [service.name substringToIndex:range.location];
        }else {
            serviceName = service.name;
        }
        
        
        if ([roomName isEqualToString:serviceName]) {
            //join room
            [netServiceBrowser stop];
            
            if (searchTimer != nil) [searchTimer invalidate];
            
            if(localConnection != nil)
            {
                [localConnection close];
                [localConnection release];
            }
            
            localConnection = [[Connection alloc] initWithNetService:service];
            localConnection.delegate = self;
            
            if( [localConnection connect])
            {	
                host = NO;
                
                localTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(sendLocalHeartbeat) userInfo:nil repeats:YES];
                
//                [localConnection sendNetworkPacket: [NSNumber numberWithInt:[[ECDrawAppDelegate getInstance] getClientID]] withType:CLIENT_ID_REQUEST_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:0 andID:0];
                
                //Tells the client it is connected to a local server incase the host of the server disconnects, they know to connect to the main server.
                isLocalClient = YES;
                
                [self refresh];
            }
            else {
                host = YES;
                isLocalClient = NO;
            }
            
            return;
        }
        
    }
    [searchTimer fire];
    
}

- (void)sendLocalHeartbeat{
    [localConnection sendNetworkPacket:0 withType:HEARTBEAT_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:0 andID:0];
}

-(void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didNotSearch:(NSDictionary *)errorDict{
    NSLog(@"Error: %@",errorDict);
    [self connectionStatusChange:@"ERROR"];
    [serviceBrowser release];
    serviceBrowser = nil;
}
-(void)netServiceBrowserDidStopSearch:(NSNetServiceBrowser *)aNetServiceBrowser{
    [serviceBrowser release];
    serviceBrowser = nil;
}
#pragma mark ConnectionInfoViewController Delegate Methods
//////// ConnectionInfoViewControllerDelegate implementation
- (BOOL)connectToHost:(NSString *) hostAddr room:(BOOL)create name:(NSString *)name
{
    //This method is called first
    
    roomName = [name retain];
    
    if (!create) {
        //Search for local server
        serviceBrowser = [[NSNetServiceBrowser alloc] init];
        [serviceBrowser setDelegate:self];
        [serviceBrowser searchForServicesOfType:@"_ecdraw._tcp" inDomain:@""];
        
        NSDictionary *timerInfo = [NSDictionary dictionaryWithObjectsAndKeys:hostAddr,@"hostAddr",name,@"name", nil];
        searchTimer = [[NSTimer scheduledTimerWithTimeInterval:2.5 target:self selector:@selector(stopSearching:) userInfo:timerInfo repeats:NO] retain];
        [[NSRunLoop currentRunLoop] addTimer:searchTimer forMode:@"searching"];
        
        tryToJoin = YES;
    }
    else {
        [self connectTo:hostAddr withRoomName:name  createRoom:create];
    }
    
    ///////
    return YES;
}

-(void) connectTo:(NSString *)hostAddr withRoomName:(NSString *)name createRoom:(BOOL)create{
    //Then connect to app server and create connection that way.
    host = NO;
	
	if(localConnection != nil)
	{
        [localConnection close];
		[localConnection release];
	}
	
	localConnection = [[Connection alloc] initWithHostAddress:hostAddr andPort:9200];
	localConnection.delegate = self;
    
    
	if( [localConnection connect])
	{	
        if (create) {                        
            [localConnection sendNetworkPacket:roomName withType:NEW_HOST_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:-1 andID:-1];
        }
        else{
            [localConnection sendNetworkPacket:roomName withType:CLIENT_ID_REQUEST_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:-1 andID:1];
        }
	}
	else {
		host = YES;
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Could Not Connect" message:@"There was an error connecting. Please try again later." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
        [alertView release];
    }
    
}

-(void)stopSearching:(NSTimer *)timer{
    [serviceBrowser stop];
    [serviceBrowser release];
    serviceBrowser = nil;
    
    NSDictionary *userInfo = [timer.userInfo retain];
    
    [searchTimer invalidate];
    
    [self connectTo:[userInfo objectForKey:@"hostAddr"] withRoomName:[userInfo objectForKey:@"name"] createRoom:NO];
    
    [userInfo release];
    userInfo = nil;
}


// New client connected to our server. Add it.
- (void) handleNewConnection:(Connection*)connection {
	// Delegate everything to us
	connection.delegate = self;
	
	// Add to our list of clients
	[clientSet addObject:connection];
    
    //    NSLog(@"New Local Connection");
    [self refresh];
    
	//[self connectionInfo:@"Connection Added"];//alert user to new connection
}

#pragma mark Connection Delegate Methods
//////// ConnectionDelegate Method Implementations

- (BOOL) networkConnection{
    // Part 1 - Create Internet socket addr of zero 
    struct sockaddr_in zeroAddr;
    bzero(&zeroAddr, sizeof(zeroAddr)); 
    zeroAddr.sin_len = sizeof(zeroAddr);
    zeroAddr.sin_family = AF_INET;
    // Part 2- Create target in format need by SCNetwork
    SCNetworkReachabilityRef target = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *) &zeroAddr);
    // Part 3 - Get the flags
    SCNetworkReachabilityFlags flags;
    SCNetworkReachabilityGetFlags(target, &flags); 
    // Part 4 - Create output 
    BOOL networkReachable; 
    if (flags & kSCNetworkFlagsReachable) 
        networkReachable = YES; 
    else 
        networkReachable = NO; 
    BOOL cellNetwork; 
    if (flags & kSCNetworkReachabilityFlagsIsWWAN)
        cellNetwork = YES; 
    else 
        cellNetwork = NO;  
    
    return (networkReachable || cellNetwork);
}

- (void) connectionStatusChange:(NSString *) message
{
    if([message isEqualToString:@"NOCONNECT"])
	{
        message = @"ERROR";
        [localConnection close];
        localConnection = nil;
        if([self networkConnection] && [localServer start:roomName]){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Could Not Connect" message:@"The server could not be reached. A local room was connected and can be reached over the local network." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alertView show];
            [alertView release];
            host = YES;
            message = @"COMPLETE";
        }
        else if (![self networkConnection]){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Could Not Connect" message:@"The server could not be reached. Please check your connection to a wireless network and try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alertView show];
            [alertView release];
        }
	}
	if([message isEqualToString:@"ERROR"])
	{
		[self connectionInfo:message];
        [localServer stop];
	}
	else if([message isEqualToString:@"COMPLETE"])
	{
		[self connectionInfo:message];
	}
}

- (void) connectionAttemptFailed:(Connection*)connection {
	if(host)
	{
		// If we are the host we won't be initiating connections, so this is not important
	}
	else
	{
        [self connectionInfo:@"ERROR"];
	}
}

- (void)refresh
{
	if(!host)
	{
        //request host to refresh pages
		[self sendRefreshPageRequestForPage:[drawingView currentPageID]];
	}
    else {
        //since we are host, send pages out to all clients
        [drawingView sendPages];
    }
    [self update];
}

// One of the clients disconnected, remove it from our list
- (void) connectionTerminated:(Connection*)connection {
    BOOL connectionWasLocalClient = [clientSet containsObject:connection];;
    
    if (isLocalClient) {
        //connect to Main server.
        [localConnection close];
        localConnection = nil;
        
        [self connectTo:kServerAddress withRoomName:roomName createRoom:YES];
    }
    else if (tryToJoin) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Could Not Find Room" message:@"The room could not be found. Please check the name and try again." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
    else if (localConnection == connection) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"There has been a error with the connection to the server. You have been disconnected." delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
    
    if (!isLocalClient && !connectionWasLocalClient)
        if (!host || [clientSet count] == 0) {
            [self connectionInfo:@"Connection Terminated"];
            [localServer stop];
            [localConnection close];
            [localConnection release];
            localConnection = nil;
        }
        
   
    
    //release connection
    if(host && connectionWasLocalClient)
	{
        //A client disconnected
        [connection close];
		[clientSet removeObject:connection];
        connection = nil;
        
	}
//	else
//	{
//        //The host disconnected
//        if (!isLocalClient) {
//            host = YES;
//            [connection close];
//            [connection release];
//            connection = nil;
//        }
//	}
    
    //reset local client status.
    isLocalClient = NO;
    
}

// One of connected clients sent a drawing object. Propagate it further.
- (void) receivedNetworkPacket:(NSData *)packet viaConnection:(Connection*)connection withPacketInfo:(int *)packetInfo {
    
    tryToJoin = NO;
    
	switch(packetInfo[PACKET_TYPE_HDR])
	{
		case LINE_MSG:
		{
			ECLine* line = [NSKeyedUnarchiver unarchiveObjectWithData:packet];
			[drawingView addDrawingObjectToPage:line];
            
			break;
		}
		case IMAGE_MSG:
		{
            ECImage *img = [NSKeyedUnarchiver unarchiveObjectWithData:packet];
            [drawingView addDrawingObjectToPage:img];
            
			break;
		}
		case TEXT_MSG:
		{
			ECText* text = [NSKeyedUnarchiver unarchiveObjectWithData:packet];
			[drawingView addDrawingObjectToPage:text];
			break;
		}
		case SHAPE_MSG:
		{
			ECShape* shape= [NSKeyedUnarchiver unarchiveObjectWithData:packet];
			[drawingView addDrawingObjectToPage:shape];
			
			break;
		}
		case UNDO_MSG:
		{
            
			[drawingView undoForClient:packetInfo[CLIENT_ID_HDR] onPage:packetInfo[PAGE_ID_HDR]];
			break;
		}
        case REDO_MSG:
        {
            [drawingView redoForClient:packetInfo[CLIENT_ID_HDR] onPage:packetInfo[PAGE_ID_HDR]];
            
            break;
        }
		case CLEAR_MSG:
		{   
            if (host) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Request" message:[NSString stringWithFormat:@"A user wants to delete the drawings on page %d.",packetInfo[PAGE_ID_HDR]+1]  delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
                [alert show];
                [alert release];
            }
            else {
                [drawingView erasePageDrawingForPageID:packetInfo[PAGE_ID_HDR]];
            }
			break;
		}
		case CLEAR_ALL_MSG:
		{
            if (host) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Request" message:[NSString stringWithFormat:@"A user wants to delete the drawings and images on page %d.",packetInfo[PAGE_ID_HDR]+1]  delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
                [alert show];
                [alert release];
            }
            else {
                imageView.image = nil;
                [drawingView erasePageImageForPageID:packetInfo[PAGE_ID_HDR]];
                
                [imageView setNeedsDisplay];
			}
			break;
		}
		case ADD_PAGE_MSG:
		{
			[drawingView addPage];
			
			break;
		}
		case REQUEST_PAGE_MSG:
		{
			[self sendAddPage];
			
			break;
		}
		case DELETE_PAGE_MSG:
		{
            
            if (host) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Request" message:[NSString stringWithFormat:@"A user wants to delete page %d.",packetInfo[PAGE_ID_HDR]+1]  delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
                [alert show];
                [alert release];
            }
            else {
                [drawingView removePageForPageID:packetInfo[PAGE_ID_HDR]];
                
                [self update];
            }
			break;
		}
		case REFRESH_PAGE_REQUEST_MSG:
		{
			if(host)
			{
				[drawingView sendPages];
			}
			
			break;
		}
		case REFRESH_PAGE_MSG:
		{
            //			if(!host)
            //			{
            NSMutableArray* pages = [NSKeyedUnarchiver unarchiveObjectWithData:packet];
            [drawingView replacePageWith:pages];
            //			}
            
            [self update];
			
			break;
		}
        case CLIENT_ID_REQUEST_MSG:
        {
            if (host) {
//                [self refresh];
                NSNumber *ID;// = [NSKeyedUnarchiver unarchiveObjectWithData:packet];       
                //generate new client ID here
                ID = [NSNumber numberWithInt:(arc4random()%10000)+10000];
                
                
                [connection sendNetworkPacket:0 withType:CLIENT_ID_MSG andCID:[ID intValue] andPID:-1 andID:1];
                
            }
            break;
        }
        case CLIENT_ID_MSG:
        {
            
            NSInteger ID = packetInfo[CLIENT_ID_HDR];
            NSInteger oldID = [[ECDrawAppDelegate getInstance] getClientID];
            if (oldID != ID) {
                // Update local lines with new ID
                
                [[ECDrawAppDelegate getInstance] setClientID:ID];
                
                [drawingView updateClientID:ID from:oldID];
            }
            
            // If the user tried to create a room that already existed, the user
            // joins that room but still thinks they are the host on ipad.
            // If the LINE_ID_HDR isnt -1, they are not the host, so we set 'host'
            // accordingly.
            if (packetInfo[LINE_ID_HDR] != -1) {
                if (host) {
                    //used to be host, send refresh before changing.
                    [drawingView sendPages];
                }
                host = NO;
                //Stop Local server
                [localServer stop];
            }
            else if (packetInfo[LINE_ID_HDR] == -1) {
                host = YES;
                
                [localServer start:roomName];
            }
            
            [self refresh];
            
            break;
        }
        case SESSION_NEW_MSG:
        {
            if (host) {
                //Ask host if they want to start a new session.
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"New Session Request" message:@"A user would like to start a new session. All unsaved data will be lost."  delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok",nil];
                alert.tag = 3;
                [alert show];
                [alert release];
            }
            break;
        }
        case SESSION_LOAD_MSG:
        {
            break;
        }
        case SESSION_SAVE_MSG:
        {
            break;
        }
        case NEW_HOST_MSG:
        {
            //            NSLog(@"You are the new Host!");
            
            [drawingView updateClientID:-1 from:packetInfo[CLIENT_ID_HDR]];
            
            if (packetInfo[LINE_ID_HDR] == -1) {
                
                host = YES;              
                
                [self refresh];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notice!" message:@"Since the old host disconnected, you are the new host."  delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
                [alert show];
                [alert release];
            }
            break;            
        }
        case CHECK_PASSWORD_MSG:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Room Password" message:@"Please enter the room password.\n\n\n" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Submit",nil];
            alertText = [[UITextField alloc] initWithFrame:CGRectMake(12,85, 260, 25)];
            [alertText setBackgroundColor:[UIColor whiteColor]];
            alertText.autocorrectionType = UITextAutocorrectionTypeNo;
            [alert addSubview:alertText];
            alert.tag = 4;
            [alert show];
            [alertText becomeFirstResponder];
            [alert release];
            break;
        }
        case DELETE_MSG:
        {
            DrawingObject *object = [NSKeyedUnarchiver unarchiveObjectWithData:packet];
            [drawingView deleteObject:object];
            break;
        }
        case CLOSE_CONNECTION_MSG:
        {
            break;
        }
        case HEARTBEAT_MSG:
        {
            if (isLocalClient) {
                [localTimer invalidate];
                localTimer = nil;
                
                //Local Client, that means thelocal host finally recieved the heartbeat.
                //Time to send CLIENT_ID_REQUEST_MSG
                [localConnection sendNetworkPacket: [NSNumber numberWithInt:[[ECDrawAppDelegate getInstance] getClientID]] withType:CLIENT_ID_REQUEST_MSG andCID:[[ECDrawAppDelegate getInstance] getClientID] andPID:0 andID:0];
            }
            else{
                // Respond to server heartbeat check to see if client (aka me) is still connected.
                [connection sendNetworkPacket:0 withType:HEARTBEAT_MSG andCID:0 andPID:0 andID:0];
            }
            break;
        }
            
	}
	
	
	if(host){ 
        if(packetInfo[PACKET_TYPE_HDR] == LINE_MSG || packetInfo[PACKET_TYPE_HDR] == TEXT_MSG || packetInfo[PACKET_TYPE_HDR] == SHAPE_MSG || packetInfo[PACKET_TYPE_HDR] == IMAGE_MSG || packetInfo[PACKET_TYPE_HDR] == DELETE_MSG) 
        {
            //relay new drawing object to all local clients
            for (id object in clientSet) {
                if (object != connection) {
                    [object sendNetworkPacket:[NSKeyedUnarchiver unarchiveObjectWithData:packet] withType:packetInfo[PACKET_TYPE_HDR] andCID:packetInfo[CLIENT_ID_HDR] andPID:packetInfo[PAGE_ID_HDR] andID:packetInfo[LINE_ID_HDR]];
                }
            }
            
            //send to server
            [localConnection sendNetworkPacket:[NSKeyedUnarchiver unarchiveObjectWithData:packet] withType:packetInfo[PACKET_TYPE_HDR] andCID:packetInfo[CLIENT_ID_HDR] andPID:packetInfo[PAGE_ID_HDR] andID:packetInfo[LINE_ID_HDR]];
            
        }
        else if (packetInfo[PACKET_TYPE_HDR] == UNDO_MSG || packetInfo[PACKET_TYPE_HDR] == REDO_MSG) {
            for (id object in clientSet) {
                if (object != connection) {
                    [object sendNetworkPacket:0 withType:packetInfo[PACKET_TYPE_HDR] andCID:packetInfo[CLIENT_ID_HDR] andPID:packetInfo[PAGE_ID_HDR] andID:packetInfo[LINE_ID_HDR]];
                }
                else{
                    [localConnection sendNetworkPacket:0 withType:packetInfo[PACKET_TYPE_HDR] andCID:packetInfo[CLIENT_ID_HDR] andPID:packetInfo[PAGE_ID_HDR] andID:packetInfo[LINE_ID_HDR]];
                }
            }
        }
        
    }
    
	
}


- (void)dealloc {
	
	[localServer release];
    [redoButton release];
    [gridButton release];
    [saveButton release];
    [connectButton release];
    [homeButton release];
    [undoButton release];
    [searchTimer release];
    [super dealloc];
}


@end
