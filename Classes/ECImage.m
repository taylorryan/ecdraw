//
//  ECImage.m
//  ECDraw
//
//  Created by Ryan, Taylor on 5/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ECImage.h"

@implementation ECImage

-(ECImage *)initWithImage:(UIImage *)source atPoint:(CGPoint)point andPageNumber:(NSInteger)pid andElementID:(NSInteger)eid{
    
    self = [self initWithPage:pid];
    location = point;
    image = [source retain];
    
    elementID = eid;
    
    type = IMAGE_TYPE;
    
    return self;
}

//NSCoding compliance
//init using the info decoded from the NSCoder
- (ECImage *)initWithCoder:(NSCoder *)coder
{
	self = [super initWithCoder:coder];
	
    image = [[coder decodeObjectForKey:@"image"] retain];
	location.x = [coder decodeFloatForKey:@"locationX"];
	location.y = [coder decodeFloatForKey:@"locationY"];
	
	type = IMAGE_TYPE;
    
	return self;
}

//Encodes objects into the provided NSCoder
- (void)encodeWithCoder:(NSCoder *)coder
{		
	[super encodeWithCoder:coder];
    [coder encodeObject:image forKey:@"image"];
    [coder encodeFloat:location.x forKey:@"locationX"];
	[coder encodeFloat:location.y forKey:@"locationY"];
}


-(void)drawInContext:(CGContextRef)context{
    UIGraphicsPushContext(context);
    
    //CGContextTranslateCTM(context, 0, 768.0);
    //CGContextScaleCTM(context, 1.0, -1.0);
    //CGContextDrawImage(context, CGRectMake(location.x-image.size.width/2, location.y-768+image.size.height/2, image.size.width, image.size.height), [image CGImage]);
    //CGContextDrawImage(context, CGRectMake(0, 0, image.size.width, image.size.height), image.CGImage);
    [image drawInRect:CGRectMake(location.x-image.size.width/2, location.y-image.size.height/2, image.size.width, image.size.height)];
    
    UIGraphicsPopContext();
}

-(BOOL)containsPoint:(CGPoint)point
{
    return CGRectContainsPoint(CGRectMake(location.x-image.size.width/2, location.y-image.size.height/2, image.size.width, image.size.height), point);
}

-(BOOL)moveToPoint:(CGPoint)point
{
    
    location = point;
    
//    location.x += (point.x - location.x);
//    location.y += (point.y - location.y);
    
    return YES;
}

@end
