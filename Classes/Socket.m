//
//  Socket.m
//  ECDraw
//
//  Created by Ryan, Taylor on 5/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Socket.h"

@implementation Socket

@synthesize inputStream, outputStream,delegate;



- (id) initWithAddress:(NSString*)addr port:(int)p {
	if(self=[super init]) {
        CFReadStreamRef readStream;
        CFWriteStreamRef writeStream;
        CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)addr, p, &readStream, &writeStream);
        
        inputStream = (NSInputStream *)readStream;
        outputStream = (NSOutputStream *)writeStream;
        [inputStream setDelegate:self];
        [outputStream setDelegate:self];
        [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [inputStream open];
        [outputStream open];
    }
    return self;
}

- (void) sendMessage:(NSString*)s {
	
	NSData *data = [[NSData alloc] initWithData:[s dataUsingEncoding:NSASCIIStringEncoding]];
	[outputStream write:[data bytes] maxLength:[data length]];

    //    outputStream.flush;
	
}


- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent {
    
    NSLog(@"stream event %i", streamEvent);
	
	switch (streamEvent) {
			
		case NSStreamEventOpenCompleted:
        {
            NSLog(@"Stream opened");
			break;
        }
		case NSStreamEventHasBytesAvailable:
        {
			if (theStream == inputStream) {
				
                uint8_t buffer[1024];
				int len;
				
				while ([inputStream hasBytesAvailable]) {
					len = [inputStream read:buffer maxLength:sizeof(buffer)];
					if (len > 0) {
						
						NSString *output = [[NSString alloc] initWithBytes:buffer length:len encoding:NSASCIIStringEncoding];
						
						if (nil != output) {
                            
                            NSLog(@"server said: %@", output);
							[self messageReceived:output];
							
						}
					}
				}
			}
			break;
        }
			
		case NSStreamEventErrorOccurred:
		{	
            NSLog(@"Can not connect to the host!");
			break;
        }
		case NSStreamEventEndEncountered:
        {   
            [theStream close];
            [theStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
            [theStream release];
            theStream = nil;
			
			break;
        }
            //		default:
            //			NSLog(@"Unknown event");
	}
    
}

- (void) messageReceived:(NSString *)message {
    if(delegate && [delegate respondsToSelector:@selector(messageReceived:)]) {
        [delegate messageReceived:message];
    }
    
}


- (void)dealloc {
	[inputStream release];
	[outputStream release];
    [super dealloc];
}


@end

