//
//  TextViewController.h
//  ECDraw
//
//  Created by Bianco, Anthony M on 7/14/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextViewControllerDelegate.h"

@interface TextViewController : UIViewController {

	id<TextViewControllerDelegate> delegate;
	
	UITextField *userText;
}

@property (retain, nonatomic) IBOutlet UITextView *textView;
@property (nonatomic, retain) IBOutlet UITextField *userText;
@property (assign) id<TextViewControllerDelegate> delegate;

- (IBAction)cancel:(UIButton *)sender;
- (IBAction)confirm:(UIButton *) sender;

@end
