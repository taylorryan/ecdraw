//
//  NSString+SHA1.h
//  ECDraw
//
//  Created by Ryan, Taylor on 6/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (SHA1)
- (NSString *)sha1;
@end
