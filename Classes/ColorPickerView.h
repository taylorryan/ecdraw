//
//  ColorPickerView.h
//  ColorPicker
//
//  Created by Gilly Dekel on 23/3/09.
//  Extended by Fabián Cañas August 2010.
//  Copyright 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ColorSwatchView.h"

@class GradientView;
@interface ColorPickerView : UIView {
	GradientView *gradientView;
	IBOutlet ColorSwatchView *showColor;
	IBOutlet UIImageView *crossHairs;
	IBOutlet UIImageView *brightnessBar;
	
    IBOutlet ColorSwatchView *lastColor0;
    IBOutlet ColorSwatchView *lastColor1;
    IBOutlet ColorSwatchView *lastColor2;
    IBOutlet ColorSwatchView *lastColor3;
    IBOutlet ColorSwatchView *lastColor4;
    
	//Private vars
	CGRect colorMatrixFrame;
	
	CGFloat currentBrightness;
	CGFloat currentHue;
	CGFloat currentSaturation;
	
	UIColor *currentColor;
}

@property (readwrite) CGFloat currentBrightness;
@property (readwrite) CGFloat currentHue;
@property (readwrite) CGFloat currentSaturation;

- (UIColor *) getColorShown;
- (void) setColor:(UIColor *)color;
- (void) animateView:(UIImageView *)theView toPosition:(CGPoint) thePosition;

@end
