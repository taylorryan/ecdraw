//
//  ShapePickerController.m
//  ECDraw
//
//  Created by Bianco, Anthony M on 8/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ShapePickerController.h"
#import "ECShape.h"

static BOOL show3DShapes = false;

@implementation ShapePickerController

@synthesize delegate;

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
        if (show3DShapes) {
            //show 3D shapes
            self.view = shapes3D;
        }
        else {
            //show 2D shapes
            self.view = shapes2D;
        }
    }
    return self;
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    // Custom initialization.
    if (show3DShapes) {
        //show 3D shapes
        self.view = shapes3D;
    }
    else {
        //show 2D shapes
        self.view = shapes2D;
    }
    [super viewDidLoad];
    
}


/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark -
#pragma mark 2D Shapes
- (IBAction)squarePressed:(UIButton *) sender
{
	[delegate shapePickerDidFinishWithShape: SQUARE_SHAPE];
}

- (IBAction)rectanglePressed:(UIButton *) sender
{
	[delegate shapePickerDidFinishWithShape: RECTANGLE_SHAPE];
}

- (IBAction)circlePressed:(UIButton *) sender
{
	[delegate shapePickerDidFinishWithShape: CIRCLE_SHAPE];
}

- (IBAction)eTrianglePressed:(UIButton *) sender
{
	[delegate shapePickerDidFinishWithShape: ETRIANGLE_SHAPE];
}

- (IBAction)rTrianglePressed:(UIButton *) sender
{
	[delegate shapePickerDidFinishWithShape: RTRIANGLE_SHAPE];
}

- (IBAction)ovalPressed:(UIButton *) sender
{
	[delegate shapePickerDidFinishWithShape: OVAL_SHAPE];
}

- (IBAction)hLinePressed:(UIButton *) sender
{
	[delegate shapePickerDidFinishWithShape: HLINE_SHAPE];
}

- (IBAction)vLinePressed:(UIButton *) sender
{
	[delegate shapePickerDidFinishWithShape: VLINE_SHAPE];
}

- (IBAction)dLinePressed:(UIButton *) sender
{
	[delegate shapePickerDidFinishWithShape: DLINE_SHAPE];
}

#pragma mark -
#pragma mark 3D Shapes
- (IBAction)cubePressed:(UIButton *)sender {
    [delegate shapePickerDidFinishWithShape: CUBE_3D_SHAPE];
}

- (IBAction)cuboidPressed:(UIButton *)sender {
    [delegate shapePickerDidFinishWithShape: CUBOID_3D_SHAPE];
}

- (IBAction)pyramidPressed:(UIButton *)sender {
    [delegate shapePickerDidFinishWithShape: PYRAMID_3D_SHAPE];
}

- (IBAction)prismPressed:(UIButton *)sender {
    [delegate shapePickerDidFinishWithShape: PRISM_3D_SHAPE];
}

- (IBAction)conePressed:(UIButton *)sender {
    [delegate shapePickerDidFinishWithShape: CONE_3D_SHAPE];
}

- (IBAction)cylinderPressed:(UIButton *)sender {
    [delegate shapePickerDidFinishWithShape: CYLINDER_3D_SHAPE];
}

- (IBAction)spherePressed:(UIButton *)sender {
    [delegate shapePickerDidFinishWithShape: SPHERE_3D_SHAPE];
}

#pragma mark -


- (IBAction)flipView:(id)sender
{
    show3DShapes = !show3DShapes;
    
    //Switch between 2D and 3D shapes.
    if (show3DShapes) {
        //show 3D shapes
        self.view = shapes3D;
    }
    else {
        //show 2D shapes
        self.view = shapes2D;
    }
    
    
    
    //    ShapePicker3DController *controller = [[[ShapePicker3DController alloc] initWithNibName:@"3DShapePickerController" bundle:nil] autorelease];
    //    controller.flipSide = self;
    //    controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    //    [self presentModalViewController:controller animated:YES];
}

- (void)dealloc {
    [super dealloc];
}

@end
