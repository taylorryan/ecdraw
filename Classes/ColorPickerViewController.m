//
//  ColorPickerViewController.m
//  ColorPicker
//
//  Created by Gilly Dekel on 23/3/09.
//  Extended by Fabián Cañas August 2010.
//  Copyright 2010. All rights reserved.
//

#import "ColorPickerViewController.h"
#import "ColorPickerView.h"
#import "UIColor-HSVAdditions.h"

@implementation ColorPickerViewController

@synthesize delegate, chooseButton;
@synthesize defaultsKey;
@synthesize defaultsColor;

NSString *keyForHue = @"hue";
NSString *keyForSat = @"sat";
NSString *keyForBright = @"bright";

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	NSUserDefaults *saveColors = [NSUserDefaults standardUserDefaults];
	defaultsKey = @"lastColor0";
    
    NSData *colorData= [saveColors objectForKey:defaultsKey];
//    UIColor *color;
    if (colorData!=nil) {
        defaultsColor = (UIColor*)[NSKeyedUnarchiver unarchiveObjectWithData:colorData];
    }
    else {
        defaultsColor = [UIColor blackColor];
    }
    
	
    [self moveToDefault];   // Move the crosshair to the default setting
}

-(void) moveToDefault {
    ColorPickerView *theView = (ColorPickerView*) [self view];
    NSUserDefaults *saveColors = [NSUserDefaults standardUserDefaults];
    NSData *colorData= [saveColors objectForKey:defaultsKey];
    UIColor *color;
    if (colorData!=nil) {
        color = (UIColor*)[NSKeyedUnarchiver unarchiveObjectWithData:colorData];
        [theView setColor:color];
    }
    else
        [theView setColor:defaultsColor];
}

//save color to recently used color list (saves last 5 colors)
- (void) saveColor:(UIColor *) color{
    NSUserDefaults *saveColors = [NSUserDefaults standardUserDefaults];
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:color];
    
    NSString *colorKey;
    
    for (int i = 4; i > 0; i--) {
        //Get UserDefaults key for last color
        colorKey = [NSString stringWithFormat:@"lastColor%d",i];
        //get data from previous last color
        NSData *data= [saveColors objectForKey:[NSString stringWithFormat:@"lastColor%d",i-1]];
        //move previous color to position one later.
        [saveColors setObject:data forKey:colorKey];
    }
    //set new color to last color (the first spot)    
    colorKey = @"lastColor0";
    [saveColors setObject:colorData forKey:colorKey];
    //sync UserDefaults
    [saveColors synchronize];
}

- (UIColor *) getSelectedColor {
	return [(ColorPickerView *) [self view] getColorShown];
}

- (IBAction) chooseSelectedColor {
    UIColor *color = [self getSelectedColor];
    [self saveColor:color];
    [delegate colorPickerViewController:self didSelectColor:color];
}

- (IBAction) cancelColorSelection {
	[delegate colorPickerViewControllerDidCancel:self];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}


// Housekeeping actions when a view as unloaded
- (void)viewDidUnload {
    // Release any retained subviews of the main view.
#if ___IPHONE_OS_VERSION_MAX_ALLOWED >= 30000
    [super viewDidUnload];  // First super, from iOS 3 on
    
    self.chooseButton=nil;   // Same as release, but also setting it to nil
#endif
    
    return;
}



- (void)dealloc {
    // Release claimed resources also
    [defaultsKey release];
    [defaultsColor release];
    
    [super dealloc];
}

@end
