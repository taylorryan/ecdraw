//
//  ECText.m
//  ECDraw
//
//  Created by Bianco, Anthony M on 7/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ECText.h"
#import "DrawArea.h"


@implementation ECText

@synthesize text;
@synthesize snapToGrid;

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)aText{
    NSMutableString* newText = [[NSMutableString alloc] init];
    [newText appendString:[textView.text substringWithRange:NSMakeRange(0, range.location)]];
    [newText appendString:aText];
    
    self.text = newText;
    [self.text retain];
    textView.text = newText;
    
    [[DrawArea getInstance] setNeedsDisplay];
    
    [newText release];
    
    return NO;
}


 - (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
		
	 NSMutableString* newText = [[NSMutableString alloc] init];
	 [newText appendString:[textField.text substringWithRange:NSMakeRange(0, range.location)]];
	 [newText appendString:string];
	 
	 text = newText;
	 textField.text = newText;
	 
	 [[DrawArea getInstance] setNeedsDisplay];
	 
	 return NO;
		   
 }
 

- (ECText *)initWithText:(NSString *)userText andPoint:(CGPoint)point andSize:(CGFloat)setSize andColor:(UIColor *)setColor andPage:(NSInteger)pid andElementID:(NSInteger)eid
{
	self = [super initWithPage:pid];
	
	text = [[NSString alloc] initWithString: userText];
	location = point;
	size= setSize;
	color = setColor;
	
	elementID = eid;
	
	type = TEXT_TYPE;
    
	return self;
}

//NSCoding compliance
//init using the info decoded from the NSCoder
- (ECText *)initWithCoder:(NSCoder *)coder
{
	self = [super initWithCoder:coder];
	
	text = [[coder decodeObjectForKey:@"text"] retain];
	location.x = [coder decodeFloatForKey:@"locationX"];
	location.y = [coder decodeFloatForKey:@"locationY"];
	
	type = TEXT_TYPE;
    
	return self;
}

//Encodes objects into the provided NSCoder
- (void)encodeWithCoder:(NSCoder *)coder
{		
	[super encodeWithCoder:coder];
	[coder encodeObject:text forKey:@"text"];
	[coder encodeFloat:location.x forKey:@"locationX"];
	[coder encodeFloat:location.y forKey:@"locationY"];
}

- (void) snapLocationToGrid{
    if (snapToGrid) {
        //Calc snap to grid positions
        
        //x
        int rounding =  fmodf( location.x , 32) ;
        if (rounding < 0) 
            rounding+=32;
        if (rounding < 16) 
            location.x -= rounding;
        else 
            location.x += (32 - rounding);
        //y
        rounding = fmodf( location.y , 32);
        if (rounding < 0) 
            rounding+=32;
        if (rounding < 16) 
            location.y -= rounding;
        else 
            location.y += (32 - rounding);
    }
}

- (void)drawInContext:(CGContextRef)context
{
    [self snapLocationToGrid];
    
	UIGraphicsPushContext(context);
    
	CGContextTranslateCTM(context, 0, 768.0);
	CGContextScaleCTM(context, 1, -1);
    
    //CoreText 
    CTFontRef font = CTFontCreateWithName(CFSTR("Helvetica-Bold"), size, NULL);
    // Create an attributed string
    CFStringRef keys[] = { kCTFontAttributeName,kCTForegroundColorAttributeName };
    CFTypeRef values[] = { font, color.CGColor };
    CFDictionaryRef attr = CFDictionaryCreate(NULL, (const void **)&keys, (const void **)&values,
                                              sizeof(keys) / sizeof(keys[0]), &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
    CFAttributedStringRef attrString = CFAttributedStringCreate(NULL, (CFStringRef)text, attr);
    CFRelease(attr);

    CGContextSetShouldSmoothFonts(context, YES);
    CGContextSetShouldAntialias(context, YES);
    CGRect textRect = CGRectMake(0, 0, 900, 768);
    
    //Manual Line braking
    lineCount = 0;
    BOOL shouldDrawAnotherLine = YES;
    double width = textRect.size.width;
    CGPoint textPosition = CGPointMake(location.x, 768-location.y);
    ;
    // Initialize those variables.
    
    // Create a typesetter using the attributed string.
    CTTypesetterRef typesetter = CTTypesetterCreateWithAttributedString((__bridge CFAttributedStringRef )  attrString);
    
    // Find a break for line from the beginning of the string to the given width.
    CFIndex start = 0;
    while (shouldDrawAnotherLine) {
        lineCount++;
        
        CFIndex count = CTTypesetterSuggestLineBreak(typesetter, start, width);
        
        // Use the returned character count (to the break) to create the line.
        CTLineRef line = CTTypesetterCreateLine(typesetter, CFRangeMake(start, count));
        
        // Move the given text drawing position by the calculated offset and draw the line.
        CGContextSetTextPosition(context, textPosition.x, textPosition.y);
        CTLineDraw(line, context);
        if (line!=NULL) {
            CFRelease(line);
        }
        // Move the index beyond the line break.
        
        if (start + count >= CFAttributedStringGetLength(attrString)) {
            shouldDrawAnotherLine = NO;
            continue;
        }
        start += count;
        textPosition.y-=size;
    }
    if (typesetter!=NULL) {
        CFRelease(typesetter);
        
    }

    CFRelease(attrString);
    CFRelease(font); 
	
	CGContextTranslateCTM(context, 0, 768.0);
	CGContextScaleCTM(context, 1, -1);

	UIGraphicsPopContext();
}

-(BOOL)containsPoint:(CGPoint)point
{
    CGRect rect = CGRectMake(location.x, location.y-size, 900, size*lineCount);
    return CGRectContainsPoint(rect, point);
    
}

-(BOOL)moveToPoint:(CGPoint)point
{
    
    location.x += (point.x - location.x);
    location.y += (point.y - location.y);
    
    return YES;
}

@end
