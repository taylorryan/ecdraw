//
//  ECShape.m
//  ECDraw
//
//  Created by Bianco, Anthony M on 8/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ECShape.h"


@implementation ECShape

@synthesize endPoint;
@synthesize snapToGrid;

- (ECShape *)initWithColor:(UIColor *)setColor andSize:(CGFloat)setSize andType:(ShapeType)setType atPoint:(CGPoint)setStart andPageNumber:(NSInteger)pid andElementID:(NSInteger)eid
{
	self = [super initWithPage:pid];
	startPoint = setStart;
	
	elementID=eid;
	
	size = setSize;
	color = setColor;
	
	shapeType = setType;
	
	type = SHAPE_TYPE;	
	
	return self;
}

- (ECShape *)initWithCoder:(NSCoder *)coder
{
	self = [super initWithCoder:coder];
	
	type = SHAPE_TYPE;
	
	startPoint.x = [coder decodeFloatForKey:@"startX"];
	startPoint.y = [coder decodeFloatForKey:@"startY"];
	endPoint.x = [coder decodeFloatForKey:@"endX"];
	endPoint.y = [coder decodeFloatForKey:@"endY"];
	shapeType = [coder decodeIntForKey:@"shapeType"];
	
	
	return self;
}

//Encodes objects into the provided NSCoder
- (void)encodeWithCoder:(NSCoder *)coder
{	
	[super encodeWithCoder:coder];
	[coder encodeFloat:startPoint.x forKey:@"startX"];
	[coder encodeFloat:startPoint.y forKey:@"startY"];
	[coder encodeFloat:endPoint.x forKey:@"endX"];
	[coder encodeFloat:endPoint.y forKey:@"endY"];
	[coder encodeInt:shapeType forKey:@"shapeType"];
}

- (void) snapStartToGrid{
    if (snapToGrid) {
        //Calc snap to grid positions
        
        //x
        int rounding =  fmodf( startPoint.x , 32) ;
        if (rounding < 0) 
            rounding+=32;
        if (rounding < 16) 
            startPoint.x -= rounding;
        else 
            startPoint.x += (32 - rounding);
        //y
        rounding = fmodf( startPoint.y , 32);
        if (rounding < 0) 
            rounding+=32;
        if (rounding < 16) 
            startPoint.y -= rounding;
        else 
            startPoint.y += (32 - rounding);
    }
}

- (void) snapEndToGrid{
    if (snapToGrid) {
        //Calc snap to grid positions
        
        //x
        int rounding =  fmodf( endPoint.x , 32) ;
        if (rounding < 0) 
            rounding+=32;
        if (rounding < 16) 
            endPoint.x -= rounding;
        else 
            endPoint.x += (32 - rounding);
        //y
        rounding = fmodf( endPoint.y , 32);
        if (rounding < 0) 
            rounding+=32;
        if (rounding < 16) 
            endPoint.y -= rounding;
        else 
            endPoint.y += (32 - rounding);
    }
}

- (void)drawInContext:(CGContextRef)context
{
    
    UIGraphicsPushContext(context);
    
    CGContextSetLineWidth(context, size);
    [color setStroke];
    
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    
    //CGContextMoveToPoint(context, startPoint.x, startPoint.y);
    
    switch (shapeType) {
        case SQUARE_SHAPE:
        {
            CGFloat sideLength;
            CGFloat width, height;
            
            [self snapStartToGrid];
            [self snapEndToGrid];
            
            CGContextMoveToPoint(context, startPoint.x, startPoint.y);
            
            width = height = sideLength = MAX(abs(endPoint.x - startPoint.x) , abs(endPoint.y - startPoint.y));
            
            if( endPoint.x < startPoint.x)
                width = -sideLength;
            if( endPoint.y < startPoint.y)
                height = -sideLength;
            
            
            CGContextAddLineToPoint(context, startPoint.x + width, startPoint.y);
            CGContextAddLineToPoint(context, startPoint.x + width, startPoint.y + height);
            CGContextAddLineToPoint(context, startPoint.x, startPoint.y + height);
            
            CGContextClosePath(context);
        }
            break;
        case RECTANGLE_SHAPE:
        {
            [self snapStartToGrid];
            [self snapEndToGrid];
            
            CGContextMoveToPoint(context, startPoint.x, startPoint.y);
            CGContextAddLineToPoint(context, startPoint.x, endPoint.y);
            CGContextAddLineToPoint(context, endPoint.x, endPoint.y);
            CGContextAddLineToPoint(context, endPoint.x, startPoint.y);
            CGContextClosePath(context);				
            
            break;
        }
        case OVAL_SHAPE:
        {
            [self snapStartToGrid];
            [self snapEndToGrid];
            
            CGContextAddEllipseInRect(context, CGRectMake(startPoint.x, startPoint.y, endPoint.x - startPoint.x, endPoint.y - startPoint.y));
            
            break;
        }
        case CIRCLE_SHAPE:
        {
            [self snapStartToGrid];
            [self snapEndToGrid];
            
            CGFloat radius = 0.0;
            radius = sqrt(((endPoint.x - startPoint.x) * (endPoint.x - startPoint.x)) + ((endPoint.y - startPoint.y) * (endPoint.y - startPoint.y)));
            
            if(snapToGrid)
            {
                //snap radius
                int rounding =  fmodf( radius , 32) ;
                if (rounding < 0) 
                    rounding+=32;
                if (rounding < 16) 
                    radius -= rounding;
                else 
                    radius += (32 - rounding);
            }
            
            CGContextAddArc(context, startPoint.x, startPoint.y, radius, 0, 2 * M_PI, 0);
            
            break;
        }
        case RTRIANGLE_SHAPE:
        {
            [self snapStartToGrid];
            [self snapEndToGrid];
            
            CGContextMoveToPoint(context, startPoint.x, startPoint.y);
            CGContextAddLineToPoint(context, endPoint.x, startPoint.y);
            CGContextAddLineToPoint(context, endPoint.x, endPoint.y);
            CGContextClosePath(context);
            
            break;
        }
        case ETRIANGLE_SHAPE:
        {
            [self snapStartToGrid];
            [self snapEndToGrid];
            
            CGFloat sideLength;
            CGFloat width, height;
            CGContextMoveToPoint(context, startPoint.x, startPoint.y);
            
            width = sideLength = MAX(abs(endPoint.x - startPoint.x) , (2 * abs(endPoint.y - startPoint.y))/sqrt(3.0));
            
            if( endPoint.x < startPoint.x)
                width = -sideLength;
            if( endPoint.y < startPoint.y)
                height = -sqrt(3.0)/2.0 * sideLength;
            else
                height = sqrt(3.0)/2.0 * sideLength;
            
            CGContextAddLineToPoint(context, startPoint.x + width, startPoint.y);
            CGContextAddLineToPoint(context, startPoint.x + width/2, startPoint.y + height);
            
            CGContextClosePath(context);
        }
            break;
        case VLINE_SHAPE:
        {
            [self snapStartToGrid];
            [self snapEndToGrid];
            
            CGContextMoveToPoint(context, startPoint.x, startPoint.y);
            CGContextAddLineToPoint(context, startPoint.x, endPoint.y);
            
            break;
        }
        case HLINE_SHAPE:
        {
            [self snapStartToGrid];
            [self snapEndToGrid];
            
            CGContextMoveToPoint(context, startPoint.x, startPoint.y);
            CGContextAddLineToPoint(context, endPoint.x, startPoint.y);
            
            break;
        }
        case DLINE_SHAPE:
        {
            [self snapStartToGrid];
            [self snapEndToGrid];
            
            CGContextMoveToPoint(context, startPoint.x, startPoint.y);
            CGContextAddLineToPoint(context, endPoint.x, endPoint.y);
            
            break;
        }
        case CUBE_3D_SHAPE:
        {
            CGFloat sideLength, width, height, displaceWidth, displaceHeight;
            
            [self snapStartToGrid];
            [self snapEndToGrid];
            
            width = height = sideLength = MAX(abs(endPoint.x - startPoint.x) , abs(endPoint.y - startPoint.y));
            
            displaceWidth = displaceHeight = (sideLength/3) * (sqrtf(2)/2);
            
            if( endPoint.x < startPoint.x){
                width = -sideLength;
                displaceWidth *= -1;
            }
            if( endPoint.y < startPoint.y){
                height = -sideLength;
                displaceHeight *= -1;
            }
            
            // Draw Back Square and Connections
            CGContextMoveToPoint(context, startPoint.x + width + displaceWidth, startPoint.y + displaceHeight);
            CGContextAddLineToPoint(context, startPoint.x + width, startPoint.y);
            CGContextAddLineToPoint(context, startPoint.x, startPoint.y);
            CGContextAddLineToPoint(context, startPoint.x, startPoint.y+ height);
            CGContextAddLineToPoint(context, startPoint.x + displaceWidth, startPoint.y+ height + displaceHeight);               
            CGContextStrokePath(context);
            CGContextMoveToPoint(context, startPoint.x, startPoint.y);
            CGContextAddLineToPoint(context, startPoint.x + displaceWidth, startPoint.y + displaceHeight);               
            CGContextStrokePath(context);
            CGContextSetLineWidth(context, size/4);
            CGContextMoveToPoint(context, startPoint.x + width, startPoint.y);
            CGContextAddLineToPoint(context, startPoint.x + width, startPoint.y + height);
            CGContextAddLineToPoint(context, startPoint.x, startPoint.y + height);
            CGContextStrokePath(context);
            CGContextMoveToPoint(context, startPoint.x + width, startPoint.y + height);
            CGContextAddLineToPoint(context, startPoint.x + width + displaceWidth, startPoint.y + height + displaceHeight);
            CGContextStrokePath(context);
            CGContextSetLineWidth(context, size);
            
            // Draw Front Square
            CGContextMoveToPoint(context, startPoint.x+ displaceWidth, startPoint.y+ displaceHeight);
            CGContextAddLineToPoint(context, startPoint.x + width + displaceWidth, startPoint.y + displaceHeight);
            CGContextAddLineToPoint(context, startPoint.x + width + displaceWidth, startPoint.y + height + displaceHeight);
            CGContextAddLineToPoint(context, startPoint.x + displaceWidth, startPoint.y + height + displaceHeight);
            CGContextClosePath(context);
            
            break;
        }
        case CUBOID_3D_SHAPE:
        {
            [self snapStartToGrid];
            [self snapEndToGrid];
            
            float displacement, displaceWidth, displaceHeight;
            displacement = MAX(abs(endPoint.x - startPoint.x) , abs(endPoint.y - startPoint.y));
            displacement = (displacement/3) * (sqrtf(2)/2);
            
            displaceWidth = displaceHeight = displacement;
            
            if( endPoint.x < startPoint.x)
                displaceWidth *= -1;
            if( endPoint.y < startPoint.y)
                displaceHeight *= -1;
            
            
            // Draw Back Square and Connections
            CGContextMoveToPoint(context, endPoint.x + displaceWidth, startPoint.y + displaceHeight);
            CGContextAddLineToPoint(context, endPoint.x, startPoint.y);
            CGContextAddLineToPoint(context, startPoint.x, startPoint.y);
            CGContextAddLineToPoint(context, startPoint.x, endPoint.y);
            CGContextAddLineToPoint(context, startPoint.x + displaceWidth, endPoint.y + displaceHeight);               
            CGContextStrokePath(context);
            CGContextMoveToPoint(context, startPoint.x, startPoint.y);
            CGContextAddLineToPoint(context, startPoint.x + displaceWidth, startPoint.y + displaceHeight);               
            CGContextStrokePath(context);
            CGContextSetLineWidth(context, size/4);
            CGContextMoveToPoint(context, endPoint.x, startPoint.y);
            CGContextAddLineToPoint(context, endPoint.x, endPoint.y);
            CGContextAddLineToPoint(context, startPoint.x, endPoint.y);
            CGContextStrokePath(context);
            CGContextMoveToPoint(context, endPoint.x, endPoint.y);
            CGContextAddLineToPoint(context, endPoint.x + displaceWidth, endPoint.y + displaceHeight);
            CGContextStrokePath(context);
            CGContextSetLineWidth(context, size);
            
            // Draw Front Square
            CGContextMoveToPoint(context, startPoint.x+ displaceWidth, startPoint.y+ displaceHeight);
            CGContextAddLineToPoint(context, endPoint.x + displaceWidth, startPoint.y + displaceHeight);
            CGContextAddLineToPoint(context, endPoint.x + displaceWidth, endPoint.y + displaceHeight);
            CGContextAddLineToPoint(context, startPoint.x + displaceWidth, endPoint.y + displaceHeight);
            CGContextClosePath(context);
            break;
        }
        case CONE_3D_SHAPE:
        {
            [self snapStartToGrid];
            [self snapEndToGrid];
            
            float width = endPoint.x - startPoint.x;
            float height = endPoint.y - startPoint.y;
            height /= 4;
            
            // Draw triangle point
            CGContextMoveToPoint(context, startPoint.x, endPoint.y);
            CGContextAddLineToPoint(context, startPoint.x + width/2, startPoint.y);
            CGContextAddLineToPoint(context, endPoint.x, endPoint.y);
            CGContextStrokePath(context);
            
            // Draw "hidden" half of bottom ellipse
            CGContextSaveGState(context);
            BOOL onlyDrawTopHalf = YES;
            CGFloat halfMultiplier = onlyDrawTopHalf ? -1.0 : 1.0;
            CGRect ellipse = CGRectMake(startPoint.x, endPoint.y-height/2, width, height);
            CGRect clipRect = CGRectOffset(ellipse,0, halfMultiplier * height / 2);
            CGContextClipToRect(context, clipRect);
            CGContextSetLineWidth(context, size/4);
            CGContextStrokeEllipseInRect(context, ellipse);
            CGContextRestoreGState(context);
            
            // Draw visible half of bottom ellipse
            CGContextSaveGState(context);
            onlyDrawTopHalf = NO;
            halfMultiplier = onlyDrawTopHalf ? -1.0 : 1.0;
            ellipse = CGRectMake(startPoint.x, endPoint.y-height/2, width, height);
            clipRect = CGRectOffset(ellipse,0, halfMultiplier * height / 2);
            clipRect = CGRectInset(clipRect, -10, 0);
            CGContextClipToRect(context, clipRect);
            CGContextStrokeEllipseInRect(context, ellipse);
            CGContextRestoreGState(context);
            break;
        }
        case PYRAMID_3D_SHAPE:
        {
            [self snapStartToGrid];
            [self snapEndToGrid];
            
            float width = endPoint.x - startPoint.x;
            float height = endPoint.y - startPoint.y;
            
            CGContextMoveToPoint(context, startPoint.x+ width/2, startPoint.y);
            CGContextAddLineToPoint(context, startPoint.x, startPoint.y + height*3/4);
            CGContextAddLineToPoint(context, startPoint.x + width/5, endPoint.y);
            CGContextAddLineToPoint(context, startPoint.x + width/2, startPoint.y);
            CGContextAddLineToPoint(context, endPoint.x, endPoint.y);
            CGContextAddLineToPoint(context, startPoint.x + width/5, endPoint.y);
            CGContextStrokePath(context);
            
            CGContextSetLineWidth(context, size/4);
            CGContextMoveToPoint(context, startPoint.x+ width/2, startPoint.y);
            CGContextAddLineToPoint(context, startPoint.x+ width*4/5, startPoint.y + height*3/4);
            CGContextAddLineToPoint(context, startPoint.x, startPoint.y + height*3/4);
            CGContextStrokePath(context);
            
            CGContextMoveToPoint(context, endPoint.x, endPoint.y);
            CGContextAddLineToPoint(context, startPoint.x+ width*4/5, startPoint.y + height*3/4);
            CGContextStrokePath(context);
            CGContextSetLineWidth(context, size);
            break;
        }
        case PRISM_3D_SHAPE:
        {
            [self snapStartToGrid];
            [self snapEndToGrid];
            
            CGFloat width, height;
            
            width = abs(endPoint.x - startPoint.x);
            height = abs(endPoint.y-startPoint.y);
            
            if( endPoint.x < startPoint.x)
                width *= -1;
            if( endPoint.y < startPoint.y)
                height *= -1;			
            
            // Draw outlines.
            CGContextMoveToPoint(context, startPoint.x + width*2/3, startPoint.y + height/3);//1
            CGContextAddLineToPoint(context, startPoint.x + width/3, startPoint.y);//2
            CGContextAddLineToPoint(context, startPoint.x, startPoint.y + height*2/3);//3
            CGContextAddLineToPoint(context, startPoint.x + width/3, startPoint.y + height);//4
            CGContextAddLineToPoint(context, startPoint.x + width, startPoint.y + height);//5
            CGContextAddLineToPoint(context, startPoint.x + width*2/3, startPoint.y + height/3);//6
            CGContextAddLineToPoint(context, startPoint.x + width/3, startPoint.y + height); //7
            CGContextStrokePath(context);
            
            // Draw hidden lines
            CGContextSetLineWidth(context, size/4);
            CGContextMoveToPoint(context, startPoint.x + width/3, startPoint.y);//2
            CGContextAddLineToPoint(context, startPoint.x + width*2/3, startPoint.y + height*2/3);//8
            CGContextAddLineToPoint(context, startPoint.x, startPoint.y + height*2/3);//3
            CGContextMoveToPoint(context, startPoint.x + width*2/3, startPoint.y + height*2/3);//8
            CGContextAddLineToPoint(context, startPoint.x + width, startPoint.y + height);//5
            CGContextStrokePath(context);
            CGContextSetLineWidth(context, size);
            
            break;
        }
        case CYLINDER_3D_SHAPE:
        {
            [self snapStartToGrid];
            [self snapEndToGrid];
            
            float width = endPoint.x - startPoint.x;
            float height = endPoint.y - startPoint.y;
            
            // Draw top ellipse
            CGContextAddEllipseInRect(context, CGRectMake(startPoint.x, startPoint.y, width, height));
            
            // Draw vertical lines
            CGContextMoveToPoint(context, startPoint.x, startPoint.y+height/2);
            CGContextAddLineToPoint(context, startPoint.x, startPoint.y+(height*4) );
            CGContextMoveToPoint(context, endPoint.x, startPoint.y+height/2);
            CGContextAddLineToPoint(context, endPoint.x, startPoint.y+(height*4) );
            CGContextStrokePath(context);
            
            // Draw "hidden" half of bottom ellipse
            CGContextSaveGState(context);
            BOOL onlyDrawTopHalf = YES;
            CGFloat halfMultiplier = onlyDrawTopHalf ? -1.0 : 1.0;
            CGRect ellipse = CGRectMake(startPoint.x, startPoint.y+(height*3.5), width, height);
            CGRect clipRect = CGRectOffset(ellipse,0, halfMultiplier * height / 2);
            CGContextClipToRect(context, clipRect);
            CGContextSetLineWidth(context, size/4);
            CGContextStrokeEllipseInRect(context, ellipse);
            CGContextRestoreGState(context);
            
            // Draw visible half of bottom ellipse
            CGContextSaveGState(context);
            onlyDrawTopHalf = NO;
            halfMultiplier = onlyDrawTopHalf ? -1.0 : 1.0;
            ellipse = CGRectMake(startPoint.x, startPoint.y+(height*3.5), width, height);
            clipRect = CGRectOffset(ellipse,0, halfMultiplier * height / 2);
            clipRect = CGRectInset(clipRect, -10, 0);
            CGContextClipToRect(context, clipRect);
            CGContextStrokeEllipseInRect(context, ellipse);
            CGContextRestoreGState(context);
            
            break;
        }
        case SPHERE_3D_SHAPE:
        {
            [self snapStartToGrid];
            [self snapEndToGrid];
            
            CGFloat radius = 0.0;
            radius = sqrt(((endPoint.x - startPoint.x) * (endPoint.x - startPoint.x)) + ((endPoint.y - startPoint.y) * (endPoint.y - startPoint.y)));
            
            if(snapToGrid)
            {
                //snap radius
                int rounding =  fmodf( radius , 32) ;
                if (rounding < 0) 
                    rounding+=32;
                if (rounding < 16) 
                    radius -= rounding;
                else 
                    radius += (32 - rounding);
            }
            
            // Draw Circle
            CGContextAddArc(context, startPoint.x, startPoint.y, radius, 0, 2 * M_PI, 0);
            CGContextStrokePath(context);
            
            // Draw Back "hidden" part of "3D" ellipse
            CGContextSaveGState(context);
            BOOL onlyDrawTopHalf = YES;
            CGFloat halfMultiplier = onlyDrawTopHalf ? -1.0 : 1.0;
            CGRect ellipse = CGRectMake(startPoint.x-radius, startPoint.y - (radius /4), 2*radius, radius/2);
            CGRect clipRect = CGRectOffset(ellipse,0, halfMultiplier * radius/4);
            CGContextClipToRect(context, clipRect);
            CGContextSetLineWidth(context, size/4);
            CGContextStrokeEllipseInRect(context, ellipse);
            CGContextRestoreGState(context);
            
            // Draw Front part of "3D" ellipse
            CGContextSaveGState(context);
            onlyDrawTopHalf = NO;
            halfMultiplier = onlyDrawTopHalf ? -1.0 : 1.0;
            ellipse = CGRectMake(startPoint.x-radius, startPoint.y - (radius /4), 2*radius, radius/2);
            clipRect = CGRectOffset(ellipse,0, halfMultiplier * radius/4);
            clipRect = CGRectInset(clipRect, -10, 0);
            CGContextClipToRect(context, clipRect);
            CGContextStrokeEllipseInRect(context, ellipse);
            CGContextRestoreGState(context);
            
            break;
        }
        default:
            break;
    }
    
    CGContextStrokePath(context);
    UIGraphicsPopContext();
	
}

-(BOOL)containsPoint:(CGPoint)point{
    switch (shapeType) {
        case SQUARE_SHAPE:
        {
            CGFloat sideLength;
            CGFloat width, height;
            
            width = height = sideLength = MAX(abs(endPoint.x - startPoint.x) , abs(endPoint.y - startPoint.y));
            
            if( endPoint.x < startPoint.x)
                width = -sideLength;
            if( endPoint.y < startPoint.y)
                height = -sideLength;
            
            return CGRectContainsPoint(CGRectMake(startPoint.x, startPoint.y, width, height), point);
        }
            break;
        case RECTANGLE_SHAPE:
        {
            return CGRectContainsPoint(CGRectMake(startPoint.x, startPoint.y, endPoint.x - startPoint.x, endPoint.y - startPoint.y), point);
            
            break;
        }
        case OVAL_SHAPE:
        {            
            float a = abs(endPoint.x - startPoint.x) / 2;
            float b = abs(endPoint.y - startPoint.y) / 2;
            float x = point.x - (MIN(startPoint.x, endPoint.x) + a);
            float y = point.y - (MIN(startPoint.y, endPoint.y) + b);
            
            //Check if point is in Oval.
            if ( ( (x*x)/(a*a) + (y*y)/(b*b) ) <=1) {
                return YES;
            }
            return NO;
            
            break;
        }
        case CIRCLE_SHAPE:
        {               
            
            CGFloat radius = 0.0;
            radius = sqrt(((endPoint.x - startPoint.x) * (endPoint.x - startPoint.x)) + ((endPoint.y - startPoint.y) * (endPoint.y - startPoint.y)));
            
            if(snapToGrid)
            {
                //snap radius
                int rounding =  fmodf( radius , 32) ;
                if (rounding < 0) 
                    rounding+=32;
                if (rounding < 16) 
                    radius -= rounding;
                else 
                    radius += (32 - rounding);
            }
            
            float x = point.x - startPoint.x;
            float y = point.y - startPoint.y;
            
            if ( (x*x) + (y*y) <= (radius*radius) ) {
                return YES;
            }
            return NO;
            
            break;
        }
        case RTRIANGLE_SHAPE:
        {
            float x = point.x;
            float y = point.y;
            float x1,x2,x3,y1,y2,y3;
            x1 = startPoint.x;
            x2 = x3 = endPoint.x;
            y1 = y2 = startPoint.y;
            y3 = endPoint.y;
            float AB = (y-y1)*(x2-x1) - (x-x1)*(y2-y1);
            float BC = (y-y3)*(x1-x3) - (x-x3)*(y1-y3);
            float CA = (y-y2)*(x3-x2) - (x-x2)*(y3-y2);
            
            if (AB*BC > 0 && BC*CA > 0) {
                return YES;
            }
            return NO;
            
            break;
        }
        case ETRIANGLE_SHAPE:
        {
            
            [self snapStartToGrid];
            [self snapEndToGrid];
            
            CGFloat sideLength;
            CGFloat width, height;
            
            width = sideLength = MAX(abs(endPoint.x - startPoint.x) , (2 * abs(endPoint.y - startPoint.y))/sqrt(3.0));
            
            if( endPoint.x < startPoint.x)
                width = -sideLength;
            if( endPoint.y < startPoint.y)
                height = -sqrt(3.0)/2.0 * sideLength;
            else
                height = sqrt(3.0)/2.0 * sideLength;
            
            float x = point.x;
            float y = point.y;
            float x1,x2,x3,y1,y2,y3;
            x1 = startPoint.x;
            x2 = startPoint.x + width;
            x3 = startPoint.x + width/2;
            y1 = startPoint.y;
            y2 = startPoint.y;
            y3 = startPoint.y + height;
            float AB = (y-y1)*(x2-x1) - (x-x1)*(y2-y1);
            float BC = (y-y3)*(x1-x3) - (x-x3)*(y1-y3);
            float CA = (y-y2)*(x3-x2) - (x-x2)*(y3-y2);
            
            if (AB*BC > 0 && BC*CA > 0) {
                return YES;
            }
            return NO;
            
        }
            break;
        case VLINE_SHAPE:
        {
            
            if ( abs(point.x - startPoint.x) <= 8 )
                if ( (startPoint.y <= point.y && point.y <= endPoint.y) || (endPoint.y <= point.y && point.y <= startPoint.y)) 
                    return YES;
            
            return NO;
            
            break;
        }
        case HLINE_SHAPE:
        {
            
            if ( abs(point.y - startPoint.y) <= 8 )
                if ( (startPoint.x <= point.x && point.x <= endPoint.x) || (endPoint.x <= point.x && point.x <= startPoint.x)) 
                    return YES;
            
            return NO;
            
            break;
        }
        case DLINE_SHAPE:
        {     
            
            float m = (endPoint.y - startPoint.y) / (endPoint.x - startPoint.x);
            float yIntercept = startPoint.y - (startPoint.x * m);
            
            float a = -m;
            float b = 1;
            float c = -yIntercept;
            
            float x = point.x;
            float y = point.y;
            
            float distance = abs(a*x + b*y + c) / sqrtf(a*a + b*b);
            
            if (distance <= 10) {
                return YES;
            }
            return NO;
            
            break;
        }
        case CUBE_3D_SHAPE:
        {
            CGFloat sideLength;
            CGFloat width, height;
            CGFloat displaceWidth, displaceHeight;
            
            [self snapStartToGrid];
            [self snapEndToGrid];
            
            width = height = sideLength = MAX(abs(endPoint.x - startPoint.x) , abs(endPoint.y - startPoint.y));
            
            displaceWidth = displaceHeight = (sideLength/3) * (sqrtf(2)/2);
            
            if( endPoint.x < startPoint.x){
                width = -sideLength;
                displaceWidth *= -1;
            }
            if( endPoint.y < startPoint.y){
                height = -sideLength;
                displaceHeight *= -1;
            }
            
            return CGRectContainsPoint(CGRectMake(startPoint.x, startPoint.y, width+displaceWidth, height+displaceHeight), point);
            
            break;
        }
        case CUBOID_3D_SHAPE:
        {          
            //Displacement calculations
            [self snapStartToGrid];
            [self snapEndToGrid];
            
            float displacement, displaceWidth, displaceHeight;
            displacement = MAX(abs(endPoint.x - startPoint.x) , abs(endPoint.y - startPoint.y));
            displacement = (displacement/3) * (sqrtf(2)/2);
            
            displaceWidth = displaceHeight = displacement;
            
            if( endPoint.x < startPoint.x)
                displaceWidth *= -1;
            if( endPoint.y < startPoint.y)
                displaceHeight *= -1;
            
            return CGRectContainsPoint(CGRectMake(startPoint.x, startPoint.y, endPoint.x - startPoint.x+displaceWidth, endPoint.y - startPoint.y+displaceHeight), point);
            
            break;
        }
        case CONE_3D_SHAPE:
        {
            [self snapStartToGrid];
            [self snapEndToGrid];
            
            float width = endPoint.x - startPoint.x;
            float height = endPoint.y - startPoint.y;
            height /= 4;
            
            //Ellipse
            float a = abs(width) / 2;
            float b = abs(height) / 2;
            float x = point.x - (MIN(startPoint.x,endPoint.x) + a);
            float y = point.y - endPoint.y;
            
            if ( ( (x*x)/(a*a) + (y*y)/(b*b) ) <=1) {
                return YES;
            }
            
            //triangle  
            x = point.x;
            y = point.y;
            float x1,x2,x3,y1,y2,y3;
            x1 = startPoint.x;
            x2 = startPoint.x + width/2;
            x3 = endPoint.x;
            y1 = endPoint.y;
            y2 = startPoint.y;
            y3 = endPoint.y;
            float AB = (y-y1)*(x2-x1) - (x-x1)*(y2-y1);
            float BC = (y-y3)*(x1-x3) - (x-x3)*(y1-y3);
            float CA = (y-y2)*(x3-x2) - (x-x2)*(y3-y2);
            
            if (AB*BC > 0 && BC*CA > 0) {
                return YES;
            }
            
            //Nothing
            return NO;
            
            break;
        }
        case PYRAMID_3D_SHAPE:
        {
            float width = endPoint.x - startPoint.x;
            float height = endPoint.y - startPoint.y;
            float x = point.x;
            float y = point.y;
            float x1,x2,x3,y1,y2,y3;
            x1 = startPoint.x + width/2;
            x2 = startPoint.x + width/5;
            x3 = endPoint.x;
            y1 = startPoint.y;
            y2 = endPoint.y;
            y3 = endPoint.y;
            float AB = (y-y1)*(x2-x1) - (x-x1)*(y2-y1);
            float BC = (y-y3)*(x1-x3) - (x-x3)*(y1-y3);
            float CA = (y-y2)*(x3-x2) - (x-x2)*(y3-y2);
            
            if (AB*BC > 0 && BC*CA > 0) {
                return YES;
            }
            
            x1 = startPoint.x + width/2;
            x2 = startPoint.x;
            x3 = startPoint.x + width/5;
            y1 = startPoint.y;
            y2 = startPoint.y + height*3/4;
            y3 = endPoint.y;
            AB = (y-y1)*(x2-x1) - (x-x1)*(y2-y1);
            BC = (y-y3)*(x1-x3) - (x-x3)*(y1-y3);
            CA = (y-y2)*(x3-x2) - (x-x2)*(y3-y2);
            
            if (AB*BC > 0 && BC*CA > 0) {
                return YES;
            }
            
            return NO;
            break;
        }
        case PRISM_3D_SHAPE:
        {
            
            CGFloat width, height;
            width = abs(endPoint.x - startPoint.x);
            height = abs(endPoint.y-startPoint.y);
            
            if( endPoint.x < startPoint.x)
                width *= -1;
            if( endPoint.y < startPoint.y)
                height *= -1;	
            
            // Triangle
            float x = point.x;
            float y = point.y;
            float x1,x2,x3,y1,y2,y3;
            x1 = startPoint.x + width*2/3;
            x2 = startPoint.x + width/3;
            x3 = startPoint.x + width;
            y1 = startPoint.y + height/3;
            y2 = startPoint.y + height;
            y3 = startPoint.y + height;
            float AB = (y-y1)*(x2-x1) - (x-x1)*(y2-y1);
            float BC = (y-y3)*(x1-x3) - (x-x3)*(y1-y3);
            float CA = (y-y2)*(x3-x2) - (x-x2)*(y3-y2);
            
            if (AB*BC > 0 && BC*CA > 0) {
                return YES;
            }
            
            
            // Quadrilateral 
            
            // Ax + By + C = 0
            float A,B,C,D;
            CGPoint points[4];
            points[0] = CGPointMake(startPoint.x + width/3, startPoint.y);
            points[1] = CGPointMake(startPoint.x, startPoint.y + height*2/3);
            points[2] = CGPointMake(startPoint.x + width/3, startPoint.y + height);
            points[3] = CGPointMake(startPoint.x + width*2/3, startPoint.y + height/3);
            
            for (int i = 0; i < 4; i++) {
                //loop through each side of quadrilateral and see which side of the line the point touched is.
                
                A = -(points[(i+1)%4].y - points[(i)%4].y);
                B = points[(i+1)%4].x - points[(i)%4].x;
                C = -(A * points[(i)%4].x + B * points[(i)%4].y);
                D = A * point.x + B * point.y + C;
                if (D > 0) {
                    //point is outside quadrilateral
                    return NO;
                }
            }
            
            return YES;
            
            break;
        }
        case CYLINDER_3D_SHAPE:
        {
            float width = endPoint.x - startPoint.x;
            float height = endPoint.y - startPoint.y;
            //Top Ellipse
            float a = abs(width) / 2;
            float b = abs(height) / 2;
            float x = point.x - (MIN(startPoint.x, endPoint.x) + a);
            float y = point.y - (MIN(startPoint.y, endPoint.y) + b);
            if ( ( (x*x)/(a*a) + (y*y)/(b*b) ) <=1) {
                return YES;
            }
            
            //Bottom Ellipse
            x = point.x - (MIN(startPoint.x, endPoint.x) + a);
            y = point.y - (MIN(startPoint.y+(height*3.5), endPoint.y+(height*3.5)) + b);
            if ( ( (x*x)/(a*a) + (y*y)/(b*b) ) <=1) {
                return YES;
            }
            
            //Middle Rectangle
            return CGRectContainsPoint(CGRectMake(startPoint.x, startPoint.y+height/2, width, 3.5*height), point);
            
            break;
        }
        case SPHERE_3D_SHAPE:
        {
            CGFloat radius = 0.0;
            radius = sqrt(((endPoint.x - startPoint.x) * (endPoint.x - startPoint.x)) + ((endPoint.y - startPoint.y) * (endPoint.y - startPoint.y)));
            
            if(snapToGrid)
            {
                //snap radius
                int rounding =  fmodf( radius , 32) ;
                if (rounding < 0) 
                    rounding+=32;
                if (rounding < 16) 
                    radius -= rounding;
                else 
                    radius += (32 - rounding);
            }
            
            float x = point.x - startPoint.x;
            float y = point.y - startPoint.y;
            
            if ( (x*x) + (y*y) <= (radius*radius) ) {
                return YES;
            }
            return NO;
            
            break;
        }
        default:
            break;
    }
    
    return NO;
}

-(BOOL)moveToPoint:(CGPoint)point
{
    CGFloat width = endPoint.x - startPoint.x;
    CGFloat height = endPoint.y - startPoint.y;
    
    if (lastOffset != nil) {
        startPoint.x = point.x - [lastOffset x];
        startPoint.y = point.y -[lastOffset y];
        endPoint.x = point.x + (width - [lastOffset x]);
        endPoint.y = point.y + (height - [lastOffset y]);
    }
    
    CGPoint offset;
    offset.x = point.x - startPoint.x;
    offset.y = point.y - startPoint.y;
    lastOffset = [[ECPoint alloc] initWithX:offset.x andY:offset.y];
    
    return YES;
}

@end

