//
//  ECPoint.h
//  QuartzDraw
//
//  Created by Bianco, Anthony M on 6/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ECPoint : NSObject <NSCoding> {
	
	//an nsobject version of a point. has an x and y float
	
	@private
		CGFloat x;
		CGFloat y;
}

- (void)setX:(CGFloat)aFloat;
- (CGFloat)x;
- (void)setY:(CGFloat)aFloat;
- (CGFloat)y;
- (void)encodeWithCoder:(NSCoder *)coder;
- (ECPoint *)initWithCoder:(NSCoder *)coder;
- (ECPoint *)initWithX:(CGFloat)setX andY:(CGFloat)setY;
- (ECPoint *)initWithPoint:(CGPoint) setPoint;


@end
