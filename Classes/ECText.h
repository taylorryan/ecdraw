//
//  ECText.h
//  ECDraw
//
//  Created by Bianco, Anthony M on 7/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DrawingObject.h"
#import <CoreText/CoreText.h>

@interface ECText : DrawingObject <NSCoding,UITextFieldDelegate,UITextViewDelegate> {
	NSString *text;
	CGPoint location;
	int lineCount;
}

@property (assign) NSString *text;
@property BOOL snapToGrid;

- (ECText *)initWithText:(NSString *)userText andPoint:(CGPoint)location andSize:(CGFloat)size andColor:(UIColor *) setColor andPage:(NSInteger)pid andElementID:(NSInteger)eid;
- (void)drawInContext:(CGContextRef)context;

- (ECText *)initWithCoder:(NSCoder *)coder;
- (void)encodeWithCoder:(NSCoder *)coder;

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;

@end
