//
//  ECDrawAppDelegate.m
//  ECDraw
//
//  Created by Bianco, Anthony M on 6/22/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ECDrawAppDelegate.h"
#import "DrawArea.h"

static ECDrawAppDelegate* instance;
//Sets up NSCoding compliance for UIImage
@interface UIImage (NSCoding)
- (id)initWithCoder:(NSCoder *)decoder;
- (void)encodeWithCoder:(NSCoder *)encoder;
@end

@implementation UIImage (NSCoding)
- (id)initWithCoder:(NSCoder *)decoder {
	NSData *pngData = [decoder decodeObjectForKey:@"PNGRepresentation"];
	[self autorelease];
	self = [[UIImage alloc] initWithData:pngData];
	return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder {
	[encoder encodeObject:UIImagePNGRepresentation(self) forKey:@"PNGRepresentation"];
}
@end


@implementation ECDrawAppDelegate



@synthesize window;

#pragma mark -
#pragma mark Application lifecycle

+ (ECDrawAppDelegate *)getInstance
{
	if(instance == nil)
	{
		instance = [[ECDrawAppDelegate alloc] init];
	}
	
	return instance;
}

- (NSInteger)getClientID
{
	return clientID;
}

- (void)setClientID:(NSInteger)cid
{
	clientID=cid;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    
	//Initializations after application launch.

	instance = self;// only one instance of ECDrawAppDelegate allowed set it equal to current app.
	
	DrawAreaViewController *dvc = [[DrawAreaViewController alloc] init];//create a DrawAreaViewController
    
	self.window.rootViewController = dvc;
    [self.window makeKeyAndVisible];
    
    //External Screen Setup
    if ([[UIScreen screens] count] > 1) {
        [self addScreen:[[UIScreen screens] objectAtIndex:1] ];
    }
    [self setupScreenConnectionNotificationHandlers];
	
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive.
     */
}


- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     */
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}

- (void)dealloc {
    [window release];
    [externalWindow release];
    [externalDrawArea release];
	
	//[dvc release];
    [super dealloc];
}

#pragma mark -
#pragma mark External Screen

-(DrawArea *)getExternalDrawingArea{
    ExternalScreenController *externalScreen = (ExternalScreenController *)externalWindow.rootViewController;
    
    return externalScreen.drawingView;
}

- (void) setupScreenConnectionNotificationHandlers{
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    [center addObserver:self selector:@selector(screenDidConnect:) name:UIScreenDidConnectNotification object:nil];
    [center addObserver:self selector:@selector(screenDidDisconnect:) name:UIScreenDidDisconnectNotification object:nil];
}

- (void) addViewController:(UIViewController *)controller toWindow:(UIWindow *)aWindow {
    [aWindow setRootViewController:controller];
    [aWindow setHidden:NO];
}
- (void) screenDidConnect:(NSNotification *) notification {
    UIScreen                    *_screen            = [notification object];;
    [self addScreen:_screen];
    
}

- (void) addScreen:(UIScreen *)_screen{    
    NSLog(@"Screen connected: %@", _screen);
    
    // Get a window for it
    externalWindow = [[UIWindow alloc] initWithFrame:[_screen bounds]];
    [externalWindow setScreen:_screen];
    
    externalWindow.rootViewController = [[ExternalScreenController alloc] init];
    externalWindow.hidden = NO;
    
    DrawAreaViewController *dvc = (DrawAreaViewController *)window.rootViewController;
    dvc.drawingView.externalScreenAttached = YES;
}

- (void) screenDidDisconnect:(NSNotification *) notification {    
    NSLog(@"Screen disconnected");
    
    DrawAreaViewController *dvc = (DrawAreaViewController *)window.rootViewController;
    dvc.drawingView.externalScreenAttached = NO;
   
    [externalDrawArea release];
    externalDrawArea = nil;
    [externalWindow release];
    externalWindow = nil;
        
    
    return;
}

@end
