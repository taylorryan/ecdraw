//
//  ShapeTableController.h
//  ECDraw
//
//  Created by Ryan, Taylor on 6/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShapePickerControllerDelegate.h"
#import "ECShape.h"

@interface ShapeTableController : UITableViewController{
    id<ShapePickerControllerDelegate> controller;
}

@property (nonatomic,retain) id<ShapePickerControllerDelegate> controller;

@end
