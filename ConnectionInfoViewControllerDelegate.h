//
//  ConnectionInfoViewControllerDelegate.h
//  ECDraw
//
//  Created by Bianco, Anthony M on 7/13/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ConnectionInfoViewControllerDelegate

- (BOOL)connectToHost:(NSString *)hostAddr room:(BOOL)create name:(NSString*)name;
- (void)closeConnection;
- (void)refresh;

@end

