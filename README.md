# ECDraw

## A Collaborative Instruction Tool for the iPad

### Background
-Desired a tool that allows for real-time collaboration among students and faculty 
	-Virtual tutoring
	-Virtual office hours
	-Traditional classroom applications
-No existing app adequately addressed our needs

### Features
-Shared whiteboard
-Freehand drawing and writing
-Multiple colors
-Erase previous content and �undo� function
-Basic shapes
-Text box
-Scrolling, home and multiple pages
-Background images

### Future Development
-Using a server to manage connections
-Web browser support
-Addition of grid and graphing capabilities
-Ability to upload images and sessions to one location
-Audio/video capabilities
-Audience response capabilities
-Pilot use in traditional classroom as well as for virtual tutoring during the 2012-2013 school-year


