//
//  Connection.h
//  Chatty
//
//  Copyright (c) 2009 Peter Bakhyryev <peter@byteclub.com>, ByteClub LLC
//
//  Permission is hereby granted, free of charge, to any person
//  obtaining a copy of this software and associated documentation
//  files (the "Software"), to deal in the Software without
//  restriction, including without limitation the rights to use,
//  copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following
//  conditions:
//  
//  The above copyright notice and this permission notice shall be
//  included in all copies or substantial portions of the Software.
//  
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//  OTHER DEALINGS IN THE SOFTWARE.

typedef enum {
	PACKET_TYPE_HDR,
	SIZE_HDR,
	CLIENT_ID_HDR,
	PAGE_ID_HDR,
	LINE_ID_HDR
} HeaderType;

typedef enum {
	LINE_MSG,
	IMAGE_MSG,
	TEXT_MSG,
	SHAPE_MSG,
	UNDO_MSG,
	CLEAR_MSG,
	CLEAR_ALL_MSG,
	ADD_PAGE_MSG,
	REQUEST_PAGE_MSG,
	DELETE_PAGE_MSG,
	REFRESH_PAGE_REQUEST_MSG,
	REFRESH_PAGE_MSG,
    CLIENT_ID_MSG,
    CLIENT_ID_REQUEST_MSG,
    REDO_MSG,
    SESSION_NEW_MSG,
    SESSION_LOAD_MSG,
    SESSION_SAVE_MSG,
    NEW_HOST_MSG,
    CLOSE_CONNECTION_MSG,
    CHECK_PASSWORD_MSG,
    DELETE_MSG,
    HEARTBEAT_MSG
} MessageType;


#import <Foundation/Foundation.h>
#import <CFNetwork/CFSocketStream.h>
#import "ConnectionDelegate.h"

@class ECText;
@class ECImage;
@class ECLine;
@class ECShape;

@interface Connection : NSObject<NSNetServiceDelegate> {
    id<ConnectionDelegate> delegate;
    
    // Connection info: host address and port
    NSString* host;
    int port;
    
    // Connection info: native socket handle
    CFSocketNativeHandle connectedSocketHandle;
    
    // Connection info: NSNetService
    NSNetService* netService;
    
    // Read stream
    CFReadStreamRef readStream;
    NSInputStream *inputStream;
    bool readStreamOpen;
    NSMutableData* incomingDataBuffer;
    int packetInfo[5];
    
    // Write stream
    CFWriteStreamRef writeStream;
    NSOutputStream *outputStream;
    bool writeStreamOpen;
    NSMutableData* outgoingDataBuffer;
}

@property(nonatomic,retain) id<ConnectionDelegate> delegate;

// Initialize and store connection information until 'connect' is called
- (id)initWithHostAddress:(NSString*)host andPort:(int)port;

// Initialize using a native socket handle, assuming connection is open
- (id)initWithNativeSocketHandle:(CFSocketNativeHandle)nativeSocketHandle;

// Initialize using an instance of NSNetService
- (id)initWithNetService:(NSNetService*)netService;

// Connect using whatever connection info that was passed during initialization
- (BOOL)connect;

// Close connection
- (void)close;

// Send network message
- (void)sendNetworkPacket:(id)packet withType:(int)type andCID:(int)cid andPID:(int)pid andID:(int)lineID;

@end
