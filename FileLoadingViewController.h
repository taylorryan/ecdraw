//
//  FileLoadingViewController.h
//  ECDraw
//
//  Created by Ryan, Taylor on 5/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DrawAreaViewController;

@interface FileLoadingViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>{
    NSMutableArray *files;
}

@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (retain) DrawAreaViewController *controller;
- (IBAction)cancelPressed:(UIButton *)sender;
@end
